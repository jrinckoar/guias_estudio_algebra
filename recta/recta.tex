%*****************************************************************************************************************
%*****************************************************************************************************************
%                                       ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                 GUÍA DE ESTUDIO
%                                        RECTA EN EL PLANO Y EN EL ESPACIO
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2021-04-20
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                     FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark, babel}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
 {\left(\begin{array}{@{}#1@{}}}
 {\end{array}\right)}

\newcommand{\ro}[1]{{\color{blue}\scriptscriptstyle{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
\pgfplotsset{compat=1.8}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio VI \\ La Recta en el Espacio\\[3pt]
\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                    OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de la Sección 4.5.
%=================================================================================================================
%                                                   SECCIÓN 4.5
%=================================================================================================================
\section{Sección 4.5:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Ecuación vectorial de una recta (ecuación 4.5.4).
  \item Ecuaciones paramétricas de una recta (ecuación 4.5.5).
  \item Ecuaciones simétricas de una recta (ecuación 4.5.6).
\end{itemize}
Realice los ejemplos: 4.5.1, 4.5.2, 4.5.3, 4.5.5
%--------------------------------------------------------
\subsection{Notas de clase: Rectas en el espacio}
\textbf{Recta:}
En el espacio ($\RR^3$) se puede definir una recta $L$ como el lugar geométrico:
\[
  L=\W{P\E{x,y,z}\in\RR^3\, /\, \vv{P_1P}=t\vv{u};\, t\in\RR}
\]
donde $P_1=\E{x_0,y_0,z_0}$ es un punto sobre la recta y  $\vv{u}$ es un vector paralelo a ella (vector director).
En otras palabras,  la recta $L$ es el conjunto de  todos los puntos $P=\E{x,y,z}$ tales que el vector $\vv{P_1P}$
(formado entre los puntos $P$ y $P_1$) es  paralelo al vector $\vv{u}$\footnote[2]{Recordemos que dos vectores son
paralelos si y solo si $\vv{v}=t\vv{u}$ con $t\in\RR$.}.\\[0.5em]

\textbf{Posición relativa entre dos rectas en el espacio:}
En el espacio, dos rectas pueden ubicarse de forma relativa (una respecto a la otra) diferentes maneras:
\begin{itemize}
  \item Rectas que se cortan: Son dos rectas que tienen un solo punto de intersección.
  \item Rectas perpendiculares: Son rectas que se cortan y además sus vectores directores son perpendiculares~(ver
  figura~\ref{fig:Rper}).
  \item Rectas que no  se cortan (alabeadas): Son rectas que no comparten puntos en común.
  \item Rectas paralelas: Son rectas alabeadas cuyos vectores directores son paralelos~(ver figura~\ref{fig:Par}).
\end{itemize}

\deactivatequoting%
\begin{figure}[t!]
  \centering
  \subfloat[\label{fig:Rper}Rectas perpendiculares.]{%
    \begin{tikzpicture}
      \begin{axis}[
          width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-8,xmax=8,
          ymin=-8,ymax=8,
          zmin=-8,zmax=8,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          xticklabels={},
          yticklabels={},
          zticklabels={},
          view={120}{20},
          smooth,
        ]
        % Recta L1
        \addplot3[y domain=0:0, samples=10, domain=-1.5:3, ultra thick, blue, <->]
            ({1-x},{1+x},{-3+4*x}) node[right, pos=1.0]{$\vv{L_1}$};
        % Vector paralelo a L1
        \addplot3[y domain=0:0, samples=10, domain=-0.5:0.5, ultra thick, blue, ->]
            ({1-x},{1+x+5.2},{-3+4*x+8.05}) node[right, pos=0.8]{$\vv{v_1}$};
        % Recta L2
        \addplot3[y domain=0:0, samples=10, domain=-0.8:3, ultra thick, red, <->]
            ({-5+5*x},{5-3*x},{-1+2*x}) node[below, pos=1.0]{$L_2$};
        % Vector paralelo a L2
        \addplot3[y domain=0:0, samples=10, domain=-0.5:0.5, ultra thick, red, <-]
            ({-5+5*x},{5-3*x},{-1+2*x+1.5}) node[above, pos=0.5]{$\vv{v_2}$};
      \end{axis}
    \end{tikzpicture}
  }
  \subfloat[\label{fig:Par}Rectas paralelas.]{%
    \begin{tikzpicture}
      \begin{axis}[
          width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-8,xmax=8,
          ymin=-8,ymax=8,
          zmin=-8,zmax=8,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          xticklabels={},
          yticklabels={},
          zticklabels={},
          view={120}{20},
          smooth,
        ]
        % Recta L1
        \addplot3[y domain=0:0, samples=10, domain=-1.5:1.5, ultra thick, blue, <->]
            ({1+5*x},{2-2*x + 2},{-1-3*x}) node[above left, pos=0.0]{$L_1$};
        % Vector paralelo a L1
        \addplot3[y domain=0:0, samples=10, domain=-0.5:0.5, ultra thick, blue, <-]
            ({1+5*x},{2-2*x + 2 - 2},{-1-3*x}) node[left, pos=0.2]{$\vv{v_1}$};
        % Recta L2
        \addplot3[y domain=0:0, samples=10, domain=-0.8:0.8, ultra thick, red, <->]
            ({1-10*x},{-3+4*x},{2+6*x}) node[right, pos=1.0]{$L_2$};
        % Vector paralelo a L1
        \addplot3[y domain=0:0, samples=10, domain=-0.25:0.25, ultra thick, red, ->]
            ({1-10*x},{-3+4*x + 1.5},{2+6*x}) node[right, pos=0.5]{$\vv{v_2}$};
      \end{axis}
    \end{tikzpicture}
  }
\end{figure}
\activatequoting%
%--------------------------------------------------------
\subsection{Ejercicio 1:}
Demuestre que la recta $L_1$ que pasa por los puntos $P\E{1,1,-3}$ y $Q\E{0,2,1}$ es ortogonal a la recta:
\[
  L_2:\quad \frac{x+5}{5}=\frac{y-5}{-3}=\frac{z+1}{2}=s.
\]

El enunciad nos pide  demostrar que las rectas $L_1$ y $L_2$ son  ortogonales.  Es decir,  debemos mostrar que las
rectas se cortan en un punto y además que sus vectores directores son ortogonales.

Si analizamos la información que nos brinda el ejercicio,  podemos ver que tenemos las ecuaciones simétricas de la
recta $L_2$ y  no contamos con ninguna expresión de  la recta $L_1$,  solo nos dan dos  puntos pertenecientes a la
recta.  Esta información es suficiente para encontrar una expresión analítica para la recta $L_1$.

Utilizaremos los puntos $P$ y $Q$ para encontrar un vector director de la recta $L_1$, luego utilizaremos el punto
$P$  para encontrar  la ecuación  vectorial de  $L_1$ (se  podría utilizar  también el  punto $Q$)  y terminaremos
encontrando las ecuaciones paramétricas de $L_1$.

Sea el vector $\vv{v_1}$ director de la recta:
\[
  \vv{v_1} = \vv{PQ} = \vv{OQ} - \vv{OP} = \E{0,2,1} - \E{1,1,-3}= \E{-1,1,4}
\]

Para encontrar la  ecuación vectorial de $L_1$ proponemos un  punto $R\E{x,y,z}$,  cualquiera,  perteneciente a la
recta ($R\in L_1$).  Teniendo en cuenta el vector $\vv{v_1}$  y el punto $P$,  planteamos la ecuación vectorial de
$L_1$:
\[
  L_1:\W{R\E{x,y,z}\in\RR^3\, /\, \vv{PR}=t\vv{v_1};\, t\in\RR}
\]
De esta ecuación se desprende que:
\begin{align*}
  \vv{PR} &= t\vv{v_1} &&\\
  \vv{OR} - \vv{OP} &= t\vv{v_1} &&\\
  \E{x,y,z}- \E{1,1,-3} &= t\vv{v_1} && \text{Reemplazando los vectores $\vv{OR}$ y $\vv{OP}$.}\\
  \E{x-1,y-1,z+3} &= t\vv{v_1} && \\
  \E{x-1,y-1,z+3} &= t\E{-1,1,4} && \text{Reemplazando el vector $\vv{v_1}$.}\\
  \E{x-1,y-1,z+3} &= \E{-t,t,4t} && \text{Producto de un vector por un escalar.}
\end{align*}
En este punto nos encontramos con una igualdad entre vectores, igualando componente a componente obtenemos:
\[
  L_1:\quad \begin{cases}x-1 &= -t \\y-1 &= t \\z+3 &= 4t\\\end{cases}\Rightarrow
    \begin{cases}x &= 1-t \\y &= 1+t \\z &=-3+ 4t\\\end{cases} \qquad\text{Ecuación paramétrica de la recta.}
\]

En este  punto ya tenemos  expresiones para las rectas  $L_1$ y $L_2$.  Ahora  comprobemos que se  cortan el algún
punto del espacio.  El sentido común nos dirá que si estas rectas se cortan en un punto $\E{x,y,z}$,  entonces las
coordenadas de ese punto deben ser iguales para ambas rectas.  Partiendo de la ecuación simétrica de $L_2$:
\[
  \frac{x+5}{5}=\frac{y-5}{-3}=\frac{z+1}{2}=s
\]
que nos lleva al sistema:
\begin{align*}
  \begin{cases}\frac{x+5}{5}=s\\\frac{y-5}{-3}=s\\\frac{z+1}{2}=s\end{cases} \Rightarrow
  \begin{cases}x=-5+5s\\y=5-3s\\z=-1+2s\end{cases}
  \Rightarrow&\begin{cases}1-t=-5+5s\\1+t=5-3s\\-3+4t=-1+2s\end{cases} &&
    \begin{tabular}{l}
    \small{Igualando las ecuaciones de las rectas $L_1$ y $L_2$,}\\
    \small{es decir, reemplazando: $x=1-t$, $y=1+t$ y}\\
    \small{$z=-3+4t$.}
    \end{tabular}\\
  \Rightarrow& \systeme{-5s-t=-6, 3s+t=4, -2s+4t=2} &&
\end{align*}

La última ecuación nos plantea un sistema no homogéneo de 3 ecuaciones con 2 incógnitas.  Sabemos que los sistemas
no homogéneos pueden tener  solución única o infinitas soluciones o no tener  solución.  Esto nos dice mucho sobre
los puntos que tienen en común las rectas $L_1$ y $L_2$:
\begin{itemize}
  \item Si el sistema tienen solución única, entonces las rectas comparten un solo punto.
  \item Si el sistema tienen infinitas soluciones, entonces las rectas comparten infinitos puntos, es decir son la
  misma recta (recordemos que las ecuaciones de una recta no son únicas).
  \item Si el sistema no tiene solución, entonces las rectas no se cortan.
\end{itemize}

Resolviendo el sistema:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rr|r}
   -5 & -1 &  -6 \\
   3 &  1 &  4 \\
  -2 &  4 &  2 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
  & \ro{(-1/2)R_3\to R_1}
% Segunda Matriz
\begin{sysmatrix}{rr|r}
  1 &  -2 &  -1 \\
  3 &  1 &  4 \\
 -5 & -1 &  -6 \\
\end{sysmatrix}
&\!\begin{aligned}
  & \ro{-3R_1+R_2\to R_2}\\
  & \ro{5R_1+R_3\to R_3}\\
\end{aligned}
% Operaciones por renglón (solo una operación)
% Tercera Matriz
\begin{sysmatrix}{rr|r}
  1 &  -2 &  -1 \\
  0 &  7 &  7 \\
  0 & -11 &  -11 \\
\end{sysmatrix}
\\
&\!\begin{aligned}
  & \ro{(1/7)R_2\to R_2}\\
  & \ro{(-1/11)R_3\to R_3}\\
\end{aligned}
% Cuarta Matriz
\begin{sysmatrix}{rr|r}
  1 &  -2 &  -1 \\
  0 &  1 &  1 \\
  0 &  1 &  1 \\
\end{sysmatrix}
& \ro{(-1)R_2+R_3\to R_3}
\begin{sysmatrix}{rr|r}
  1 &  -2 &  -1 \\
  0 &  1 &  1 \\
  0 &  0 &  0 \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
Sistema equivalente:
\[
  \systeme{s-2t=-1, t=1,0t=0}\Rightarrow \begin{cases}t &= 1 \\s= -1+2t  &= 1 \\\end{cases}
\]

Vemos que el sistema tiene solución única $\E{s,t}=\E{1,1}$ lo que indica que el único punto de intersección es:
\begin{align*}
  L_1:\quad\begin{cases}x &= 1-t \\y &= 1+t \\z &=-3+ 4t\\\end{cases}\Rightarrow&
    \begin{cases}x &= 1-1 \\y &= 1+1 \\z &=-3+ 4\cdot1\\\end{cases} &&\text{Reemplazando $t=1$.} \\
      \Rightarrow&\begin{cases}x &= 0 \\y &= 2 \\z &=1\\\end{cases}
\end{align*}

Hasta el  momento hemos  probado que las  rectas $L_1$ y  $L_2$ se  cortan en  el punto  $I\E{0,2,1}$.  La segunda
condición que  debemos verificar  para que  las rectas sean  perpendiculares,  es que  sus vectores  directores son
ortogonales.   El  vector   director de  $L_1$   es  $\vv{v_1}=\E{-1,1,4}$  y   el  correspondiente  a   $L_2$  es
$\vv{v_2}=\E{5,-3,2}$.  Para comprobar si los vectores son ortogonales,  basta con constatar que su producto punto
es cero:
\[
  \vv{v_1}\cdot\vv{v_2}=\E{-1,1,4}\cdot\E{5,-3,2}= -5 -3+8=0
\]

En conclusión, podemos afirmar que las rectas $L_1$ y $L_2$ son perpendiculares ya que comparten un punto en común
y sus vectores directores son ortogonales.
%=================================================================================================================
%                                                      BIBLIOGRAFÍA
%=================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%=================================================================================================================
\end{document}
%=================================================================================================================
