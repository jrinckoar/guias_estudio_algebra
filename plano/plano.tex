%*****************************************************************************************************************
%*****************************************************************************************************************
%                                       ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                 GUÍA DE ESTUDIO
%                                                      PLANO
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2021-04-20
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                     FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark, babel}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
 {\left(\begin{array}{@{}#1@{}}}
 {\end{array}\right)}

\newcommand{\ro}[1]{{\color{blue}\scriptscriptstyle{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
\pgfplotsset{compat=1.8}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio VII \\ Plano en el Espacio\\[3pt]
\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                    OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de la Sección 4.5.
%=================================================================================================================
%                                                   SECCIÓN 4.5
%=================================================================================================================
\section{Sección 4.5:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 4.5.1: Plano.
  \item Ecuación Cartesiana de un plano (ecuación 4.5.9).
  \item Definición 4.5.2: Planos paralelos.
\end{itemize}
Realice los ejemplos: 4.5.6, 4.5.7, 4.5.8 y 4.5.9.
%--------------------------------------------------------
\subsection{Notas de clase: Planos}

\textbf{Posición relativa entre dos planos:}
Dos planos $\pi_1$ y $\pi_2$,  con vectores normales $\vv{n_1}$ y $\vv{n_2}$,  respectivamente; pueden ubicarse de
forma relativa en el espacio de las siguientes maneras:
\begin{itemize}
  \item Planos paralelos:  Son planos  que no tienen puntos en común y además  sus vectores normales son paralelos
  ($\vv{n_1}\parallel\vv{n_2}$).  Ver figura~\ref{fig:PlanPar}.
  \item Planos  coincidentes:  Son planos cuyos  vectores normales son  paralelos y comparten  infinitos puntos en
  común.  En otras  palabras,  son el mismo plano  (el mismo lugar  geométrico) pero  descrito por  dos ecuaciones
  cartesianas distintas (la ecuación cartesiana de un plano no es única).Ver figura~\ref{fig:PlanCon}.
  \item Planos que se cortan:  Son planos cuyos vectores normales no son paralelos ($\vv{n_1}\nparallel\vv{n_2}$).
  Es importante resaltar que la intersección entre dos planos es una recta.
  \item Planos perpendiculares: Son planos cuyos vectores normales son ortogonales ($\vv{n_1}\perp\vv{n_2}$).  Ver
  figura~\ref{fig:PlanPer}.
\end{itemize}

\deactivatequoting%
\begin{figure}[t]
  \centering
  \subfloat[\label{fig:PlanPar}Planos paralelos.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          xticklabels={},
          yticklabels={},
          zticklabels={},
        ]
        % Plano pi1
        \addplot3[fill=blue!30]coordinates{(-2,-2,-3)(0,2,-3)(0,2,3)(-2,-2,3)(-2,-2,-3)};
        % Plano pi2
        \addplot3[fill=red!30]coordinates{(0,-2,-3)(2,2,-3)(2,2,2)(0,-2,2)(0,-2,-3)};
        % Vector normal a p1
        \addplot3[ultra thick, ->,blue]coordinates{(-1,0,1)(2,-1,1)} node[above, pos=0.7]{$\vv{n_1}$};
        % Vector normal a p2
        \addplot3[ultra thick, ->,red]coordinates{(0,0,-1)(3,-1,-1)} node[above, pos=0.8]{$\vv{n_2}$};
      \end{axis}
     \end{tikzpicture}
    }
  \subfloat[\label{fig:PlanCon}Planos coincidentes.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          xticklabels={},
          yticklabels={},
          zticklabels={},
        ]
        % Plano pi1
        \addplot3[fill=blue!30]coordinates{(-2,-2,-3)(0,2,-3)(0,2,3)(-2,-2,3)(-2,-2,-3)};
        % Plano pi2
        \addplot3[fill=red!30]coordinates{(-1,-1,-2)(0,1,-2)(0,1,2)(-1,-1,2)(-1,-1,-2)};
        % Vector normal a p1
        \addplot3[ultra thick, ->,blue]coordinates{(-1,0,1)(2,-1,1)} node[above, pos=0.8]{$\vv{n_1}$};
        % Vector normal a p2
        \addplot3[ultra thick, ->,red]coordinates{(0,0,-1)(3,-1,-1)} node[above, pos=0.8]{$\vv{n_2}$};
      \end{axis}
     \end{tikzpicture}
    }
  \subfloat[\label{fig:PlanPer}Planos  perpendiculares.  La recta  $L$ es  el  conjunto  intersección  entre ambos
  planos.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.4\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          xticklabels={},
          yticklabels={},
          zticklabels={},
        ]
        % Plano pi1 parte 1
        \addplot3[fill=red!30]coordinates{(0,3,0)(-3,3,0)(-3,-3,0)(0,-3,0)};
        % Plano pi2
        \addplot3[fill=blue!30]coordinates{(0,-2,-3)(0,2,-3)(0,2,3)(0,-2,3)(0, -2,-3)};
        % Plano pi1 parte 2
        \addplot3[fill=red!30]coordinates{(0,-3,0)(3,-3,0)(3,3,0)(0,3,0)};
        % Recta L intersección
        \addplot3[ ultra thick, olive, <->]coordinates{(0,-2.5,0)(0,2.5,0)} node[above right, pos=0.9]{$\bm{L}$};
        % Vector normal a p1
        \addplot3[ultra thick, ->,blue]coordinates{(0,0,2)(3,0,2)} node[above, pos=0.9]{$\vv{n_1}$};
        %% Vector normal a p2
        \addplot3[ultra thick, ->,red]coordinates{(1,0,0)(1,0,3)} node[right, pos=0.3]{$\vv{n_2}$};
      \end{axis}
     \end{tikzpicture}
    }
    \caption{}
\end{figure}
\activatequoting%
%--------------------------------------------------------
\subsection{Ejercicio 2:}
Sea el plano:
\[
  \pi_1:\quad 2x+3y+z=1.
\]

Determine la posición relativa de cada plano con $\pi_1$.
\begin{multicols}{2}
  \begin{enumerate}[label=\alph*.]
    \item $\pi_2:\, 6x+9y+3z=4$.
    \item $\pi_2:\, 4x+6y+z=2$.
  \end{enumerate}
\end{multicols}
Para cada uno de los ejercicios escriba el conjunto intersección entre los planos.\\[0.5em]

\textbf{a.} El ejercicio nos da  la ecuación cartesiana de los planos $\pi_1$ y  $\pi_2$,  y nos pide encontrar la
posición  relativa de  entre ambos  planos y  dar  una  expresión  para  su  conjunto  imagen.  De  las ecuaciones
cartesianas de ambos planos podemos obtener sus vectores normales:
\[
  \vv{n_1}= \E{2,3,1} \quad\text{y}\quad\vv{n_2}=\E{6,9,3}
\]
Podemos observar que $\vv{n_2}$ y $\vv{n_1}$ son paralelos, ya que son múltiplos escalares ($\vv{n_2}=3\vv{n_1}$).
Por tanto los planos pueden ser paralelos o coincidentes.  Para determinar cual de las anteriores posibilidades es
la correcta,  basta con verificar  si existen algún punto en común entre  ambos planos\footnote[3]{Tener en cuenta
que si  verificamos que existe  un solo punto  en común,  esto significa  que existen infinitos  puntos en común}.
Podemos obtener  un punto  $P$ perteneciente  al plano  $\pi_1$ y  ver si  este punto  también pertenece  al plano
$\pi_2$.  El punto $P\in\pi_1$ lo podemos encontrar proponiendo valores  para las coordenadas $x$ e $y$ y utilizar
la ecuación del plano para encontrar la coordenada $z$.  Sea $x=0$ y $y=0$, entonces:
\[
      z = 1 - 2x - 3y = 1-0-0 = 1
\]

Entonces el  punto $P(0,0,1)$ pertenece  al plano $\pi_1$.  Si  este punto pertenece  también a $\pi_2$,  entonces
debería verificar su ecuación:
\begin{align*}
  \pi_2:\qquad\qquad 6x+9y+3z&=4 \\
               6\cdot0 + 9\cdot0+3\cdot1 &=4 \\
               3 &=4
\end{align*}
La última ecuación nos plantea un absurdo e indica que el punto $P$ no pertenece al plano $\pi_2$.  Esto significa
que los  planos no comparten  ningún punto en  común y por  tanto son planos  paralelos.  El conjunto intersección
entre estos dos planos es el conjunto vacío ($\varnothing$).\\[0.5em]

\textbf{b.}  El vector  normal al  plano $\pi_2$  es $\vv{n_2}=\E{4,6,1}$.  Podemos  observar fácilmente  que este
vector no  el paralelo al  vector normal del  plano $\pi_1$.  Esto nos  indica que los  planos se cortan  y podría
tratarse de planos perpendiculares.  Para comprobar si es  el caso,  debemos mostrar que los vectores $\vv{n_2}$ y
$\vv{n_2}$ son ortogonales:
\[
  \vv{n_1} \cdot\vv{n_2}= \E{2,3,1}\cdot\E{4,6,1} = 8+18+1=27.
\]
Esto significa que los vectores normales no son ortogonales, por tanto $\pi_1$ y $\pi_2$ son dos planos que
simplemente se cortan.

Sabemos que el conjunto  intersección entre dos planos que se  cortan (sea de forma ortogonal o  no) es una recta.
Para encontrarla debemos buscar  los  puntos  $\E{x,y,z}$  que  verifiquen  ambas  ecuaciones cartesianas al mismo
tiempo, es decir, debemos resolver el sistema:
\[
  \systeme{2x+3y+z=1,4x+6y+z=2}
\]
Observemos que es un  sistema  no  homogéneo  de  2  ecuaciones  y  3  incógnitas.  Esperamos que el sistema tenga
infinitas soluciones ya que la recta consta de infinitos puntos.

Resolviendo el sistema:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rrr|r}
  2 & 3 &  1 &1 \\
  4 & 6 &  1 &2 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
  & \ro{(-4)R_1 + 2R_2 \to R_2}
% Segunda Matriz
\begin{sysmatrix}{rrr|r}
  2 & 3 &  1 &1 \\
  0 & 0 & -2 &0 \\
\end{sysmatrix}
  & \ro{(-1/2)R_2\to R_2}
% Tercera Matriz
\begin{sysmatrix}{rrr|r}
  2 & 3 &  1 &1 \\
  0 & 0 &  1 &0 \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
Sistema equivalente:
\[
  \systeme{2x+3y+z=1, z=0}\Rightarrow\systeme{2x+3y=1, z=0}
                          \Rightarrow \begin{cases}x=\frac{1-3y}{2} \\ z=0\end{cases}
\]
Entonces el vector solución es:
\[
  \E{x,y,z}= \E{\frac{1-3y}{2},y,0}= \E{\frac{1}{2}-\frac{3y}{2},y,0}
\]
Si igualamos componente a componente y hacemos que $y=t$ con $t\in\RR$, entonces obtenemos la ecuación paramétrica
de una recta
\[
  \begin{cases} x=\frac{1}{2}-\frac{3}{2}t \\ y = 0 + t \\z=0+0t\end{cases}
\]
Esto nos muestra que el conjunto intersección entre el plano $\pi_1$ y $\pi_2$ es la recta:
\[
  L:\quad\begin{cases} x=\frac{1}{2}-\frac{3}{2}t \\ y = t \\z=0\end{cases}
\]
%--------------------------------------------------------
\subsection{Notas de clase: Planos y Rectas}

\textbf{Posición relativa entre un plano y una recta:}
Una recta $L$ con vector director $\vv{v}$ y un plano $\pi$ con vector normal $\vv{n}$,  se pueden ubicar de forma
relativa de las siguiente maneras:
\begin{itemize}
  \item  Recta  paralela  al  plano:  El  vector director  de  recta  es  ortogonal  al  vector  normal  del plano
  ($\vv{v}\perp\vv{n}$) y  no tienen  puntos en  común.  Es decir,  la  recta se  encuentra fuera  del plano.  Ver
  figura~\ref{fig:PlanRecPar}.

  \item Recta  contenida en el plano:  El  vector director de la  recta es  ortogonal al  vector normal  del plano
  ($\vv{v}\perp\vv{n}$)   y  tienen   infinitos  puntos   en  común   (todos  los   puntos  de   la  recta).   Ver
  figura~\ref{fig:PlanRecCon}.
  \item Recta que  corta al plano:  El vector director de  recta no es ortogonal ni paralelo  al vector normal del
  plano ($\vv{v}\not\perp\vv{n}$) y tienen un solo punto en común.  Ver figura~\ref{fig:PlanRecCor}.
  \item Recta  perpendicular al plano:  El  vector director de  la recta es  paralelo al  vector normal  del plano
  ($\vv{v}\parallel\vv{n}$) y comparten un solo punto en común.  Ver figura~\ref{fig:PlanRecPerp}.
\end{itemize}

\deactivatequoting%
\begin{figure}[t!]
  \centering
  \subfloat[\label{fig:PlanRecPar}Recta paralela al plano.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          %xticklabels={},
          %yticklabels={},
          %zticklabels={},
        ]
        % Plano pi
        \addplot3[fill=blue!30]coordinates{(3,-3,0)(3,3,0)(-3,3,0)(-3,-3,0)(3,-3,0)};
        % Recta L
        \addplot3[ultra thick, <->, red!70]coordinates{(2.5,-2.5,2)(-2.5,2.5,2)}node[above, pos=0.5]{$L$};
        % Vector normal a pi
        \addplot3[ultra thick, ->,blue]coordinates{(1,1,0)(1,1,2.1)} node[right, pos=0.3]{$\vv{n}$};
        % Vector paralelo a L
        \addplot3[ultra thick, ->,red]coordinates{(0,0,1.2)(-2,2,1.2)} node[right, pos=0.9]{$\vv{v}$};
      \end{axis}
     \end{tikzpicture}
    }
  \subfloat[\label{fig:PlanRecCon}Recta contenida en el plano.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          %xticklabels={},
          %yticklabels={},
          %zticklabels={},
        ]
        % Plano pi
        \addplot3[fill=blue!30]coordinates{(3,-3,0)(3,3,0)(-3,3,0)(-3,-3,0)(3,-3,0)};
        % Recta L
        \addplot3[ultra thick, <->, red!70]coordinates{(2.5,-2.5,0)(-2.5,2.5,0)}node[above, pos=0.3]{$L$};
        % Vector normal a pi
        \addplot3[ultra thick, ->,blue]coordinates{(0,0,0)(0,0,2.1)} node[above, pos=1]{$\vv{n}$};
        % Vector paralelo a L
        \addplot3[ultra thick, ->,red]coordinates{(0,0,0.5)(-2,2,0.5)} node[above, pos=0.6]{$\vv{v}$};
      \end{axis}
     \end{tikzpicture}
    }
    \\
  \subfloat[\label{fig:PlanRecCor}Recta que corta al plano.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          %xticklabels={},
          %yticklabels={},
          %zticklabels={},
        ]
        % Plano pi parte 1
        \addplot3[fill=blue!30]coordinates{(0,-3,0)(-3,-3,0)(-3,3,0)(0,3,0)};
        % Recta L
        \addplot3[ultra thick, <->, red!70]coordinates{(2,-2,-3)(-2,2,3)}node[above, pos=0.3]{$L$};
        % Plano pi
        \addplot3[fill=blue!30]coordinates{(0,-3,0)(3,-3,0)(3,3,0)(-3,3,0)};
        % Vector normal a pi
        \addplot3[ultra thick, ->,blue]coordinates{(0,0,0)(0,0,2.1)} node[above, pos=1]{$\vv{n}$};
        % Vector paralelo a L
        \addplot3[ultra thick, ->,red]coordinates{(-1.5,1.5,1.5)(-2.5,2.5,3)} node[right, pos=0.6]{$\vv{v}$};
      \end{axis}
     \end{tikzpicture}
    }
  \subfloat[\label{fig:PlanRecPerp}Recta perpendicular al plano.]{%
    \begin{tikzpicture}
      \begin{axis}[
          view={120}{20},
          %width=0.5\textwidth,
          axis equal image,
          grid=major,
          xmin=-3,xmax=3,
          ymin=-3,ymax=3,
          zmin=-3,zmax=3,
          xlabel={$x$},
          ylabel={$y$},
          zlabel={$z$},
          %xticklabels={},
          %yticklabels={},
          %zticklabels={},
        ]
        % Plano pi parte 1
        \addplot3[fill=blue!30]coordinates{(0,-3,0)(-3,-3,0)(-3,3,0)(0,3,0)};
        % Recta L
        \addplot3[ultra thick, <->, red!70]coordinates{(1,1,-3)(1,1,4)}node[right, pos=1]{$L$};
        % Plano pi
        \addplot3[fill=blue!30]coordinates{(0,-3,0)(3,-3,0)(3,3,0)(-3,3,0)};
        % Vector normal a pi
        \addplot3[ultra thick, ->,blue]coordinates{(0,0,0)(0,0,2.1)} node[left, pos=0.9]{$\vv{n}$};
        % Vector paralelo a L
        \addplot3[ultra thick, ->,red]coordinates{(-1,1,0)(-1,1,2)} node[right, pos=0.9]{$\vv{v}$};
      \end{axis}
     \end{tikzpicture}
    }
    \caption{}
\end{figure}
\activatequoting%
%--------------------------------------------------------
\subsection{Ejercicio 3:}
Determine la posición relativa entre la recta:
\[
    L: \begin{cases}
      x&= -1 -3t\\
      y&= 1 +2t \\
      z&= 2
    \end{cases}
\]
y el plano que contiene los puntos $P\E{0,0,3}$, $Q\E{1,0,1}$ y $R\E{1,1,-2}$.  Encuentre el conjunto intersección
entre ambos.\\[1em]

El enunciado nos da las ecuaciones paramétricas de una recta $L$ y tres puntos contenidos en un plano~$\pi$.  Para
encontrar la posición relativa entre ambos,  primero debemos  encontrar la ecuación cartesiana del plano.  En este
sentido buscaremos  el vector normal al  plano $\vv{n}$.  Para esto utilizaremos  los puntos $P$,  $Q$  y $R$ para
obtener dos  vectores pertenecientes  al plano,  el vector  $\vv{PQ}$ y  el vector  $\vv{PR}$.  Luego calcularemos
$\vv{n}$ utilizando el producto cruz.
\[
  \vv{PQ} = \vv{OQ} - \vv{OP}= \E{1,0,1}-\E{0,0,3} =\E{1,0,-2}
\]
y
\[
  \vv{PR} = \vv{OR} - \vv{OP}= \E{1,1,-2}-\E{0,0,3} =\E{1,2,-5}
\]
El vector normal al plano es:
\begin{align*}
  \vv{n} &= \vv{PQ} \times\vv{PR}\\
         &= \begin{vmatrix}\vv{i}&\vv{j}&\vv{k}\\1&0&-2\\1&2&-5\end{vmatrix}\\
         &= \E{0+4}\vv{i}-\E{-5+2}\vv{j}+\E{2-0}\vv{k}\\
         &= 4\vv{i}+3\vv{j}+2\vv{k}
\end{align*}

Para encontrar la  ecuación cartesiana del plano  utilizaremos la Definición 4.5.1.  Proponemos  un punto genérico
perteneciente al plano $S\E{x,y,z}$, entonces:
\begin{align*}
  \vv{PS}\cdot\vv{n}&=0 && \text{Definición 4.5.1}\\
  \E{\vv{OS}-\vv{OP}}\cdot\vv{n}&=0 &&\text{El vector $\vv{PS}=\vv{OS}-\vv{OP}$.}\\
  \E{\E{x,y,z}-\E{0,0,3}}\cdot\vv{n}&=0 &&\text{Reemplazando los vectores puntos $\vv{OP}$ y $\vv{OS}$.}\\
    \E{x,y,z-3}\cdot\vv{n}&=0 &&\\
    \E{x,y,z-3}\cdot\E{4,3,2}&=0 &&\text{Reemplazando el vector normal $\vv{n}$.}\\
    4x+3y+2\E{z-3}&=0 &&\text{Realizando el producto punto.}\\
    4x+3y+2z-6&=0 &&\text{Ecuación del plano $\pi$.}
\end{align*}

Para determinar la ubicación relativa entre la recta $L$ y el plano $\pi$,  observemos la relación entre el vector
paralelo a la recta $\vv{v}=\E{-3,2,0}$ y el vector normal al plano $\vv{n}=\E{4,3,2}$.  Primero observamos que no
son paralelos (uno no es  múltiplo escalar del otro).  Esto nos indica que la recta  no es perpendicular al plano.
Segundo comprobemos si son ortogonales:
\[
  \vv{v}\cdot\vv{n}=\E{-3,2,0}\cdot\E{4,3,2}=-12+6+0=-6
\]
Lo anterior nos dice que  la recta no es paralela al plano,  ni tampoco está  contenida en él.  La única opción es
que la recta corta al plano.

Sabemos que la  intersección entre una recta que corta  a un plano es un único  punto del espacio.  Para encontrar
dicho punto debemos reemplazar las expresiones de $x$,  $y$ y $z$ de las ecuaciones paramétricas de la recta en la
ecuación cartesiana del plano: última:
\begin{align*}
  4x+3y+2z-6&=0 &&\\
  4\E{-1-3t} + 3\E{1+2t} +2\E{2}-6&=0 &&\text{Remplazando las ecuaciones paramétricas de la recta.}\\
  -4-12t + 3+6t +4-6&=0 &&\\
  -6t -3&=0 &&\\
     t &=-\frac{3}{6}=-\frac{1}{2}&&
\end{align*}

La última  ecuación muestra  que el  punto intersección  se alcanza  cuando $t=-1/2$.  Si  lo reemplazamos  en las
ecuaciones paramétricas de la recta obtenemos:
\[
  \begin{cases}
    x&= -1 -3t\\
    y&= 1 +2t \\
    z&= 2
  \end{cases}
  \Rightarrow
  \begin{cases}
    x&= -1 -3\E{-1/2}\\
    y&= 1 +2\E{-1/2} \\
    z&= 2
  \end{cases}
  \Rightarrow
  \begin{cases}
    x&= 1/2\\
    y&= 0 \\
    z&= 2
  \end{cases}
\]

El conjunto intersección entre la recta y el plano es el punto $\E{1/2,0,2}$.
%=================================================================================================================
%                                                      BIBLIOGRAFÍA
%=================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%=================================================================================================================
\end{document}
%=================================================================================================================
