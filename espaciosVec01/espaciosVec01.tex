%*****************************************************************************************************************
%*****************************************************************************************************************
%                                       ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                GUÍA DE ESTUDIO 9
%                                              ESPACIOS VECTORIALES I
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2020-05-05
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                     FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
 {\left(\begin{array}{@{}#1@{}}}
 {\end{array}\right)}

\newcommand{\ro}[1]{{\color{blue}\scriptscriptstyle{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio IX \\Espacios Vectoriales I\\[3pt]
\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                    OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de las secciones 5.1, 5.2 y 5.3:
\begin{itemize}
  \item \textbf{Sección 5.1 Definición  y propiedades básicas:} Presenta la definición de  espacio vectorial y los
  10 axiomas que deben cumplir.
  \item \textbf{Sección 5.2 Subespacios vectoriales:} Presenta la definición de subespacio vectorial y las
    condiciones que debe cumplir un conjunto de objetos para ser considerado como un subespacio.
  \item \textbf{Sección 5.3 Combinación lineal y espacio generado:} Presenta las definiciones de combinación
    lineal, espacio generado por un conjunto de vectores y conjunto generador de un espacio vectorial.
\end{itemize}
%=================================================================================================================
%                                                   SECCIÓN 5.1
%=================================================================================================================
\section{Sección 5.1 Definición y propiedades básicas:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 5.1.1: Espacio vectorial real.
  \item Axiomas de un espacio vectorial.
  \item Teorema 5.1.1: Resultados sobre espacios vectoriales.
  \item Resumen 5.1.
\end{itemize}
Realice los ejemplos: 5.1.1, 5.1.2, 5.1.3, 5.1.4, 5.1.5, 5.1.6 y 5.1.11.
%--------------------------------------------------------
\subsection{Notas de clase: Espacios vectoriales}
\textbf{Espacios vectoriales:}
Es importante  mencionar que  en este  cursado solo nos  enfocaremos en  espacios vectoriales  sobre el  cuerpo de
vectores  en $\RR^n$.  Es  decir,  prestaremos especial  atención a  esos espacios  y subespacios  vectoriales con
interpretación geométrica:  puntos,  rectas y planos definidos en el plano ($\RR^2$), el espacio ($\RR^3$) o en el
hiperespacio ($\RR^n$).

La  definición  5.1.1 nos  presenta  un espacio  vectorial  $V$ como  un  conjunto  de  objetos  (vectores)  y dos
operaciones:  suma y multiplicación por  un escalar,  que cumplen los 10 axiomas.  Es  decir,  para afirmar que un
conjunto de objetos es un espacio vectorial es necesario verificar:
\begin{enumerate}
  \item Que esté definida una operación de suma entre dos objetos.
  \item Que esté definida una operación de multiplicación por un escalar entre un objeto y un número real.
  \item Que el conjunto de objetos y las dos operaciones cumplan con los 10 axiomas.
\end{enumerate}

Supongamos que contamos con un conjunto de objetos llamado  $V$ y con las operaciones de suma y multiplicación por
un escalar, entonces los 10 axiomas que se deben cumplir para considerar a $V$ un espacio vectorial son:
\begin{enumerate}
  \item  Si  $\vv{x}\in  V$  y  $\vv{y}\in  V$,   entonces   $\vv{x}  +  \vv{y}\in  V$.\\  Este  axioma  se  llama
  \textbf{cerradura bajo la suma}  y  nos  indica  que  si  sumo  dos  vectores  de  $V$ su resultado también debe
  pertenecer a $V$.
  \item Para todo $\vv{x}$, $\vv{y}$ y $\vv{z}\in V$, $\E{\vv{x} + \vv{y}} + \vv{z} = \vv{x}+\E{\vv{y}+\vv{z}}$.\\
  Este axioma se denomina  \textbf{ley asociativa de la suma de  vectores} y nos dice que el  resultado de la suma
  no depende de la forma en que agrupemos los términos.
  \item Existe un vector  $\vv{0}\in V$ tal que para todo $\vv{x}\in  V$,  $\vv{x} + \vv{0}=\vv{x}$.\\ Este axioma
  pide que  en nuestro  conjunto de objetos/vectores  se encuentre uno  que no  tenga efectos  en la  operación de
  suma.  A este elemento lo llamamos vector cero o idéntico aditivo.
  \item Si $\vv{x}\in V$, existe un vector $-\vv{x}\in V$ tal que $\vv{x} + \E{-\vv{x}}=\vv{0}$.\\ Este axioma nos
  indica que si nuestro  grupo  de  objetos  contiene  el  elemento  $\vv{x}$,  también  debe contener el elemento
  $-\vv{x}$ (inverso aditivo de $\vv{x}$).
  \item $\vv{x}$ y  $\vv{y}  \in  V$,  entonces  $\vv{x}  +  \vv{y}  =  \vv{y}+\vv{x}$.\\  Este axioma se denomina
  \textbf{ley conmutativa de la suma  de vectores} y nos dice que el resultado de la  suma no depende del orden en
  que sumemos los términos.
  \item  $\vv{x}\in   V$  y  $\alpha$  un   escalar,   entonces  $\alpha\vv{x}\in  V$.\\  Este   axioma  se  llama
  \textbf{cerradura bajo la multiplicación  por  un  escalar}  y  nos  indica  que  si  un vector pertenece a $V$,
  entonces la multiplicación de este vector por un real $\alpha$ también debe pertenecer a $V$.
  \item Si $\vv{x},\vv{y}\in V$  y $\alpha\in\RR$,  entonces $\alpha\E{\vv{x}+\vv{y}}=\alpha\vv{x}+ \alpha\vv{y}$.
  Este axioma se denomina  \textbf{primera ley distributiva}.  Notar que el producto por  un escalar se distribuye
  bajo la suma de vectores.
  \item Si  $\vv{x}\in  V$  y  $\alpha,\beta\in\RR$,  entonces $\E{\alpha+\beta}\vv{x}=\alpha\vv{x}+ \beta\vv{x}$.
  Este  axioma  se  denomina  \textbf{segunda ley  distributiva}.  Notar  que,  a  diferencia  de  la  primera ley
  distributiva, la suma de escalares se distribuye bajo el producto por un vector.
  \item  Si $\vv{x}\in  V$ y  $\alpha,\beta\in\RR$,  entonces $\alpha\E{\beta\vv{x}}=\E{\alpha\beta}\vv{x}$.  Este
  axioma indica que el resultado del producto escalar no depende de como se asocien los escalares.
  \item Para  cada vector $\vv{x}\in V$,  $1\vv{x}=\vv{x}$.  Este  axioma nos pide  que dentro  del cuerpo  de los
  escalares se encuentre un elemento  que no altere el producto por un escalar,  en el  caso de los números reales
  sería el 1.
\end{enumerate}

%--------------------------------------------------------
\subsection{Ejercicio 1:}
Determinar si el conjunto de vectores $V=\W{\E{x,y,z}\in\RR^3:\,x-2y+z=2}$ es un espacio vectorial.\\[0.5em]

Podemos  ver que el  conjunto $V$ está  conformado por todos  los puntos  $\E{x,y,z}$ del  espacio que
cumplen con la ecuación  $x-2y+z=2$  (ecuación  de  un  plano  en  el  espacio).  Recordemos  que sobre el espacio
($\RR^{3}$) están definidas las  operaciones  de  suma  de  vectores  y  multiplicación  por un escalar.  Para dos
vectores del espacio $\vv{v}=\E{x_1,y_1,z_1}$ y $\vv{u}=\E{x_2,y_2,z_2}$, la suma es:
\[
  \vv{v} + \vv{u} = \E{x_1,y_1,z_1}+ \E{x_2,y_2,z_2} = \E{x_1+x_2,y_1+y_2,z_1+z_2}
\]
y el producto por un escalar $\alpha$:
\[
  \alpha\vv{v}= \alpha\E{x_1,y_1,z_1}=\E{\alpha x_1,\alpha y_1,\alpha z_1}.
\]

Hasta el momento tenemos definidos un conjunto de  vectores ($V$) y las dos operaciones esenciales.  Ahora debemos
comprobar si se cumplen  los 10 axiomas.  Comenzaremos por el de cerradura  bajo la suma.  Buscaremos dos vectores
pertenecientes a $V$, por ejemplo:
\begin{align*}
  \vv{v} &= \E{0,0,2} &&\text{Dando los valores $x=0$, $y=0$ y despejando $z$ de la ec. del plano.}\\
  \vv{u} &= \E{1,0,1} &&\text{Dando los valores $x=1$, $y=0$ y despejando $z$ de la ec. del plano.}
\end{align*}
Estos dos puntos  del espacio verifican la ecuación  del plano,  por tanto pertenecen a  él.  El primer axioma nos
exige que la suma de elementos de $V$ deben pertenecer también a $V$:
\[
 \vv{v}+ \vv{u}= \E{0,0,2} + \E{1,0,1} = \E{1,0,3}
\]
si reemplazamos este punto en la ecuación del plano:
\begin{align*}
  x-2y+z&=2 \\
  1-2\E{0}+3&=2 \\
    4&=2
\end{align*}
llegamos a un absurdo,  indicando que la suma  $\vv{v}+\vv{u}$ no pertenece a $V$ ($\vv{v}+\vv{u}\notin V$).  Esto
muestra que $V$ no es cerrado bajo la suma y tampoco un espacio vectorial.

Otra forma de  mostrar que $V$ no es  un espacio vectorial,  es observar que la  ecuación $x-2y+z=2$ representa un
plano que no pasa por el origen de coordenadas,  es  decir $V$ no contiene al elemento nulo de $\RR^3$,  el vector
$\vv{0}$.  Esto es fácil de mostrar reemplazando el vector $\vv{0}$ en la ecuación del plano:
\begin{align*}
  x-2y+z&=2 \\
  0-2\E{0}+0&=2 \\
    0&=2
\end{align*}
Esto nos muestra que no existe un idéntico aditivo (vector cero) en $V$, descalificándolo como espacio vectorial.
%=================================================================================================================
%                                                    SECCIÓN 5.2
%=================================================================================================================
\newpage
\section{Sección 5.2 Subespacios vectoriales:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 5.2.1: Subespacios vectoriales.
  \item Teorema 5.2.4: Subespacio vectorial, muestra las reglas que debe cumplir un conjunto de objetos para
  ser considerado un subespacio vectorial.
  \item Resumen 5.2.
\end{itemize}
Realice los ejemplos: 5.2.1, 5.2.2, 5.2.3, 5.2.4, 5.2.5, 5.2.6 y 5.2.13.
%--------------------------------------------------------
\subsection{Notas de clase: Subespacios vectoriales}
\textbf{Subespacios vectoriales:}
Algunos subconjuntos de un espacio vectorial $V$ son en sí mismos un espacio vectorial. Por ejemplo, si $V=\RR^2$,
entonces una recta $L$ que pasa por el origen es un subespacio vectorial. En otras palabras, El conjunto de puntos
que conforma la recta $L$ también cumple con los 10 axiomas.

Para que un subconjunto  de vectores $H$ (no vacio),  de un espacio  vectorial $V$,  sea considerado un subespacio
vectorial es necesario que cumpla solo con dos reglas:
\begin{enumerate}
  \item Cerradura bajo la suma: Si $\vv{x},\,\vv{y}\in H$, entonces $\vv{x}+\vv{y}\in H$.
  \item Cerradura bajo  el producto escalar:  Si $\vv{x}\in H$,  entonces  $\alpha\vv{x}\in H$,  para todo escalar
  $\alpha$.
\end{enumerate}
Es natural  preguntarse por  que no es  necesario mostrar que  el subespacio  $H$ cumple  con los  10 axiomas?  La
respuesta es que $H$  es un subconjunto de un espacio  vectorial $V$ y por tanto hereda  parte de sus propiedades.
Es decir, $H$ cumple automaticamente con 8 de los 10 axiomas, los únicos axiomas que se deben verificar son los de
cerradura (reglas 1 y 2).\\[0.5em]

\textbf{Subespacios propios:}
Para cada espacio  vectorial  $V$  podemos  definir  varios  subespacios,  por  ejemplo el subespacio $\W{0}$.  El
conjunto está formado únicamente  por el elemento nulo de $V$  es un subespacio (cumple con las  reglas 1 y 2).  Por
otro lado,  $V$ es en si mismo un subespacio,  es decir consideramos a $V$ un espacio vectorial y un subespacio al
mismo tiempo.  Esto se debe a que $V$ cumple con la definición de espacio vectorial y de subespacio.

Sin embargo, existen otros subespacios asociados a $V$, estos se denominan subespacios propios, por ejemplo:
\begin{itemize}
  \item Los subespacios propios de $\RR^2$ son: rectas que pasan por el origen.
  \item Los subespacios propios de $\RR^3$ son: rectas que pasan por el origen y planos que pasan por el origen.
\end{itemize}
El espacio vectorial de los  números reales ($\RR$) no tiene subespacios propios ya  que sus únicos subespacios son
$\W{0}$ y $\RR$ mismo.

%--------------------------------------------------------
\subsection{Ejercicio 2:}
Muestre que la recta $L:\, \begin{cases} x=1-t\\y=3t\\z=t\end{cases}$ no es un subespacio de $\RR^3$.\\[0.5em]

Primero observemos que $L$ es una recta que no pasa por el origen de coordenadas,  esto nos indica automáticamente
no un subespacio de $\RR^3$, sin embargo vamos a probarlo.

Antes de resolver este tipo de ejercicios es necesario responder primero: ¿quien es el espacio vectorial?  Y ¿cual
es el subconjunto de elementos,  de ese espacio,  candidato a ser subespacio vectorial?  En este caso,  el espacio
vectorial es  el espacio  ($V=\RR^3$) y  el subconjunto es  la recta  ($H=L$).  Probaremos primero  que $H$  no es
cerrado bajo la multiplicación por un escalar.  Es fácil obtener un elemento de $H$, dando el valor $t=1$:
\[
  \begin{cases} x=1-t\\y=3t\\z=t\end{cases}\Rightarrow
  \begin{cases} x=1-1\\y=3\\z=1\end{cases}\Rightarrow
  \begin{cases} x=0\\y=3\\z=1\end{cases}\Rightarrow
  \vv{v} = \E{0,3,1}
\]
Si elegimos un escalar cualquiera, por ejemplo $\alpha =2$, vemos que:
\[
 \alpha\vv{v}= 2\E{0,3,1} = \E{0,6,2}
\]
si reemplazamos este punto en la ecuación de la recta:
\[
  \begin{cases} x=1-t\\y=3t\\z=t\end{cases}\Rightarrow
  \begin{cases} 0=1-t\\6=3t\\2=t\end{cases}\Rightarrow
  \begin{cases} t=1\\ t=2\\ t=2\end{cases}
\]
Las tres últimas ecuaciones  nos plantean una inconsistencia:  la primera dice que $t=1$  y las dos siguientes que
$t=2$.  Esto  muestra  que  $\alpha\vv{v}$  no  pertenece a  la  recta,  es  decir,  $H$  no  es  cerrado  bajo la
multiplicación por un escalar y por lo tanto no es un subespacio de $\RR^3$.
%=================================================================================================================
%                                                   SECCIÓN 5.2
%=================================================================================================================
\section{Sección 5.3: Combinación lineal y espacio generado:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 5.3.1: Combinación lineal.
  \item Definición 5.3.2: Conjunto generador.
  \item Definición 5.3.3: Espacio generado por un conjunto de vectores.
  \item Teorema 5.3.1: El espacio generado por vectores de un espacio vectorial es un subespacio vectorial.
  \item Teorema 5.3.2.
\end{itemize}
Realice los ejemplos: 5.3.1, 5.3.4 y 5.3.8.
%--------------------------------------------------------
\subsection{Notas de clase: Combinación lineal}
\textbf{Combinación lineal:}
La   combinación   lineal  es   una   operación  entre   vectores   y   su   resultado   es   otro  vector:   Sean
$\vv{v_1},\vv{v_2},\dots,\vv{v_n}$ vectores en un espacio vectorial $V$.  Entonces cualquier vector de la forma:
\begin{equation*}
    a_1\vv{v_1}+a_2\vv{v_2}+\dots+a_n\vv{v_n} \quad\text{con}\quad a_1,a_2,\dots,a_n\in\RR
\end{equation*}
se denomina una combinación lineal de los vectores $\vv{v_1},\vv{v_2},\dots,\vv{v_n}$.
%--------------------------------------------------------
\subsection{Ejercicio 3:}
Muestre que el vector $\vv{w}=\E{0,0,1}$ es combinación lineal de los vectores:
\[
  \vv{v_1}=\begin{pmatrix}1\\2\\-1\end{pmatrix},\,
  \vv{v_2}=\begin{pmatrix}1\\1\\1\end{pmatrix}\,\,\text{y}\,\,
  \vv{v_3}=\begin{pmatrix}0\\2\\3\end{pmatrix}.
\]

Si $\vv{w}$ es combinación lineal de $\vv{v_1}$, $\vv{v_2}$ y $\vv{v_3}$, entonces se debe cumplir que:
\begin{align*}
  \vv{w} &=a\vv{v_1}+b\vv{v_2}+c\vv{v_3} &&\text{con $a$, $b$ y $c\in\RR$.} \\
  \begin{pmatrix}0\\0\\1\end{pmatrix} &= a\begin{pmatrix}1\\2\\-1\end{pmatrix}
    +b\begin{pmatrix}1\\1\\1\end{pmatrix} + c\begin{pmatrix}0\\2\\3\end{pmatrix} &&\\
      \begin{pmatrix}0\\0\\1\end{pmatrix} &= \begin{pmatrix}a+b\\2a+b+2c\\-a+b+3c\end{pmatrix} &&\text{Mult. por
        escalar y suma de vectores.}
\end{align*}
al igualar estos dos vectores, componente a componente, obtenemos el sistema no homogéneo:
\[
  \begin{cases}a+b=0\\2a+b+2c=0\\-a+b+3c=1\end{cases}
\]
Si este  sistema tiene  solución (única  o infinitas)  indica que  $\vv{w}$ es  combinación lineal  de $\vv{v_1}$,
$\vv{v_2}$ y $\vv{v_3}$.  Por otro lado,  si este no tiene  solución entonces $\vv{w}$ no es combinación lineal de
esos vectores.  Resolvamos el sistema:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rrr|r}
  1  & 1 & 0  & 0 \\
  2  & 1 & 2  & 0 \\
  -1 & 1 & 3  & 1 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
&\!\begin{aligned}
  & \ro{-2R_1+R_2\to R_2}\\
  & \ro{R_1+R_3\to R_3}\\
\end{aligned}
% Segunda Matriz
\begin{sysmatrix}{rrr|r}
  1 &  1  & 0  & 0 \\
  0 & -1  & 2  & 0 \\
  0 &  2  & 3  & 1 \\
\end{sysmatrix}
% Operaciones por renglón (solo una operación)
& \ro{2R_2+R_3\to R_3}
% Tercera Matriz
\begin{sysmatrix}{rrr|r}
  1 &  1  & 0  & 0 \\
  0 & -1  & 2  & 0 \\
  0 &  0  & 7  & 1 \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
Sistema equivalente:
\begin{alignat*}{3}
  \systeme{a+b=0,-b+2c=0,7c=1}
\end{alignat*}
de donde obtenemos:
\begin{alignat*}{3}
  \begin{cases}
    a &= -2/7 \\
    b &= 2/7 \\
    c &= 1/7
  \end{cases}
\end{alignat*}
la  única  solución  del  sistema. En conclusión, $\vv{w}$ es combinación lineal de los vectores $\vv{v_1}$,
$\vv{v_2}$ y $\vv{v_3}$, de hecho:
\begin{align*}
  \vv{w} &=-\frac{2}{7}\vv{v_1}+\frac{2}{7}\vv{v_2}+\frac{1}{7}\vv{v_3}\\
  \begin{pmatrix}0\\0\\1\end{pmatrix} &= -\frac{2}{7}\begin{pmatrix}1\\2\\-1\end{pmatrix}
    +\frac{2}{7}\begin{pmatrix}1\\1\\1\end{pmatrix} +\frac{1}{7}\begin{pmatrix}0\\2\\3\end{pmatrix}.
\end{align*}

%--------------------------------------------------------
\subsection{Notas de clase: Espacio generado}
\textbf{Espacio generado por un conjunto de vectores:}
Llamamos espacio generado al conjunto de todos los  vectores que podemos generar mediante la combinación lineal de
un  grupo  de  vectores.  De  una  forma  más  formal,  el  espacio  generado  por  el  conjunto  de  $k$ vectores
$\vv{v_1},\vv{v_2},\dots,\vv{v_k}$, es:
\begin{equation}
   \text{gen}\W{\vv{v_1},\vv{v_2},\dots,\vv{v_k}}=\W{\vv{v}/\,\,\vv{v}=a_1\vv{v_1}+a_2\vv{v_2}+\dots+a_k\vv{v_k}}.
   \label{eq:01}
\end{equation}
donde   $a_1,a_2,\dots,a_k\in\RR$   son  escalares.   Por   otro   lado,   llamamos   al   conjunto   de  vectores
$\W{\vv{v_1},\vv{v_2},\dots,\vv{v_k}}$ conjunto generador.

Ahora,  podríamos preguntarnos si un espacio generado es un espacio vectorial,  es decir,  si el conjunto de todas
las combinaciones lineales  de un grupo de  vectores cumple con los  10 axiomas.  La respuesta se  encuentra en el
Teorema 5.3.3, que dice que el espacio generado por un conjunto de vectores, perteneciente a un espacio vectorial,
es un subespacio vectorial.  Un ejemplo son los planos que  pasan por el origen en $\RR^3$.  Todos los vectores en
esos planos  se pueden generar  como combinación lineal  de dos vectores  de $\RR^3$.  Por tanto,  el  plano es un
subespacio del espacio.

Otro  resultado   interesante  es  el   que  se  plantea   en  el  Teorema   5.3.2.   En  él,   se  dice   que  si
$\W{\vv{v_1},\vv{v_2},\dots,\vv{v_k}}$  es  un   conjunto  generador  de  un   espacio  vectorial  $V$,   entonces
$\W{\vv{v_1},\vv{v_2},\dots,\vv{v_k},\vv{v_{k+1}}}$ también es  un conjunto generador de  $V$.  En otras palabras,
si agregamos un  o más vectores (del mismo  espacio vectorial) a un conjunto  generador,  ese nuevo conjunto sigue
siendo generador.

%--------------------------------------------------------
\subsection{Ejercicio 3:}
Considere el conjunto de vectores: $\Omega =\W{\vv{v_1};\vv{v_2}}=\W{\E{1,0,1};\E{1,1,0}}$.
\begin{enumerate}[label=\alph*.]
  \item Determine el espacio generado por $\Omega$.
  \item Muestre que el vector $\vv{v_3}=\E{-1,-2,1}$ pertenece a $H=\gen\W{\vv{v_1};\vv{v_2}}$.
  \item Muestre que $\gen\W{\vv{v_1};\vv{v_2};\vv{v_3}} = \gen\W{\vv{v_1};\vv{v_2}}=H$.
\end{enumerate}
\textbf{a.} El enunciado  nos presenta un conjunto,  llamado $\Omega$,  compuesto por  dos vectores de $\RR^3$.  A
este conjunto  lo llamamos conjunto  generador.  Antes de encontrar cual  es el subespacio  generado por $\Omega$,
podemos inferir los posibles  resultados.  Observemos que $\vv{v_1}$ y $\vv{v_2}$ son vectores  de $\RR^3$ y según
el Teorema~5.3.1, el espacio generado por estos vectores debe ser un subespacio de $\RR^3$, es decir:
\[
  H =\gen\W{\vv{v_1};\vv{v_2}}
\]
podría ser una  recta que  pasa por el  origen o un  plano que  pasa por  el origen.  Por  definición,  $H$ está
compuesto por todas las posibles combinaciones lineales de $\vv{v_1}$ y $\vv{v_2}$~(ecuación~(\ref{eq:01})).  Para
encontrar  $H$ podemos  plantear un  vector genérico  $\vv{v}=\E{x,y,z}$ perteneciente  a $H$  y buscar  todas las
posibles combinaciones lineales de los vectores $\vv{v_1}$ y $\vv{v_2}$:
\begin{align*}
  \vv{v} &= a\vv{v_1} + b\vv{v_2} && \text{Comb. lineal de $\vv{v_1}$ y $\vv{v_2}$.}\\
  \begin{pmatrix}x\\y\\z\end{pmatrix}&= a\begin{pmatrix}1\\0\\1\end{pmatrix}+b\begin{pmatrix}1\\1\\0\end{pmatrix}
    &&\\
  \begin{pmatrix}x\\y\\z\end{pmatrix}&= \begin{pmatrix}a\\0\\a\end{pmatrix}+\begin{pmatrix}b\\b\\0\end{pmatrix}=
      \begin{pmatrix}a+b\\b\\a\end{pmatrix} &&
\end{align*}
Esta igualdad entre vectores nos lleva al siguiente sistema (las incógnitas son $a$ y $b$):
\[
  \begin{cases}a+b=x\\b=y\\a=z\end{cases}
\]
Este sistema no homogéneo debe tener infinitas soluciones ya  que cada solución corresponde a una de las infinitas
combinaciones lineales posibles.  Resolviendo el sistema:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rr|r}
  1 & 1 & x \\
  0 & 1 & y \\
  1 & 0 & z \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
  & \ro{R_3\leftrightarrow R_1}
% Segunda Matriz
\begin{sysmatrix}{rr|l}
  1 & 0 & z \\
  0 & 1 & y \\
  1 & 1 & x \\
\end{sysmatrix}
% Operaciones por renglón (sólo una operación)
  & \ro{\E{-1}R_1+R_3\to R_3}
% Tercera Matriz
\begin{sysmatrix}{rr|l}
  1 & 0 & z \\
  0 & 1 & y \\
  0 & 1 & x-z \\
\end{sysmatrix}
  & \ro{\E{-1}R_2+R_3\to R_3}
% Cuarta Matriz
\begin{sysmatrix}{rr|l}
  1 & 0 & z \\
  0 & 1 & y \\
  0 & 0 & x-z-y \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
Sistema equivalente:
\[
  \begin{cases}a=x\\b=y\\0b=x-z-y\end{cases}
\]
Es importante detenernos  para  interpretar  el  sistema  equivalente.  Como  ya  dijimos  este sistema debe tener
infinitas soluciones.  Si observamos  la última ecuación del sistema equivalente,  podemos  ver que para que el
sistema tenga solución se debe verificar que:
\[
  x-z-y=0.
\]
Esto implica que los únicos vectores $\vv{v}=\E{x,y,z}$ que se pueden formar como combinación lineal de $\vv{v_1}$
y $\vv{v_2}$ son aquellos que cumplen con la condición $x-z-y=0$. En conclusión, el espacio generado por estos
vectores es:
\[
  H =\gen\W{\vv{v_1};\vv{v_2}}= \W{\vv{v}=\E{x,y,z}\in\RR^3\,/\, x-y-z=0}
\]
Además, podemos ver que $H$ es un plano que pasa por el origen.
\newpage
\textbf{b.} Este inciso  nos  pide  probar  que  el  vector  $\vv{v_3}=\E{-1,-2,1}$  pertenece al plano $H$.  Para
esto, basta con verificar si $\vv{v_3}$ cumple con la condición:
\begin{align*}
  x-y-z&=0\\
  -1-\E{-2}-1&=0\\
  0&=0
\end{align*}
Mostrando que $\vv{v_3}\in H$.\\[0.5em]

\textbf{c.} Se nos pide probar que:
\[
  \gen\W{\vv{v_1};\vv{v_2};\vv{v_3}} = \gen\W{\vv{v_1};\vv{v_2}}=H
\]
Es decir, probar que el conjunto de vectores $\W{\vv{v_1};\vv{v_2};\vv{v_3}}$ también es un conjunto generador de
$H$. Aplicando la misma metodología del primer inciso podemos plantear: Sea
$\vv{v}=\E{x,y,z}\in\gen\W{\vv{v_1};\vv{v_2};\vv{v_3}}$, entonces
\begin{align*}
  \vv{v} &= a\vv{v_1} + b\vv{v_2}+c\vv{v_3} && \text{Comb. lineal de $\vv{v_1}$, $\vv{v_2}$ y $\vv{v_3}$.}\\
  \begin{pmatrix}x\\y\\z\end{pmatrix}&= a\begin{pmatrix}1\\0\\1\end{pmatrix}+b\begin{pmatrix}1\\1\\0\end{pmatrix}
    +c\begin{pmatrix}-1\\-2\\1\end{pmatrix}&&\\
\end{align*}
que nos lleva al sistema:
\[
  \begin{cases}a+b-c=x\\b-2c=y\\a+c=z\end{cases}
\]
Resolviendo:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rrr|l}
  1 & 1 & -1 & x \\
  0 & 1 & -2 & y \\
  1 & 0 &  1 & z \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
  & \ro{-R_1+R_3\to R_3}
% Segunda Matriz
\begin{sysmatrix}{rrr|l}
  1 &  1 & -1 & x \\
  0 &  1 & -2 & y \\
  0 & -1 &  2 & z-x \\
\end{sysmatrix}
% Operaciones por renglón (sólo una operación)
  & \ro{R_2+R_3\to R_3}
% Tercera Matriz
\begin{sysmatrix}{rrr|l}
  1 &  1 & -1 & x \\
  0 &  1 & -2 & y \\
  0 &  0 &  0 & z-x+y \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
Sistema equivalente:
\[
  \begin{cases}a + b -c=x\\b -2c=y\\0c=z-x+y\end{cases}
\]
Podemos ver que la última ecuación nos plantea la condición $z-x+y=0$ que es lo mismo que $x-z-y=0$. Esto nos
muestra que:
\[
  H =\gen\W{\vv{v_1};\vv{v_2};\vv{v_3}}= \W{\vv{v}=\E{x,y,z}\in\RR^3\,/\, x-y-z=0}=H
\]

Este resultado se plantea  en el Teorema 5.3.2 y una  de sus consecuencias es que para  un mismo espacio vectorial
podemos encontrar infinitos conjuntos generadores.

%===================================================================================================================
%                                                      BIBLIOGRAFÍA
%===================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%===================================================================================================================
\end{document}
%===================================================================================================================
