%*****************************************************************************************************************
%*****************************************************************************************************************
%                                       ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                GUÍA DE ESTUDIO 10
%                                              ESPACIOS VECTORIALES II
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2020-05-12
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                     FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
 {\left(\begin{array}{@{}#1@{}}}
 {\end{array}\right)}

\newcommand{\ro}[1]{{\color{blue}\scriptscriptstyle{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio X \\Espacios Vectoriales II\\[3pt]
\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                    OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de las secciones 5.4, 5.5 y 5.7:
\begin{itemize}
  \item \textbf{Sección 5.4: Independencia lineal.}
  \item \textbf{Sección 5.5: Bases y dimensión.}
  \item \textbf{Sección 5.7: Rango, nulidad, espacio renglón y espacio columna.}
\end{itemize}
%=================================================================================================================
%                                                   SECCIÓN 5.4
%=================================================================================================================
\section{Sección 5.4 Independencia lineal:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 5.4.1: Dependencia e independencia lineal.
  \item Teorema 5.4.1.
  \item Teorema 5.4.2.
  \item Corolario 5.4.1.
  \item Teorema 5.4.3.
  \item Teorema 5.4.4.
  \item Teorema 5.4.5.
  \item Teorema 5.4.6: Teorema resumen (importante).
  \item Teorema 5.4.7.
\end{itemize}
Realice los ejemplos: 5.4.1, 5.4.2, 5.4.3, 5.4.4, 5.4.5, 5.4.6 y 5.4.7.
%--------------------------------------------------------
\subsection{Notas de clase: Independencia lineal}
La dependencia lineal  y la independencia lineal  son propiedades complementarias de un  conjunto de vectores,  es
decir,  si un  conjunto de  vectores es  linealmente independiente  (L.I) entonces  no puede  ser al  mismo tiempo
linealmente dependiente (L.D) y viceversa.

La definición 5.4.1 nos presenta  la condición que debe cumplir un conjunto de  vectores para ser considerado L.D.
Sin embargo, es más cómodo presentar la condición específica para decir que un conjunto de vectores es L.I.

Sean $\vv{v_1},\vv{v_2},\dots,\vv{v_n}$,  $n$  vectores de  un espacio  vectorial $V$.  Entonces  se dice  que los
vectores son {\textbf{Linealmente Independientes}} si la ecuación:
\begin{equation}
  c_1\vv{v_1}+c_2\vv{v_2}+\dots+c_n\vv{v_n}=\vv{0} \label{eq:01}
\end{equation}
se satisface \textbf{solo cuando} $c_1=\dots=c_n=0$.

Esta  definición dice  que un  conjunto de  vectores $\W{\vv{v_1},\vv{v_2},\dots,\vv{v_n}}$  es L.I,  si  la única
combinación  lineal  que  puede  producir  el  vector  nulo  ($\vv{0}$),  es  aquella  donde  todos  los escalares
$c_1,c_2,\dots,c_n$ son iguales a cero.

Por otro lado, los Teoremas 5.4.1, 5.4.2 presentan algunos resultados para determinar si un conjunto particular de
vectores es L.I o L.D:
\begin{itemize}
  \item Teorema 5.4.1: Su resultado muestra que si dos vectores son múltiplos escalares (vectores paralelos),
    entonces son L.D.
  \item Teorema 5.4.2:  Nos enseña que un conjunto de $n$ vectores pertenecientes a $\RR^m$ es L.D si $n>m$.  Por
  ejemplo, tres vectores ($n=3$) pertenecientes a $\RR^2$ ($m=2$).
\end{itemize}

El procedimiento  general para  determinar si  un conjunto  de vectores  es L.I  o L.D  consiste en  encontrar las
soluciones  de un  sistema homogéneo  (Teorema 5.4.3).  Si  consideramos  $n$  vectores  de  $\RR^{m}$  es posible
representar la ecuación~(\ref{eq:01}) como el siguiente sistema homogéneo:
\[
  c_1\vv{v_1}+c_2\vv{v_2}+\dots+c_n\vv{v_n}=\vv{0}
\]
\begin{center}
\begin{tikzpicture}[baseline, ampersand replacement=\&]
    \matrix [matrix of math nodes, left delimiter={[}, right delimiter={]}, column sep=0.25cm,
             row sep=15\pgflinewidth,](A)
     {%
      v_{11} \& v_{12} \&\cdots \&v_{1n}\\
      v_{21} \& v_{22} \&\cdots \&v_{2n}\\
      \vdots \& \vdots \&\cdots \&\vdots\\
      v_{m1} \& v_{m2} \&\cdots \&v_{mn}\\
    };

    \node[font=\large, anchor=south, xshift=-0.00cm, yshift = 0.1cm] at (A-1-1.north) {$\vv{v_1}$};
    \node[font=\large, anchor=south, xshift=-0.00cm, yshift = 0.1cm] at (A-1-2.north) {$\vv{v_2}$};
    \node[font=\large, anchor=south, xshift=-0.00cm, yshift = 0.1cm] at (A-1-4.north) {$\vv{v_n}$};

    \matrix [matrix of math nodes, left delimiter={[}, right delimiter={]}, column sep=0.45cm,
             row sep=15\pgflinewidth, right=0.5cm of A](C)
     {c_{1}\\ c_{2}\\\vdots\\c_{n}\\};

    \node[font=\large, right=0.3cm of C]{$=$};

    \matrix [matrix of math nodes, left delimiter={[}, right delimiter={]}, column sep=0.45cm,
             row sep=15\pgflinewidth, right=1.2cm of C](O)
     {0\\ 0\\\vdots\\0\\};

  \draw [decorate,decoration={brace,amplitude=0.3cm,mirror}, thick] (A-4-1.south west) -- (A-4-4.south east)
    node[below=1cm,pos=0.5]{$A$};

  \draw [decorate,decoration={brace,amplitude=0.3cm,mirror}, thick] (C-4-1.south west) -- (C-4-1.south east)
    node[below=1cm,pos=0.5]{$\vv{c}$};
\end{tikzpicture}\\
\end{center}

En este sistema, cada columna de la matriz de coeficientes $A$ es uno de los $n$ vectores y las incógnitas son los
escalares de la  combinación lineal ($c_1,c_2,\dots,c_n$).  Las soluciones de  este sistema ($A\vv{c}=\vv{0}$) nos
indican si los vectores son L.I o L.D:
\begin{itemize}
  \item Solución única $\vv{c}=\vv{0}$:  Todos los escalares $c_1,c_2,\dots,c_n$ son iguales a cero, por tanto los
  vectores son L.I (Teorema 5.4.4).
  \item Infinitas  soluciones:  Existe más de  una combinación lineal  genera al  vector $\vv{0}$,  por  tanto los
  vectores son L.D.
\end{itemize}

Siempre  es  posible  encontrar la  solución  del  sistema  $A\vv{c}=\vv{0}$  mediante  eliminación Gaussiana.  No
obstante,  si la matriz $A$ es cuadrada ($n=m$, tenemos $n$ vectores de $\RR^{n}$) podemos calcular el determinante
de $A$.  Si $\det\E{A}\neq0$ los vectores son L.I (Teorema 4.4.5).

%--------------------------------------------------------
\subsubsection{Aclaraciones importantes:}
\begin{itemize}
  \item El conjunto de vectores conformado por un solo vector distinto del nulo es L.I.
  \item El conjunto de vectores conformado solamente por el vector nulo es L.D.
  \item Si un conjunto de vectores contiene al vector nulo es L.D.
  \item Si a un conjunto de vectores L.D se le agrega un vector, entonces el nuevo conjunto es L.D.
  \item Todo subconjunto de un conjunto L.I es L.I.
\end{itemize}

%--------------------------------------------------------
\subsection{Ejercicio 1:}
  Determinar si los siguientes conjuntos de vectores son linealmente independientes:
\begin{multicols}{2}
  \begin{enumerate}[label=\alph*.]
    \item
      $\Omega=\W{\begin{pmatrix}1\\-2\\3\end{pmatrix}; \begin{pmatrix}2\\-2\\0\end{pmatrix};
        \begin{pmatrix}0\\1\\7\end{pmatrix}}$

    \item
      $\Omega=\W{\begin{pmatrix}1\\1\end{pmatrix};\begin{pmatrix}4\\-1\end{pmatrix};
        \begin{pmatrix}2\\5\end{pmatrix}}$
  \end{enumerate}
\end{multicols}

\textbf{a.} Antes de  resolver este tipo de ejercicios  es muy importante analizar el  conjunto de vectores,  pues
esto nos  ayudará a decir  el camino a  seguir.  Tenemos tres vectores  ($n=3$) pertenecientes a  $\RR^3$ ($m=3$).
Esto nos plantea dos caminos, podemos utilizar eliminación Gaussiana o podemos utilizar el Teorema 4.4.5 (calcular
el determinante de la matriz $A$); optaremos por la segunda opción.  Tenemos que la matriz $A$ es:
\[
  A =\begin{pmatrix}1 &2&0\\-2&-2&1\\3&0&7\end{pmatrix}
\]
Calculando su determinante (expandiendo por la segunda columna):
\begin{align*}
  \B{A}=\begin{vmatrix}1 &2&0\\-2&-2&1\\3&0&7\end{vmatrix}&=
        2\E{-1}^3 \begin{vmatrix} -2&1\\ 3 & 7 \end{vmatrix} +
        \E{-2}\E{-1}^4 \begin{vmatrix} 1&0\\ 3 & 7 \end{vmatrix}\\
          &= 2\E{-1}\E{-14 - 4}+ \E{-2}\E{7} = 22.
\end{align*}
La última  ecuación nos dice que  $\B{A}\neq0$,  por tanto el sistema  $A\vv{c}=\vv{0}$ tiene solución  única y el
conjunto de vectores $\Omega$ es L.I.

\textbf{b.} El enunciado nos presenta un conjunto de tres vectores pertenecientes a $\RR^2$, es decir, $n=3$ y
$m=2$. Según el resultado del Teorema 4.5.2, este conjunto es L.D ya que $n>m$.
%=================================================================================================================
%                                                    SECCIÓN 5.5
%=================================================================================================================
\newpage
\section{Sección 5.2 Bases y dimensión:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 5.5.1: Definición de base.
  \item Teorema 5.5.1.
  \item Teorema 5.5.2.
  \item Definición 5.5.2: Definición de dimensión.
  \item Teorema 5.5.4.
  \item Teorema 5.5.5.
\end{itemize}
Realice los ejemplos: 5.5.3, 5.5.4, 5.5.3, 5.5.9, 5.5.10, 5.5.11 y 5.5.12.
%--------------------------------------------------------
\subsection{Notas de clase: Bases y dimensión}
La guía pasada  (Espacios Vectoriales I) presentaba la  definición de conjunto generador de  un espacio vectorial.
Recordemos que el  Teorema 5.3.2 muestra que  para un único espacio vectorial $V$  podemos tener muchos conjuntos
generadores y cada uno con un número creciente de vectores (tantos como se quiera).

Las bases también son conjuntos generadores, pero a diferencia de los anteriores, las bases son conjuntos óptimos,
en el sentido de que tiene el número justo de vectores para generar el espacio vectorial $V$.  La definición 5.5.1
dice que para que  un conjunto finito de vectores $\W{\vv{v_1},\vv{v_2},\dots,\vv{v_n}}$ sea  una base del espacio
vectorial $V$, debe cumplir con dos condiciones:
\begin{enumerate}
  \item $\W{\vv{v_1},\vv{v_2},\dots,\vv{v_n}}$ debe ser linealmente independiente.
  \item $\W{\vv{v_1},\vv{v_2},\dots,\vv{v_n}}$ debe genera a $V$.
\end{enumerate}

Las bases  cuentan con propiedades  importantes para el  análisis de espacios  vectoriales.  La primara es  que la
representación de cualquier vector $\vv{v}$ como combinación lineal  de los vectores de una base es única (Teorema
5.5.1), es decir, solo existe una combinación de escalares $c_1,c_2,\dots, c_n$ tal que:
\[
  \vv{v} = c_1\vv{v_1}+ c_2\vv{v_2}+\dots+c_n\vv{v_n}
\]

La segunda propiedad  asegura que todas las bases de  un espacio vectorial $V$ tienen el  mismo número de vectores
(Teorema 5.5.2).  Por ejemplo:
\[
  \W{\begin{pmatrix}1\\0\\0\end{pmatrix};\begin{pmatrix}0\\1\\0\end{pmatrix};\begin{pmatrix}0\\0\\1\end{pmatrix}}
  \quad\text{y}\quad
  \W{\begin{pmatrix}1\\-2\\3\end{pmatrix};\begin{pmatrix}2\\-2\\0\end{pmatrix};\begin{pmatrix}0\\1\\7\end{pmatrix}}
\]
son dos  bases de $\RR^3$.  Este  resultado nos lleva  a la definición  de dimensión de  un espacio vectorial.  La
dimensión del espacio vectorial $V$ es el número de vectores contenidos en cualquier base de $V$.  Por este motivo
decimos que la dimensión de $V=\RR^3$, es tres.

%--------------------------------------------------------
\subsubsection{Aclaraciones importantes:}
\begin{itemize}
  \item Todo conjunto de $n$ vectores linealmente independiente es una base para $\RR^n$.
  \item El conjunto de vectores conformado por un solo vector diferente del nulo es  una base.
  \item  La  dimensión  de   un  espacio  vectorial  conformado  solamente  por  el   elemento  nulo  es  cero\par
  (${\dim\E{V=\W{\vv{0}}}=0}$).
  \item Si $H$ es un subespacio vectorial de un espacio vectorial $V$ de dimensión finita, entonces:\par
    $\dim\E{H}\leq\dim\E{V}$.
\end{itemize}
%--------------------------------------------------------
\subsection{Ejercicio 2:}
Determine si el conjunto de vectores $\Omega=\W{\E{1,2,0};\E{0,1,1}}$ es una base para:
\[
  H=\W{\E{x,y,z}\in\RR^3\,/\, -2x+y+5z=0}.
\]

El conjunto de  vectores $\Omega$ está compuesto por  dos vectores de $\RR^3$,  por tanto  el espacio generado por
estos vectores es un subespacio de $\RR^3$.  Por otro lado,  $H$ es un plano que pasa por el origen (un subespacio
de $\RR^3$).

Recordemos que  para que un  conjunto de vectores  sea una base  de un espacio  vectorial,  debe ser L.I  y además
generar al espacio vectorial.  Comprobemos la primera condición. Por simple inspección, podemos ver que los
vectores en $\Omega$ no son múltiplos escalares, mostrando que son L.I (Teorema 5.4.1). Por otro lado, debemos
asegurarnos que generan a $H$.

Planteamos un vector genérico $\vv{v}=\E{x,y,z}\in\gen\W{\Omega}$ y utilizamos la definición de espacio generado:
\[
  \begin{pmatrix}x\\y\\z\end{pmatrix}=c_1\begin{pmatrix}1\\2\\0\end{pmatrix}+c_2\begin{pmatrix}0\\1\\1\end{pmatrix}
\]
que nos lleva al sistema:
\[
  \begin{cases}c_1=x\\2c_1+c_2=y\\c_2=z\end{cases}
\]
Resolviendo:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rr|l}
  1 & 0 & x \\
  2 & 1 & y \\
  0 & 1 & z \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
  & \ro{R_2\leftrightarrow R_3}
% Segunda Matriz
\begin{sysmatrix}{rr|l}
  1 & 0 & x \\
  0 & 1 & z \\
  2 & 1 & y \\
\end{sysmatrix}
% Operaciones por renglón (sólo una operación)
  & \ro{\E{-2}R_1+R_3\to R_3}
% Tercera Matriz
\begin{sysmatrix}{rr|l}
  1 & 0 & x \\
  0 & 1 & z \\
  0 & 1 & y-2x \\
\end{sysmatrix}
  & \ro{\E{-1}R_2+R_3\to R_3}
% Cuarta Matriz
\begin{sysmatrix}{rr|l}
  1 & 0 & x \\
  0 & 1 & z \\
  0 & 0 & y-2x-z\\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
llegamos al sistema equivalente:
\[
  \begin{cases}c_1=x\\c_2=z\\0c_2=-2x+y-z\end{cases}
\]

La última ecuación nos indica que $\gen\W{\Omega}=\W{\E{x,y,z}\in\RR^3:\,  -2x+y-z=0}$.  Esto muestra que $\Omega$
no genera a $H$.  En conclusión $\Omega$ no es base de $H$.

Podemos encontrar una base para $H$? Si, el procedimiento es el siguiente: Planteamos un vector genérico
$\vv{v}=\E{x,y,z}\in H$ y según la ecuación del plano $H$:
\[
  y = 2x-5z
\]
lo que implica que:
\[
  \begin{pmatrix}x\\y\\z\end{pmatrix}=\begin{pmatrix}x\\2x-5z\\z\end{pmatrix}
                  =\begin{pmatrix}x\\2x\\0\end{pmatrix}+\begin{pmatrix}0\\-5z\\z\end{pmatrix}
                  =x\begin{pmatrix}1\\2\\0\end{pmatrix}+z\begin{pmatrix}0\\-5\\1\end{pmatrix}
\]
definiendo $x=t$ y $z=s$ con $t,s\in\RR$:
\[
  \begin{pmatrix}x\\y\\z\end{pmatrix}=t\begin{pmatrix}1\\2\\0\end{pmatrix}+s\begin{pmatrix}0\\-5\\1\end{pmatrix}
\]
La anterior  ecuación dice que cualquier  vector de $H$ puede  escribirse como combinación lineal  de los vectores
$\E{1,2,0}$ y $\E{0,-5,1}$; estos conforman una base para $H$.
%=================================================================================================================
%                                                   SECCIÓN 5.7
%=================================================================================================================
\section{Sección 5.7:  Rango, nulidad, espacio renglón y espacio columna}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Ecuación 5.7.1 y definición 5.7.1: Espacio nulo y nulidad de una matriz.
  \item Teorema 5.7.1.
  \item Definición 5.7.2: Imagen de una matriz.
  \item Teorema 5.7.2.
  \item Definición 5.7.3: Rango de una matriz.
  \item Definición 5.7.4: Espacio renglón y espacio columna de una matriz.
  \item Teorema 5.7.3.
  \item Teorema 5.7.4.
  \item Teorema 5.7.7.
  \item Teorema 5.7.8.
  \item Teorema 5.7.9.
  \item Teorema 5.7.10: Teorema resumen (importante).
\end{itemize}
Realice los ejemplos: 5.7.1, 5.7.2, 5.7.3, 5.7.4, 5.7.6, 5.7.6 y 5.7.8.
%--------------------------------------------------------
\subsection{Notas de clase: Rango, nulidad, espacio renglón y espacio columna}

Si consideramos  las columnas o  los renglones de  una matriz como  conjuntos de  vectores podemos  definir varios
subespacios vectoriales  que nos ayudan  a caracterizar la  matriz.  Estos subespacios son:  El  espacio nulo,  la
imagen,  el espacio columna y el espacio renglón.  Cada uno  nos informa sobre una característica especifica de la
matriz.

\textbf{Espacio nulo y nulidad de una matriz:}
Para una matriz $A$ de $m\times n$, el espacio nulo se define como:
\[
  N_A=\W{\vv{x}\in\RR^n:\, A\vv{x}=\vv{0}}.
\]
El espacio nulo  está conformado por todos  los vectores $\vv{x}$,  de $\RR^n$,  que al  pre-multiplicarlos por la
matriz $A$ producen el vector nulo.  En otras palabras,  el  espacio nulo está conformado por todas las soluciones
del  sistema homogéneo  $A\vv{x}=\vv{0}$.  Este conjunto  solución es  un espacio  vectorial,  por tanto,  podemos
encontrar una base y una dimensión, que llama nulidad ($\nu\E{A}=\dim{N_A}$).

El espacio  nulo y la nulidad  nos ayudan a determinar  si una  matriz cuadrada  es invertible.  El  resultado del
Teorema 5.7.1 dice que una matriz cuadrada $A$ es invertible si su nulidad es cero,  es decir,  si el espacio nulo
está  conformado solamente  por el  vector nulo.  Esto  lo podemos  inferir si  el sistema  $A\vv{x}=\vv{0}$ tiene
solución única.  Por el contrario, si el sistema tiene infinitas soluciones, entonces $A$ es no invertible.

Por otro  lado,  existe un estrecho  vínculo entre la nulidad  y la independencia  lineal de las  columnas de $A$.
Recordemos que para determinar  si un conjunto de vectores es L.I  resolvíamos el sistema $A\vv{c}=\vv{0}$,  donde
cada columna  de $A$ corresponde  a uno de  los vectores en  nuestro conjunto.  Es fácil  ver la similitud  con la
definición de espacio nulo  de $A$ y asociar la dependencia  o independencia lineal de las columnas  de $A$ con su
nulidad: Las columnas de $A$ son linealmente independientes si su nulidad es cero.  En consecuencia, podemos decir
que una matriz cuadrada es invertible si sus columnas son linealmente independientes.

\textbf{Espacio Columna y espacio renglón de una matriz:}
El espacio columna de una matriz $A$ de $m\times n$, se define como el espacio generado por las columnas de $A$:
\[
  C_A=\gen\W{\text{columnas de A}}
\]
Es decir,  que el espacio columna está compuesto por  todas las combinaciones lineales de las columnas de $A$.  Es
muy importante  mencionar que este espacio  es un subespacio de  $\RR^m$.  Por ejemplo,  si $A$ es  de $3\times 2$
($m=3$),  entonces el espacio columna podría  ser:  Una recta en el espacio que pasa por el origen,  o un  plano que pasa por el
origen, o todo $\RR^3$.  Esto dependerá del número de columnas de $A$ que sean linealmente independientes.

De forma análoga podemos definir el espacio renglón de $A$ como:
\[
  R_A=\gen\W{\text{renglones de A}}
\]
es decir, el espacio generado por los renglones de $A$ y este será un subespacio de $\RR^n$.

Es importante  resaltar el resultado del  Teorema 5.7.4 que dice  que las  dimensiones del  espacio columna  y del
espacio fila de $A$ son iguales:
\[
  \dim\E{C_A}=\dim\E{R_A}
\]
Esto no implica que los espacios sean iguales,  pero si sugiere que la matriz tiene igual cantidad de columnas L.I
como de  renglones L.I.  Por ejemplo,  Si  a $A$ es  un matriz de  $3\times 2$ ($m=3$  y $n=2$) cuya  dimensión de
espacio columna es igual a uno ($\dim\E{C_A}=1$),  entonces $A$ tiene una sola columna y un solo renglón L.I, pero
el espacio columna es una recta de $\RR^3$, mientras que el espacio fila es una recta de $\RR^2$.

\textbf{Imagen y rango de una matriz:}
Para una matriz $A$ de $m\times n$, la imagen se define como:
\[
  \text{Im}_A=\W{\vv{y}\in\RR^{m}:\,\vv{y}=A\vv{x},\,\,\text{para alguna $\vv{x}\in\RR^n$}}
\]
La imagen de $A$ está compuesta por todos los vectores $\vv{y}$, de $\RR^m$, que resultan de multiplicar la matriz
$A$ por algún  vector $\vv{x}$ de $\RR^n$.  Si vemos el  resultado del producto entre una matriz  y un vector como
una combinación  lineal de las  columnas de la matriz,  entonces  la imagen es  el conjunto de  todas las posibles
combinaciones lineales de las columnas de la matriz, es decir:
\[
  \text{Im}_A=\gen\W{\text{columnas de A}}=C_A
\]
Explícitamente,  la imagen de una matriz es igual a su espacio columna (Teorema~5.7.3) y el rango de una matriz es
la dimensión de su imagen ($\rho\E{A}=\dim\E{\text{Im}_A}=\dim\E{C_A}$).
%--------------------------------------------------------
\subsubsection{Aclaraciones importantes:}
\begin{itemize}
  \item Para una matriz $A$ se cumple que $\rho\E{A}=\dim\E{\text{Im}_A}=\dim\E{C_A}=\dim\E{R_A}$ (Teorema 5.7.4).
  \item Para una matriz $A$ de $m\times n$ se verifica que $n=\nu\E{A}+\rho\E{A}$ (Teorema 5.7.7).
  \item Una matriz  de $n\times n$ es invertible si  y solo si $\rho\E{A}=n$ (Teorema 5.7.8).  Si  el rango de una
  matriz es igual al número  de columnas,  entonces implica que su nulidad es cero  (Teorema~5.7.7) y por tanto es
  invertible (Teorema~5.7.1).
\end{itemize}
%--------------------------------------------------------
\subsection{Ejercicio 3:}
Para la matriz:
\[
  A= \begin{pmatrix}1&-1&3\\2&0&4\\-1&-3&1\end{pmatrix}
\]
\begin{enumerate}[label=\alph*.]
  \item Encontrar el espacio nulo y la nulidad.
  \item Encontrar el espacio columna y su dimensión.
  \item Encontrar la imagen y el rango.
\end{enumerate}

\textbf{a.} El enunciado nos presenta una matriz de $3\times  3$.  Por tanto, el espacio nulo será un subespacio de
$\RR^3$.  Para  encontrarlo,  primero  planteamos  un  vector  genérico  perteneciente  al  espacio  nulo  de $A$,
$\vv{x}=\E{x,y,z}\in N_A$, y luego seguimos la definición de espacio nulo que nos lleva al sistema:
\begin{align*}
  A\vv{x}&=\vv{0}\\
  \begin{pmatrix}1&-1&3\\2&0&4\\-1&-3&1\end{pmatrix} \begin{pmatrix}x\\y\\z\end{pmatrix}&=
    \begin{pmatrix}0\\0\\0\end{pmatrix}
\end{align*}
Resolviendo:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rrr|l}
  1 & -1 & 3 & 0 \\
  2 &  0 & 4 & 0 \\
 -1 & -3 & 1 & 0 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
&\!\begin{aligned}
  & \ro{-2R_1+R_2\to R_2}\\
  & \ro{R_1+R_3\to R_3}\\
\end{aligned}
% Segunda Matriz
\begin{sysmatrix}{rrr|l}
  1 & -1 &  3 & 0 \\
  0 &  2 & -2 & 0 \\
  0 & -4 &  4 & 0 \\
\end{sysmatrix}
% Operaciones por renglón (sólo una operación)
  & \ro{2R_2+R_3\to R_3}
% Tercera Matriz
\begin{sysmatrix}{rrr|l}
  1 & -1 &  3 & 0 \\
  0 &  2 & -2 & 0 \\
  0 &  0 &  0 & 0 \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
obtenemos el sistema equivalente:
\[
  \begin{cases}x-y+3z=0\\2y-2z=0\\0z=0\end{cases}\Rightarrow
  \begin{cases}x=y-3z=-2z\\y=z\\0z=0\end{cases}
\]
Vemos que el sistema $A\vv{x}=\vv{0}$ tiene infinitas soluciones que podemos escribir como ($z=t$ con $t\in\RR$):
\[
\begin{pmatrix}x\\y\\z\end{pmatrix}= \begin{pmatrix}-2z\\z\\z\end{pmatrix}=z\begin{pmatrix}-2\\1\\1\end{pmatrix}
=t\begin{pmatrix}-2\\1\\1\end{pmatrix}
\]
Esto muestra que el espacio nulo de $A$ es:
\[
  N_A=\gen\W{\begin{pmatrix}-2\\1\\1\end{pmatrix}}
\]
que corresponde a una recta, de $\RR^3$, que pasa por el origen y su base es el conjunto formado únicamente por el
vector $\E{-2,1,1}$.  Como resultado, la nulidad de $A$ es:
\[
  \nu\E{A} =\dim\E{N_A}=1
\]

\textbf{b.} El espacio columna de  $A$ será un subespacio de $\RR^3$ y para  encontrarlo debemos buscar el espacio
generado por las columnas de $A$.  Proponemos un vector genérico $\vv{x}=\E{x,y,z}\in C_A$ y según la definición:
\[
  C_A=\gen\W{\begin{pmatrix}1\\2\\-1\end{pmatrix};\begin{pmatrix}-1\\0\\-3\end{pmatrix};
             \begin{pmatrix}3\\4\\1\end{pmatrix}}
\]
que nos lleva al sistema:
\[
  \begin{pmatrix}1&-1&3\\2&0&4\\-1&-3&1\end{pmatrix}
  \begin{pmatrix}c_1\\c_2\\c_3\end{pmatrix}=\begin{pmatrix}x\\y\\z\end{pmatrix}
\]
Resolviendo:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rrr|l}
  1 & -1 & 3 & x \\
  2 &  0 & 4 & y \\
 -1 & -3 & 1 & z \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
&\!\begin{aligned}
  & \ro{-2R_1+R_2\to R_2}\\
  & \ro{R_1+R_3\to R_3}\\
\end{aligned}
% Segunda Matriz
\begin{sysmatrix}{rrr|l}
  1 & -1 &  3 & x \\
  0 &  2 & -2 & y-2x \\
  0 & -4 &  4 & z+x \\
\end{sysmatrix}
% Operaciones por renglón (sólo una operación)
  & \ro{2R_2+R_3\to R_3}
% Tercera Matriz
\begin{sysmatrix}{rrr|l}
  1 & -1 &  3 & x \\
  0 &  2 & -2 & y-2x \\
  0 &  0 &  0 & z+x+2\E{y-2x}\\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
llegamos al sistema equivalente:
\[
  \begin{cases}c_1-c_2+3c_3=x\\2c_2-2c_3=y-2x\\0c_3=-3x+2y+z\end{cases}
\]
La última ecuación nos indica que el espacio columna de $A$ es:
\[
  C_A = \W{\E{x,y,x}\in\RR^3:\,-3x+2y+z=0}
\]
que corresponde a un plano de $\RR^3$ que pasa por el origen. A partir de la ecuación del plano podemos encontrar
una base para este subespacio si despejamos la variable $z$:
\[
        z=3x-2y
\]
y luego:
\[
  \begin{pmatrix}x\\y\\z\end{pmatrix} = \begin{pmatrix}x\\y\\3x-2y\end{pmatrix}
    =\begin{pmatrix}x\\0\\3x\end{pmatrix}+\begin{pmatrix}0\\y\\-2y\end{pmatrix}
    = x\begin{pmatrix}1\\0\\3\end{pmatrix}+y\begin{pmatrix}0\\1\\-2\end{pmatrix}
    = t\begin{pmatrix}1\\0\\3\end{pmatrix}+s\begin{pmatrix}0\\1\\-2\end{pmatrix}
\]
donde $x=t$ y $y=s$ con $t,s\in\RR$. El conjunto de vectores $\W{\E{1,0,3};\E{0,1,-2}}$ es una base para $C_A$ y
por tanto su dimensión es:
\[
  \dim\E{C_A} = 2.
\]

\textbf{c.} Gracias al Teorema 5.7.3 podemos determinar que la imagen de $A$ es:
\[
  \text{Im}_A=C_A = \W{\E{x,y,x}\in\RR^3:\,-3x+2y+z=0}
\]
y su rango:
\[
  \rho\E{A}=\dim\E{\text{Im}_A}=\dim\E{C_A} = 2.
\]
Además se comprueba el resultado del Teorema 5.7.7:
\begin{align*}
  \rho\E{A} +\nu\E{A}&=n\\
  2 + 1&=3\\
  3&=3.
\end{align*}
%===================================================================================================================
%                                                      BIBLIOGRAFÍA
%===================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%===================================================================================================================
\end{document}
%===================================================================================================================
