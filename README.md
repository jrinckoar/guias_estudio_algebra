# Guías de Estudio de Álgebra Lineal y Geometría Analítica

**Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>**

**https://bitbucket.org/jrinckoar/guias_estudio_algebra**
---

## Información General

Recopilación de archivos de Latex con las guías de estudio de la asignatura
de **Álgebra Lineal y Geometría Analítica** de la FI-UNER.

## Lista de guías de estudio

1. Sistemas de ecuaciones.
2. Matrices.
3. Determinantes y regla de Cramer.
5. Vectores en el plano.
5. Vectores en el espacio.
6. Recta en el espacio.
7. Plano.
8. Números complejos.
9. Secciones cónicas.
11. Espacios vectoriales 1: Definición, comb. lineal, espacio generado e independencia lineal.
12. Espacios vectoriales 2: Bases y dimensión
13. Trasformaciones lineales.
14. Valores y vectores propios.

---

## Bibliografía

- book{Grossman2008,
  title = {Álgebra {L}ineal},
  isbn = {970-100-890-1},
  publisher = {McGraw-Hill},
  author = {Grossman S, Stanley I},
  edition={6},
  year = {2008}}

- book{Colombo2015-a,
  title={Álgebra lineal y geometría analítica},
  author={María Mercedes Colombo},
  year={2015},
  pages={290},
  isbn={978-950-698-346-8},
  publisher = {{EDUNER}}}

- book{Larson2010,
  title = {Cálculo {E}sencial},
  isbn = {9786074812695},
  publisher = {Cengage Learning Latin America},
  author = {Larson, Ron and Hostetler, Robert P. and Edwards, Bruce H.},
  year = {2010}}

- book{Stewart2012,
  title={Cálculo de varias variables trascendentes tempranas, 7ma edición},
  author={Stewart, James},
  year={2012},
  publisher={Cengage Learning Editores}}

- book{Stewart2016,
  title={Precálculo. Matemática para el cálculo. Sexta edición},
  author={Stewart, James and Redlin, Lothar and Watson, Saleem},
  year={2016},
  isbn = {978-607-481-826-0},
  publisher={Cengage Learning}}

---

## Log

- 2021-08-24 Actualización. 

---

Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
