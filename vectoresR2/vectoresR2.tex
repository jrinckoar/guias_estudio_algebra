%*******************************************************************************************************************
%*******************************************************************************************************************
%                                          ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*******************************************************************************************************************
%*******************************************************************************************************************
%                                                 GUÍA DE ESTUDIO 6
%                                                   VECTORES EN R2
%-------------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2020-04-13
%*******************************************************************************************************************
%*******************************************************************************************************************
%===================================================================================================================
%                                                         FORMATO
%===================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%===================================================================================================================
% Comandos locales
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
{\left(\begin{array}{@{}#1@{}}}
		{\end{array}\right)}

\newcommand{\cb}[1]{{\color{blue}{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%===================================================================================================================
%                                                         TÍTULO
%===================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio VI \\ Vectores en el Plano ($\RR^2$)\\[3pt]
	\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-------------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%===================================================================================================================
%                                                     OBJETIVOS
%===================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de la sección 4.1 y 4.2:
\begin{itemize}
	\item \textbf{Sección 4.1 Vectores en el plano:} Presenta la definición geométrica y algebraica  de un vector,
	      la definición de módulo y dirección de un vector.
	\item  \textbf{Sección  4.2 Producto  escalar,  ángulo  entre vectores  y  proyecciones  en  $\RR^2$:}  Define las
	      operaciones de producto escalar entre vectores, ángulo entre vectores y proyección de un vector sobre otro.
\end{itemize}
%===================================================================================================================
%                                                    SECCIÓN 4.1
%===================================================================================================================
\section{Sección 4.1 Vectores en el plano}
Lea cuidadosamente  las definiciones,  prestando  atención a  la notación  y a  la simbología  utilizada.
\begin{itemize}
	\item Definición 4.1.1: Definición geométrica de un vector.
	\item Definición 4.1.2: Definición algebraica de un vector.
	\item Definición de magnitud/longitud de un vector.
	\item Definición de dirección de un vector.
	\item Operación de multiplicación de un vector por un escalar.
	\item Definición 4.1.3: Definición de vector unitario.
\end{itemize}
Analice detalladamente la tabla 4.1 de la página 239 y realice los ejemplos: 4.1.1, 4.1.2, 4.1.3, 4.1.4 y 4.1.6
%--------------------------------------------------------
\subsection{Notas de clase:}
\textbf{Vectores especiales:} En el plano podemos definir dos vectores unitarios, el vector $\vv{i}=\E{1,0}$ y el
vector $\vv{j}=\E{0,1}$. Todo vector de $\RR^2$ se puede escribir como una \textbf{combinación lineal} de estos dos
vectores. Por ejemplo, sea el vecotr $\vv{u}=\E{a,b}$, entonces:
\begin{align*}
	\vv{u} & =\E{a,b}                                                                                    \\
	       & = a\E{1,0} + b\E{0, 1} &  & \text{producto de un vector por un escalar y suma de vectores.} \\
	       & = a\vv{i} + b\vv{j}    &  & \text{combinación lineal de los vectores $\vv{i}$ y $\vv{j}$.}
\end{align*}

\textbf{Magnitud de un escalar por un vector:}
Si $\alpha$ es un escalar diferente de cero, entonces $\B{\alpha\vv{v}}$ (la magnitud del vector $\alpha\vv{v}$) es
igual a $\B{\alpha}\B{\vv{v}}$. Es decir, se multiplica la magnitud el vector $\vv{v}$ por el valor absoluto del
escalar $\alpha$.

%--------------------------------------------------------
\subsection{Ejercicio 1:}
Considere el  vector:  $\vv{c}= \begin{pmatrix}\alpha\\\beta\end{pmatrix}$.  Encuentre los  valores de  $\alpha$ y
$\beta$ para que $\B{\vv{c}}=\sqrt{2}$ y $\dir{\vv{c}}=\pi/4$.  Considere que $\alpha>0$.  \\[0.5em]

El enunciado  del ejercicio  nos presenta  un vector  de $\RR^2$ y nos  pide encontrar  las componentes  ($\alpha$ y
$\beta$) del vector $\vv{c}$ para  que su magnitud sea igual a $\sqrt{2}$ y su  dirección sea igual a $\pi/4$.  Para
resolverlo utilizaremos las definiciones de magnitud y dirección de un vector.  Para el vector $\vv{c}$:

\[
	\begin{dcases*}
		\B{\vv{c}} = \sqrt{\alpha^2+\beta^2} = \sqrt{2} &  Magnitud. \\
		\dir{\vv{c}} = \tan\E{\frac{\beta}{\alpha}} =\frac{\pi}{4} & Dirección.
	\end{dcases*}
\]

Tenemos un sistema de dos variables y dos ecuaciones. Comenzaremos a desarrollar la segunda ecuación:
\begin{align*}
	\tan\E{\frac{\beta}{\alpha}} & = \frac{\pi}{4}              &  &                                               \\
	\frac{\beta}{\alpha}         & = \tan^{-1}\E{\frac{\pi}{4}} &  & \text{Despejando el cociente $\beta/\alpha$.} \\
	\frac{\beta}{\alpha}         & = 1                          &  &                                               \\
	\beta                        & = \alpha                     &  &
\end{align*}

Según la primera ecuación:
\begin{align*}
	\sqrt{\alpha^2+\beta^2}  & = \sqrt{2} &  &                                                         \\
	\sqrt{\alpha^2+\alpha^2} & = \sqrt{2} &  & \text{Utilizando el resultado $\beta=\alpha$.}          \\
	\sqrt{2\alpha^2}         & = \sqrt{2} &  &                                                         \\
	\sqrt{2}\sqrt{\alpha^2}  & = \sqrt{2} &  & \text{Propiedad $\sqrt{xy}=\sqrt{x}\sqrt{y}$.}          \\
	\sqrt{\alpha^2}          & = 1        &  &                                                         \\
	\B{\alpha}               & = 1        &  & \text{Definición de valor absoluto $\B{x}=\sqrt{x^2}$.} \\
	\alpha                   & = \pm 1    &  &
\end{align*}

La  última ecuación  nos dice  que $\alpha  =\pm1$,  pero según  el enunciado  $\alpha>0$ y  por tanto  $\alpha=1$ y
$\beta=1$.      En     conclusión     el     vector     $\vv{c}=     \begin{pmatrix}\alpha\\\beta\end{pmatrix}     =
	\begin{pmatrix}1\\1\end{pmatrix}$.
%===================================================================================================================
%                                                    SECCIÓN 4.2
%===================================================================================================================
\section{Sección  4.2 Producto  escalar,  ángulo  entre vectores  y  proyecciones  en  el plano:}

Lea cuidadosamente  las definiciones y Teoremas,  prestando  atención a  la notación  y a  la simbología  utilizada.
\begin{itemize}
	\item Definición 4.2.1: Ángulo entre vectores.
	\item Definición 4.2.1: La magnitud de un vector en términos del producto escalar.
	\item Teorema 4.2.2
	\item Definición 4.2.2: Vectores paralelos.
	\item Teorema 4.2.3
	\item Definición 4.2.2: Vectores ortogonales.
	\item Teorema 4.2.4
	\item Teorema 4.2.5
	\item Definición 4.2.1: Proyección.
\end{itemize}
Analice y realice los ejemplos: 4.2.1, 4.2.2, 4.2.3 y 4.2.4.

%--------------------------------------------------------
\subsection{Notas de clase:}

\textbf{Teorema 4.2.3  - Vectores  paralelos:} Si  $\vv{u}$ es  un vector  diferente del  nulo y  $\alpha$ un
escalar distinto de cero. Entonces $\vv{v}=\alpha\vv{u}$ si y solo si $\vv{u}$ y $\vv{v}$ son paralelos.

Este teorema plantea:
\begin{enumerate}
	\item Si $\vv{u}$ y $\vv{v}$ son paralelos, entonces $\vv{v}=\alpha\vv{u}$.
	\item Si $\vv{v}=\alpha\vv{u}$, entonces $\vv{u}$ y $\vv{v}$  son paralelos.
\end{enumerate}
Vamos a demostrar el segundo enunciado. Sea $\vv{v}=\alpha\vv{u}$. Utilizando el Teorema 4.2.2:
\begin{align*}
	\cos\phi & = \frac{\vv{u}\cdot\vv{v}}{\B{\vv{u}}\B{\vv{v}}}                     &  &                                     \\
	\cos\phi & = \frac{\vv{u}\cdot\E{\alpha\vv{u}}}{\B{\vv{v}}\B{\alpha\vv{u}}}     &  & \text{Reemplazando $\vv{v}$.}       \\
	\cos\phi & = \frac{\alpha\E{\vv{u}\cdot\vv{u}}}{\B{\vv{u}}\B{\alpha\vv{u}}}     &  & \text{Propiedad \textbf{iv} Teorema
	2.2.1.}                                                                                                                  \\
	\cos\phi & = \frac{\alpha\E{\vv{u}\cdot\vv{u}}}{\B{\alpha}\B{\vv{u}}\B{\vv{u}}} &  & \text{Magnitud de un escalar por
	un vector.}                                                                                                              \\
	\cos\phi & = \frac{\alpha\B{\vv{u}}^2}{\B{\alpha}\B{\vv{u}}\B{\vv{u}}}          &  & \text{Teorema 4.2.1}                \\
	\cos\phi & = \frac{\alpha}{\B{\alpha}}                                          &  &                                     \\
	\cos\phi & = \pm 1                                                              &  & \text{$\frac{\alpha}{\B{\alpha}}=
			\begin{cases}1 & \text{si}\quad\alpha>0\\-1 & \text{si}\quad\alpha<0\end{cases}$.}
\end{align*}

La última ecuación nos indica  que el ángulo entre los vectores $\vv{u}$ y $\vv{v}$  es $\phi=0$ o $\phi=\pi$(ya que
son los valores donde el coseno se hace igual a $1$ o igual a $-1$, respectivamente), mostrando que los vectores son
paralelos (definición dos vectores paralelos).\\[0.5em]

\textbf{Teorema 4.2.4 -  Vectores ortogonales:} Los vectores $\vv{u}$ y  $\vv{v}$,  diferentes del vector nulo,  son
ortogonales si y solo si $\vv{u}\cdot\vv{v}=0$.

Este teorema plantea:
\begin{enumerate}
	\item Si $\vv{u}$ y $\vv{v}$ son ortogonales, entonces $\vv{u}\cdot\vv{v}=0$.
	\item Si $\vv{u}\cdot\vv{v}=0$, entonces $\vv{u}$ y $\vv{v}$  son ortogonales.
\end{enumerate}
Vamos a demostrar el primer enunciado. Según la Definición 4.2.3, dos vectores son ortogonales si el ángulo entre
ellos es igual a $\pi/2$. Utilizando el Teorema 4.2.2:
\begin{align*}
	\frac{\vv{u}\cdot\vv{v}}{\B{\vv{u}}\B{\vv{v}}} & =\cos\phi &  &                                                \\
	\frac{\vv{u}\cdot\vv{v}}{\B{\vv{u}}\B{\vv{v}}} & =0        &  & \text{Si $\phi=\pi/2$, entonces $\cos\phi=0$.} \\
	\intertext{para que la fracción sea igual cero, basta con que el numerador sea igual cero:}
	\vv{u}\cdot\vv{v}                              & =0        &  &                                                \\
\end{align*}
%--------------------------------------------------------
\newpage
\subsection{Ejercicio 2:}
Considere los vectores: $\vv{u}=\E{a, 1}$, $\vv{v}=\E{2, 0}$ y $\vv{w}=\E{-2, 2}$.
\begin{enumerate}[label=\alph*.]
	\item Determine el valor de $a$ para que el ángulo entre los vectores $\vv{u}$ y $\vv{v}$ sea $\pi/3$.
	\item Determine el valor de $a$ para que los vectores $\vv{u}$ y $\vv{w}$ sean ortogonales.
	\item Para $a=1$, calcule el vector $\vv{z}=  \B{\vv{w}}\E{\vv{v} - \vv{u}} + \proy{u}{v}$
\end{enumerate}

\textbf{a.} Para resolver este inciso utilizaremos el Teorema 4.2.2. Pero antes debemos calcular
$\E{\vv{u}\cdot\vv{v}}$, $\B{\vv{u}}$ y $\B{\vv{v}}$:
\begin{align*}
	\vv{u}\cdot\vv{v} & = \E{a,1}\cdot\E{2,0} = 2a &  & \text{Producto escalar.}      \\
	\B{\vv{u}}        & = \sqrt{a^2 + 1}           &  & \text{Magnitud de un vector.} \\
	\B{\vv{v}}        & = \sqrt{2^2}=2             &  & \text{Magnitud de un vector.} \\
\end{align*}

Según el Teorema 4.2.2:
\begin{align*}
	\frac{\vv{u}\cdot\vv{v}}{\B{\vv{v}}\B{\vv{u}}} & =\cos\phi                       &  &                                             \\
	\frac{2a}{2\sqrt{a^2+1}}                       & =\cos\phi                       &  &                                             \\
	\frac{a}{\sqrt{a^2+1}}                         & =\frac{1}{2}                    &  & \text{$\cos\E{\pi/3}=1/2$.}                 \\
	\intertext{Operando algebraicamente:}
	2a                                             & =\sqrt{a^2+1}                   &  &                                             \\
	\E{2a}^2                                       & =\E{\sqrt{a^2+1}}^2             &  & \text{Elevando ambos miembros al cuadrado.} \\
	\E{2a}^2                                       & =\E{\sqrt{a^2+1}}^2             &  &                                             \\
	4a^2                                           & =a^2+1                          &  &                                             \\
	a^2                                            & =1/3                            &  &                                             \\
	a                                              & =\pm \sqrt{1/3} = \pm\sqrt{3}/3 &  &
\end{align*}

La última ecuación nos indica que si $a=\sqrt{3}/3$ o $a=-\sqrt{3}/3$, entonces el ángulo entre los vectores
$\vv{u}$ y $\vv{v}$ será de $60\degree$.

\textbf{b.} El Teorema 4.2.4 nos dice que si el producto punto entre dos vectores es cero, entonces dichos vectores
son ortogonales. Podemos plantear entonces:

\begin{align*}
	\vv{u}\cdot\vv{v}    & =0 &  &                          \\
	\E{a,1}\cdot\E{-2,2} & =0 &  &                          \\
	-2a+2                & =0 &  & \text{Producto escalar.} \\
	a                    & =1 &  &                          \\
\end{align*}

Si $a=1$, entonces los vectores $\vv{u}$ y $\vv{w}$ son ortogonales.

\textbf{c.} Antes de proceder con las operaciones requeridas es necesario analizar el enunciado, que nos pide
calcular un vector $\vv{z}$ cuando $a=1$:
\[
	\vv{z} =  \B{\vv{w}}\E{\vv{v} - \vv{u}} + \proy{u}{v}
\]

Debemos  observar las  operaciones requeridas.  El  producto  $\B{\vv{w}}\E{\vv{v}  -  \vv{u}}$  debe  regresar como
resultado un vector,  ya  que es el producto de  un escalar ($\B{\vv{w}}$) por un vector  ($\vv{v} - \vv{u}$,  es la
resta de dos vectores).  Por otro lado,  $\proy{u}{v}$ es un vector, lo que implica que $\vv{z}$ se conforma como la
suma de dos vectores:
\begin{align*}
	\B{\vv{w}}\E{\vv{v} - \vv{u}} & = \E{\sqrt{\E{-2}^2+2^2}} \E{\E{2, 0} - \E{1, 1}}          &  &                                              \\
	                              & = \sqrt{8}\E{1, -1} = 2\sqrt{2}\E{1, -1}                   &  &                                              \\
	\intertext{Según la Definición 4.2.4:}
	\proy{u}{v}                   & = \frac{\vv{u}\cdot\vv{v}}{\B{\vv{u}}^2}\vv{u}             &  &                                              \\
	                              & = \frac{\E{1, 1}\cdot\E{2, 0}}{\E{\sqrt{1 + 1}}^2}\E{1, 1} &  &                                              \\
	                              & = \frac{2}{2}\E{1, 1}=\E{1,1}                              &  &                                              \\
	\intertext{Entonces:}
	\vv{z}                        & =  \B{\vv{w}}\E{\vv{v} - \vv{u}} + \proy{u}{v}             &  &                                              \\
	                              & =  2\sqrt{2}\E{1,-1} + \E{1,1}                             &  &                                              \\
	                              & =  \E{2\sqrt{2}, -2\sqrt{2}} + \E{1,1}                     &  & \text{Producto de un escalar por un vector.} \\
	                              & =  \E{2\sqrt{2}+1, -2\sqrt{2}+1}                           &  & \text{Suma de vectores.}                     \\
\end{align*}

El vector $\vv{z}=\E{1+2\sqrt{2}, 1-2\sqrt{2}}$.
%===================================================================================================================
%                                                      BIBLIOGRAFÍA
%===================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%===================================================================================================================
\end{document}
%===================================================================================================================
