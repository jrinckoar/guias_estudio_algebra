%*****************************************************************************************************************
%*****************************************************************************************************************
%                                       ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                GUÍA DE ESTUDIO 11
%                                            TRANSFORMACIONES LINEALES
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2020-05-18
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                     FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
 {\left(\begin{array}{@{}#1@{}}}
 {\end{array}\right)}

\newcommand{\ro}[1]{{\color{blue}\scriptscriptstyle{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio XI \\Transformaciones Lineales\\[3pt]
\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                    OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de las secciones 7.1, 7.2 y 7.3:
\begin{itemize}
  \item \textbf{Sección 7.1: Definición y ejemplos.}
  \item \textbf{Sección 7.2: Propiedades de las transformaciones lineales.}
  \item \textbf{Sección 7.3: Representación matricial de una trasformación lineal.}
\end{itemize}
%=================================================================================================================
%                                                   SECCIÓN 7.1
%=================================================================================================================
\section{Sección 7.1 Definición y ejemplos:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 7.1.1: Transformación lineal.
\end{itemize}
Realice los ejemplos: 7.1.1, 7.1.3, 7.1.4, 7.1.6, 7.1.8 y 7.1.10.
%--------------------------------------------------------
\subsection{Notas de clase: Definición y ejemplos}
\textbf{Transformaciones lineales:}

Las transformaciones son funciones que toman vectores pertenecientes a un espacio vectorial llamando dominio y los
transforman  en otros  vectores pertenecientes  a un  espacio  vectorial  llamado  imagen.  Existen  dos  tipos de
transformaciones: las lineales y las no lineales, en este curso nos ocuparemos de las del primer tipo.

Para representar  una transformación  lineal utilizamos la  notación $T:V\to W$.  En  este caso  el dominio  es el
espacio vectorial  $V$ y el  conjunto imagen es un  \textbf{subespacio vectorial del  espacio vectorial $W$}.  Por
ejemplo,  la transformación lineal  $T:\RR^3\to\RR^2$  transforma  todos  los  vectores  del  espacio ($\RR^3$) en
vectores pertenecientes a un subespacio de $\RR^2$.  Es decir, la imagen de $T$ puede ser: el conjunto vacío o una
recta que pasa por el origen o todo $\RR^2$.

Las  transformaciones  lineales tiene  dos  características.  La primera  es  que  cada  vector  $\vv{v}\in  V$ es
transformado a un \textbf{único} vector $T\vv{v}\in W$.  La segunda  es que si su dominio es un espacio vectorial,
entonces su imagen también lo es.

Las  transformaciones  lineales  cumplen  dos  condiciones.   Si   $T:V\to  W$  es  una  transformación  lineal  y
$\vv{v},\vv{u}\in V$, entonces:
\begin{enumerate}
  \item  $T\E{\vv{v}+\vv{u}}=T\vv{v}+T\vv{u}$,   donde  $T\vv{v},T\vv{u}\in  W$.   Esta   propiedad  dice  que  la
  transformación  de  la suma  de  dos vectores  de  $V$ es  igual  a  la  suma  de  sus  imágenes  en $W$.
  \item $T\E{\alpha\vv{v}}=\alpha T\vv{v}$,  donde $\alpha\in\RR$.  Esta propiedad  dice que la transformación del
  producto de un vector por un escalar en $V$ es igual al producto del escalar por la imagen de $\vv{v}$ en $W$.
\end{enumerate}
%--------------------------------------------------------
\subsection{Ejercicio 1:}
Sea la transformación $T:\RR^2 \to \RR^3$, $T\begin{pmatrix}x\\y\end{pmatrix}=
  \begin{pmatrix}x-y\\2x\\x+y\end{pmatrix}$.
    \begin{enumerate}[label=\alph*.]
      \item Encontrar el transformado de $\vv{v}=\E{1,-1}$.
      \item Mostrar que $T$ es lineal.
      \item Mostrar que no existe ningún vector $\vv{v}\in \RR^2$ tal que $T\vv{v}=\E{2,0,1}$.
    \end{enumerate}

\textbf{a.} El enunciado nos presenta una transformación cuyo  dominio es $\RR^2$ y cuya imagen será un subespacio
de $\RR^3$, en este caso una recta que pasa por el origen o un plano que pasa por el origen.

Podemos encontrar el transformado de $\vv{v}=\E{1,-1}$ si hacemos:
\begin{align*}
  T\vv{v}=T\E{\vv{v}} = T{\begin{pmatrix}1\\-1\end{pmatrix}} = \begin{pmatrix}1-\E{-1}\\2\E{1}\\1+\E{-1}\end{pmatrix}
                      = \begin{pmatrix}2\\2\\0\end{pmatrix}.
\end{align*}
Observemos que $T$ toma un vector $\vv{v}=\E{1,-1}$ de $\RR^2$ y lo transforma en $T\vv{v}$ que es un vector de
$\RR^3$.


\textbf{b.} Para comprobar si $T$ es lineal debemos verificar, de forma general, que se cumplen las dos
propiedades indicadas en la definición.
Proponemos un par de vectores pertenecientes a $\RR^2$ (dominio). Sean $\vv{v}=\E{x_1,y_1}$ y
$\vv{u}=\E{x_2,y_2}$ sus transformados:
\[
  T\vv{v}=T\E{\vv{v}} = T\begin{pmatrix}x_1\\y_1\end{pmatrix} = \begin{pmatrix}x_1-y_1\\2x_1\\x_1+y_1\end{pmatrix}
\]
y
\[
  T\vv{u}=T\E{\vv{u}} = T\begin{pmatrix}x_2\\y_2\end{pmatrix} = \begin{pmatrix}x_2-y_2\\2x_2\\x_2+y_2\end{pmatrix}
\]

La primera propiedad pide transformar la suma de $\vv{v}$ y $\vv{u}$:
\begin{align*}
  T\E{\vv{v}+\vv{u}} &=T\E{\begin{pmatrix}x_1\\y_1\end{pmatrix}+\begin{pmatrix}x_2\\y_2\end{pmatrix}}   &&\\
                     &=T{\begin{pmatrix}x_1+x_2\\y_1+y_2\end{pmatrix}} &&\text{Suma de vectores.}\\
                     &=\begin{pmatrix}\E{x_1+x_2}-\E{y_1+y_2}\\2\E{x_1+x_2}\\\E{x_1+x_2}+\E{y_1+y_2}\end{pmatrix}
                       &&\text{Aplicando la transfromación al vector suma.}\\
                     &=\begin{pmatrix}x_1-y_1 +x_2-y_2\\2x_1+2x_2\\x_1+y_1+ x_2+y_2\end{pmatrix}
                       &&\text{Operando algebraicamente.}\\
                     &=\begin{pmatrix}x_1-y_1\\2x_1\\x_1+y_1\end{pmatrix}+
                       \begin{pmatrix}x_2-y_2\\2x_2\\x_2+y_2\end{pmatrix} &&\\
                     &=T\vv{v} + T\vv{u}. &&
\end{align*}
La última  ecuación establece que $T\E{\vv{v}+\vv{u}}=\vv{Tv}  + \vv{Tu}$,  lo que verifica  la primera propiedad.
Para verificar la segunda propiedad debemos transformar el producto de un escalar $\alpha$ por $\vv{v}$:
\begin{align*}
  T\E{\alpha\vv{v}} &=T\E{\alpha\begin{pmatrix}x_1\\y_1\end{pmatrix}}   &&\\
                    &=T{\begin{pmatrix}\alpha x_1\\\alpha y_1\end{pmatrix}} &&\text{Prod. de un vector por un
                      escalar.}\\
                    &=\begin{pmatrix}\alpha x_1-\alpha y_1\\2\E{\alpha x_1}\\\alpha x_1+\alpha y_1\end{pmatrix}
                       &&\text{Transformando el vector producto.}\\
                    &=\begin{pmatrix}\alpha\E{x_1 -y_1}\\\alpha2x_1\\\alpha\E{x_1+y_1}\end{pmatrix}
                      &&\text{Operando algebraicamente (factorizando).}\\
                    &=\alpha\begin{pmatrix}x_1 -y_1\\2x_1\\x_1+y_1\end{pmatrix} &&\\
                    &=\alpha T\vv{v}. &&
\end{align*}
Esto nos demuestra que $T\E{\alpha\vv{v}}=\alpha T\vv{v}$ cumpliendo con la segunda propiedad.

\textbf{c.} Para  encontrar si  existe una  preimagen (vector  $\vv{v}$) del  vector $T\vv{v}=\E{2,0,1}$,  debemos
proponer un vector genérico del dominio ($\RR^2$).  Sea $\vv{v}=\E{x,y}$, entonces:
\[
T\vv{v}=T{\begin{pmatrix}x\\y\end{pmatrix}}=\begin{pmatrix}x+y\\2x\\x+y\end{pmatrix}=\begin{pmatrix}2\\0\\1\end{pmatrix}.
\]
Obtenemos una igualdad entre dos vectores que nos lleva al siguiente sistema de ecuaciones:
\[
  \systeme{x-y=2,2x=0,x+y=1}.
\]
Es  un  sistema  no homogéneo,  cuya  clasificación  nos  indicará si  existe  o no un  vector  $\vv{v}$  con imagen
${T\vv{v}=\E{2,0,1}}$.  Si el  sistema tiene  solución única,  entonces  existe un  único vector  $\vv{v}$.  Si el
sistema tiene infinitas  soluciones,  entonces existen infinitos vectores $\vv{v}$ cuyo  transformado es el vector
$\E{2,0,1}$.  Por otro lado, si el sistema no tiene solución, entonces no existe ningún $\vv{v}$ en el dominio que
sea preimagen de $\E{2,0,1}$.  En otras palabras, el vector $\E{2,0,1}$ no pertenece a la imagen $T$.

Resolviendo el sistema:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rr|l}
  1 & -1 & 2 \\
  2 &  0 & 0 \\
  1 &  1 & 1 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
&\!\begin{aligned}
  & \ro{-2R_1+R_2\to R_2}\\
  & \ro{-R_1+R_3\to R_3}\\
\end{aligned}
% Segunda Matriz
\begin{sysmatrix}{rr|l}
  1 & -1 & 2 \\
  0 &  2 & -4 \\
  0 &  2 & -1 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
 & \ro{-R_2+R_3\to R_3}
% Tercer Matriz
\begin{sysmatrix}{rr|l}
  1 & -1 & 2 \\
  0 &  2 & -4 \\
  0 &  0 & 3 \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
llegamos al sistema equivalente:
\[
  \systeme{x-y=2,2y=-4,0y=3}
\]
La última ecuación  muestra que el sistema no  tiene solución,  por tanto $\E{2,0,1}$ no pertenece  a la imagen de
$T$, es decir, no existe ningún $\vv{v}\in\RR^2$ tal que su imagen sea $T\vv{v}=\E{2,0,1}$.
%=================================================================================================================
%                                                    SECCIÓN 7.2
%=================================================================================================================
\section{Sección 7.2: Propiedades de las transformaciones lineales, imagen y núcleo}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Teorema 7.2.1.
  \item Teorema 7.2.2.
  \item Teorema 7.2.3.
  \item Definición 7.2.1: Definición de núcleo e imagen de una transformación.
  \item Teorema 7.2.4.
  \item Definición 7.2.2: Definición de nulidad y rango de una transformación.
\end{itemize}
Realice los ejemplos: 7.2.1, 7.2.2, 7.2.3, 7.2.4 y 7.2.5.
%--------------------------------------------------------
\subsection{Notas de clase: Propiedades de las transformaciones lineales, imagen y núcleo}

\textbf{Teorema 7.2.1:} Este teorema presenta tres propiedades importantes de las transformaciones lineales. Si
$T$ es una transformación lineal $T:V\to W$, entonces:

\begin{enumerate}
  \item $T\E{\vv{0}}=\vv{0}$.  Esta propiedad  asegura que la imagen del  vector nulo de $V$ siempre  es el vector
  nulo  de $W$.  Sin  embargo,  es importante  aclarar que  para algunas  transformaciones puede  haber más  de un
  vector   $\vv{v}\in   V$,   diferente   del   nulo,   cuya   transformación   sea   el   vector   nulo   de  $W$
  ($T\E{\vv{v}}=\vv{0}$).

  \item $T\E{\vv{v}-\vv{u}}=T\vv{v} - T\vv{u}$. Esta propiedad indica que el transformado de la resta de dos
    vectores de $V$ es igual a la resta de sus imágenes en $W$.
  \item $T\E{\alpha_1\vv{v_1}+\alpha_2\vv{v_2}+\dots+\alpha_n\vv{v_n}} =
    \alpha_1T\vv{v_1}+\alpha_2T\vv{v_2}+\dots+\alpha_nT\vv{v_n}$.  Esta propiedad nos  muestra que el transformado
    de una combinación  lineal de vectores de  $V$ es igual al resultado  de hacer la combinación  lineal (con los
    mismos escalares) entre los transformados de cada vector en $W$.
\end{enumerate}

\textbf{Teoremas 7.2.2}  Este teorema muestra  una propiedad fundamental  de las  trasformaciones lineales  y los
espacios vectoriales de dimensión finita.  Si conocemos el efecto  de una transformación sobre los vectores de una
base de $V$,  entonces podemos  calcular  la  imagen  de  cualquier  vector  de $V$.  Es decir,  podemos encontrar
cualquier imagen sin necesidad de conocer una expresión analítica de $T$.

\textbf{Núcleo de una transformación lineal:}
Sea la transformación lineal $T:V\to W$, entonces el núcleo de transformación se define como:
\[
  \text{nu}\,T=\W{\vv{v}\in V:\,T\E{\vv{v}}=\vv{0}}.
\]
En otras palabras,  el núcleo de una transformación lineal está conformado por todos los vectores del dominio cuya
transformación es el vector nulo de la imagen.  Este  espacio vectorial es un subespacio de $V$.  Por ejemplo,  si
$V=\RR^3$, entonces $\text{nu}\,T$ podría ser:  el conjunto conformado solo por el vector nulo ($\W{\vv{0}}$), una
recta que pasa por el origen,  o un plano que pasa por el origen, o todo $\RR^3$ (solo si $T$ es la transformación
cero).

\textbf{Imagen de una transformación lineal:}
Sea la transformación lineal $T:V\to W$, entonces la imagen de transformación se define como:
\[
  \text{Im}\,T=\W{\vv{w}\in W:\,T\E{\vv{v}}=\vv{w},\text{ para algún }\vv{v}\in V}.
\]
La imagen contiene a todos los vectores que son imagen de algún vector del dominio $V$. La imagen de $T$ es un
subespacio del conjunto de llagada $W$. Por ejemplo, si $W=\RR^2$, entonces la imagen de $T$, puede ser: el
conjunto $\W{\vv{0}}$ (solo si $T$ es la transformación cero), o una recta que pasa por el origen, o todo $\RR^2$.

%--------------------------------------------------------
\subsubsection{Aclaraciones importantes:}
\begin{itemize}
  \item Si el  núcleo de una transformación  lineal $T:V\to W$ es  igual a $V$,  entonces la  transformación es la
  transformación cero ($T\E{\vv{v}}=\vv{0}$, para todo $\vv{v}\in V$).
  \item Si la imagen de una transformación lineal  $T:V\to W$ es igual a $\W{\vv{0}}$,  entonces la transformación
  es la transformación cero.
\end{itemize}
%--------------------------------------------------------
\subsection{Ejercicio 2:}
Sea $T:\RR^3\to\RR^2$ una transformación lineal y
\[
  T\begin{pmatrix}1\\0\\0\end{pmatrix}= \begin{pmatrix}-1\\2\end{pmatrix},\,
  T\begin{pmatrix}0\\1\\0\end{pmatrix}= \begin{pmatrix}1\\1\end{pmatrix}\quad\text{y}\quad
  T\begin{pmatrix}0\\0\\1\end{pmatrix}= \begin{pmatrix}-2\\3\end{pmatrix}.
\]
Calcular $T\begin{pmatrix}4\\-2\\5\end{pmatrix}$.

El  enunciado muestra  una transformación  lineal con  dominio $\RR^3$  y conjunto  de llegada  $\RR^2$ (no  es la
imagen). Además, presenta tres vectores del dominio, en este caso los vectores de la base canónica de $\RR^3$ y sus respectivas imágenes bajo la trasformación y nos pide
encontrar la imagen del vector $\E{4,-2,5}$.

Primero observemos  que no tenemos  una representación analítica  de la  transformación lineal  $T$.  Sin embargo,
notemos que con los tres vectores obtenidos al transformar  la base canónica de $\RR^3$ podemos obtener la imagen de cualquier vector de $\RR^3$ .  En primer lugar podemos representar
el vector $\E{4,-2,5}$ como una combinación lineal de esta base:
\[
\begin{pmatrix}4\\-2\\5\end{pmatrix}=4\begin{pmatrix}1\\0\\0\end{pmatrix}-2\begin{pmatrix}0\\1\\0\end{pmatrix}
  +5\begin{pmatrix}0\\0\\5\end{pmatrix}
\]
Ahora transformemos:
\begin{align*}
  T{\begin{pmatrix}4\\-2\\5\end{pmatrix}}&=T\E{4\begin{pmatrix}1\\0\\0\end{pmatrix}
    -2\begin{pmatrix}0\\1\\0\end{pmatrix}+5\begin{pmatrix}0\\0\\1\end{pmatrix}} && \\
      &=4T\begin{pmatrix}1\\0\\0\end{pmatrix}-2T\begin{pmatrix}0\\1\\0\end{pmatrix}
        +5T\begin{pmatrix}0\\0\\1\end{pmatrix} &&\text{Propiedad 3 del Teorema 7.2.1.}\\
      &=4\begin{pmatrix}-1\\2\end{pmatrix}-2\begin{pmatrix}1\\1\end{pmatrix}
        +5\begin{pmatrix}-2\\3\end{pmatrix} &&\text{Transformando cada vector.}\\
          &=\begin{pmatrix}-16\\21\end{pmatrix} &&
\end{align*}
La última  ecuación nos indica  que la imagen  del vector $\E{4,-2,5}$  es el vector  $\E{-16,21}$.  De esta misma
forma podríamos encontrar la imagen de cualquier vector del dominio, como lo afirma el Teorema 7.2.2.
%=================================================================================================================
%                                                   SECCIÓN 7.3
%=================================================================================================================
\section{Sección 7.3:  Representación matricial de una trasformación lineal}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Teorema 7.3.1.
  \item Definición 7.3.1: Matriz de transformación.
  \item Teorema 7.3.2.
  %\item Teorema 7.3.3.
  %\item Teorema 7.3.4.
\end{itemize}
Realice los ejemplos: 7.3.1, 7.3.2, 7.3.3 y 7.3.4.
%--------------------------------------------------------
\subsection{Notas de clase: Representación matricial de una trasformación lineal}

En la guía de Espacios Vectoriales II presentamos los conceptos de espacio nulo, nulidad,  espacio columna, imagen
y rango de una  matriz.  Observemos la similitud entre las definiciones de núcleo  de una transformación y espacio
nulo de  una matriz,  y entre  las definiciones de  imagen de una  transformación e  imagen de  una matriz.  Estas
similitudes no son una coincidencia, se dan por que toda transformación lineal puede ser caracteriza por una única
matriz denominada Matriz de Transformación (Teorema 7.3.1).  Por ejemplo,  a la transformación $T:\RR^2 \to \RR^3$
(ejercicio 1):
\[
   T\begin{pmatrix}x\\y\end{pmatrix}=\begin{pmatrix}x-y\\2x\\x+y\end{pmatrix}.
\]
se le asocia la matriz de transformación:
\begin{center}
\begin{tikzpicture}[baseline, ampersand replacement=\&]
    \matrix [matrix of math nodes, left delimiter={(}, right delimiter={)}, column sep=0.00cm,align=right,
             row sep=0\pgflinewidth,](A)
     {%
      1 \& -1 \\
      2 \&  0 \\
      1 \&  1 \\
    };
    \node[font=\large, anchor=south, xshift=-1.0cm, yshift = -0.35cm] at (A-2-1.west) {$\At=$};
    \node[font=\large, anchor=south, xshift=-0.00cm, yshift = 0.05cm] at (A-1-1.north) {\color{blue}$x$};
    \node[font=\large, anchor=south, xshift=0.0cm, yshift = 0.0cm] at (A-1-2.north) {\color{blue}$y$};
\end{tikzpicture}\\
\end{center}

Podemos encontrar la transformación del vector $\vv{v}=\E{1, -1}$ mediante el producto matricial (ver ejercicio~1,
inciso a.):
\[
  \vv{Tv}=\At\vv{v}=\begin{pmatrix}1 &-1\\2&0\\1&1\end{pmatrix}\begin{pmatrix}1 \\-1\end{pmatrix}
         = \begin{pmatrix}1 + 1\\2+0\\1-1\end{pmatrix}
         = \begin{pmatrix}2\\2\\0\end{pmatrix}.
\]

Es muy importante estudiar los resultados del Teorema 7.3.2 que establece:
\begin{enumerate}
  \item $\text{Im}\,T=\text{Im}\,\At=C_\At$.\\
    La imagen de una transformación $T$ es igual a la imagen de la matriz de transformación $\At$ asociada a $T$.
  \item $\text{nu}\,T=N_\At$.\\
    El núcleo de una transformación es igual al espacio nulo de la matriz de transformación.
  \item $\rho\E{T}=\rho\E{\At}$.\\
    El rango de una transformación es igual al rango de la matriz de transformación.
  \item $\nu\E{T}=\nu\E{\At}$.\\
    La nulidad de una transformación es igual a la nulidad la matriz de transformación.
\end{enumerate}

En conclusión,  podemos caracterizar completamente  una transformación lineal mediante el estudio  de su matriz de
transformación.
%--------------------------------------------------------
\subsection{Ejercicio 3:}
Sea la transformación lineal
$T:\RR^2\to\RR^2$,  $T\begin{pmatrix}x\\y\end{pmatrix}=\begin{pmatrix}x+y\\x-y\end{pmatrix}$.  Encuentre $\At$,
$\text{nu}\,T$, $\nu\E{T}$, $\text{Im}\,T$ y $\rho\E{T}$.

El enunciado presenta una  transformación con dominio $\RR^2$ y conjunto de  llegada $\RR^2$ (no necesariamente la
imagen).  Para comenzar el análisis debemos encontrar la matriz de transformación:
\[
  \At=\begin{pmatrix}1 &1\\1&-1\end{pmatrix}.
\]

Como se expuso antes en este documento, el núcleo de la trasformación es un subespacio del dominio, por tanto el
núcleo puede ser: el conjunto vacío o una recta que pasa por el origen. También sabemos que el núcleo de $T$ es
igual al espacio nulo de la matriz $\At$.
\[
  \text{nu}\,T=N_\At=\W{\vv{x}\in\RR^2:\, \At\vv{x}=\vv{0}}.
\]
Planteamos un vector genérico perteneciente al núcleo, $\vv{x}=\E{x,y}\in N_\At$, y utilizando esta definición
llegamos al sistema homogéneo $\At\vv{x}=\vv{0}$:
\[
  \begin{pmatrix}1 &1\\1&-1\end{pmatrix}\begin{pmatrix}x\\y\end{pmatrix}=\begin{pmatrix}0\\0\end{pmatrix}
  \Rightarrow
  \systeme{x+y=0,x-y=0}
\]
que podemos resolver:
\begin{alignat*}{2}
  % Matriz Original (Primera matriz)
\begin{sysmatrix}{rr|l}
  1 &  1 & 0 \\
  1 & -1 & 0 \\
\end{sysmatrix}
% Operaciones por renglón (más de una operación)
& \ro{-R_1+R_2\to R_2}
% Segunda Matriz
\begin{sysmatrix}{rr|l}
  1 &  1 & 0 \\
  0 & -2 & 0 \\
\end{sysmatrix}
\end{alignat*}
% Sistema Equivalente
obteniendo el sistema equivalente:
\[
  \systeme{x+y=0,-2y=0}\Rightarrow \begin{cases}x=0\\y=0\end{cases}
\]
El sistema tiene  solución única,  por tanto el espacio nulo  de $\At$ está conformado solo por  el vector nulo de
$\RR^2$ y el núcleo de la transformación es:
\[
  \text{nu}\,T=N_\At=\W{\vv{0}}.
\]
finalmente la nulidad de la transformación es $\nu\E{T}=\nu\E{\At}=0$.

Podemos obtener la imagen de $T$ encontrando el espacio  columna de $\At$.  En la guía de Espacios Vectoriales II,
describimos una metodología para encontrar el espacio columna de una matriz que es completamente aplicable en este
caso.  Sin embargo,  para encontrar la imagen de $T$  utilizaremos un método deductivo.  Observemos que la nulidad
de $\At$ es cero,  lo que indica que:  (1) Las columnas de $\At$ son L.I\footnote[2]{Ver guía Espacios Vectoriales
I,  notas de  clase:  rango,  nulidad,  espacio renglón y  espacio columna.}.  (2) el  rango de  la matriz  es dos
($\rho\E{\At}=2$)\footnote[3]{Teorema 5.7.7}.  En  consecuencia,  las columnas  de $\At$ forman  una base  para su
espacio columna y podemos decir que:
\[
  C_\At = \gen\W{\begin{pmatrix}1\\1\end{pmatrix};\begin{pmatrix}1\\-1\end{pmatrix}}=\RR^2.
\]
En otras palabras,  el espacio columna de $\At$ es  $\RR^2$ (coincide con el espacio de llagada).  Para finalizar,
podemos afirmar que la imagen de la transformación es:
\[
  \text{Im}\,T=\text{Im}\,\At=C_\At= \RR^2
\]
y su rango es $\rho\E{T}=\rho\E{\At}=2$.
%=================================================================================================================
%                                                      BIBLIOGRAFÍA
%=================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%%================================================================================================================
\end{document}
%=================================================================================================================
