%*****************************************************************************************************************
%*****************************************************************************************************************
%                                          ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                 GUÍA DE ESTUDIO 3
%                                          PROPIEDADES DE LOS DETERMINANTES
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2021-03-20
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                         FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
{\left(\begin{array}{@{}#1@{}}}
		{\end{array}\right)}

\newcommand{\cb}[1]{{\color{blue}{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio III \\ Determinantes y sus Propiedades\\[3pt]
	\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                     OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de las secciones 3.1, 3.2, 3.3 y 3.4:
\begin{itemize}
	\item \textbf{Sección 3.1 Determinante de una  matriz,  definiciones:} Presenta la definición de determinante de
	      una matriz  cuadrada,  la metodología para  calcular el determinante (método  de cofactores) y  como calcular el
	      determinante de matrices triangulares (inferior, superior y diagonales).
	\item \textbf{Sección 3.2 Propiedades de los determinantes:} Presenta las propiedades de los determinantes.
	\item \textbf{Sección 3.3 Determinantes e inversas:} Presenta algunas relaciones entre el determinante de una
	      matriz y su inversa.
	\item \textbf{Sección 3.4  Regla de Cramer:} Presenta una  metodología para encontrar la solución  de un sistema
	      compatible determinado mediante el cálculo de determinantes.
\end{itemize}
%=================================================================================================================
%                                                    SECCIÓN 3.1
%=================================================================================================================
\section{Determinantes, definiciones:}

Lea las definiciones y estudie los teoremas:
\begin{itemize}
	\item Definición de determinante de una matriz cuadrada de $3\times 3$.
	\item Definición de menor.
	\item Definición de cofactor.
	\item Definición de determinante de una matriz cuadrada de $n\times n$.
	\item Teorema 3.1.1.
	\item Teorema 3.1.2.
\end{itemize}
Realice los ejemplos: 3.1.1, 3.1.2 y 3.1.7

%--------------------------------------------------------
\newpage
\subsection{Ejercicio resuelto 1:}
Calcule el determinante de las siguientes matrices:
\begin{multicols}{2}
	\begin{enumerate}
		\item $A=\begin{pmatrix}1&2\\3&-3\end{pmatrix}$
		\item $B=\begin{pmatrix}-2&3&1\\4&6&5\\0&2&1\end{pmatrix}$
	\end{enumerate}
\end{multicols}

\textbf{1.} Para  calcular el  determinante de una  matriz cuadrada de  orden 2  debemos restar  el producto  de los
elementos de sus diagonales, es decir:

\begin{align*}
	\det\E{A} & = \begin{vmatrix}1&2\\3&-3\end{vmatrix}     \\
	          & = (1)(-3) - (3)(2) = -6 + 6 = 0
\end{align*}

\textbf{2.} Para calcular el determinante de una matriz cuadrada de orden $3$ podemos utilizar el método de
expansión por cofactores.
\[
	\det\E{B} = \begin{vmatrix}-2&3&1\\4&6&5\\0&2&1\end{vmatrix}
\]
Primero debemos elegir la fila o columna de la matriz con mayor número de ceros para hacer la expansión. En este
caso podemos elegir o la primera columna o la última fila. Expandiendo por la primera columna:
\begin{align*}
	\det\E{B} & = \sum_{k=1}^{3} a_{k1}A_{k1}                                                             \\
	          & = a_{11}A_{11} + a_{21}A_{21} + a_{31}A_{31}                                              \\
	%----------------------------------------------------------------------------
	\intertext{Comenzamos la expansión con el primer termino de la sumatoria, $a_{11}A_{11}$:}
	%----------------------------------------------------------------------------
	% Primera matriz
	\begin{vmatrix}
		\tikzmarknode[circle,draw=red,inner sep=0.5pt,densely dashed]{A11}{\text{-}2} & 3 & \tikzmarknode{A13}{1} \\
		4                                                                             & 6 & 5                     \\
		\tikzmarknode{A31}{0}                                                         & 2 & 1
	\end{vmatrix}
	          & = -2\E{-1}^{2}\begin{vmatrix} 6&5\\ 2 & 1 \end{vmatrix} + \dots +                                         \\
	%----------------------------------------------------------------------------
	\intertext{donde  al factor  $a_{11}=-2$ lo  llamamos pivote  y al  factor $A_{11}=\E{-1}^{1+1}\begin{vmatrix}
			6&5\\ 2 & 1 \end{vmatrix}$ lo llamamos cofactor uno-uno. Sumando el segundo termino de la expansión,
	$a_{21}A_{21}$:}
	%----------------------------------------------------------------------------
	% Segunda matriz
	\begin{vmatrix}
		\tikzmarknode{B11}{\text{-}2}                                         & 3 & 1                     \\
		\tikzmarknode[circle,draw=red,inner sep=0.5pt,densely dashed]{B21}{4} & 6 & \tikzmarknode{B23}{5} \\
		\tikzmarknode{B31}{0}                                                 & 2 & 1
	\end{vmatrix}
	          & = -2\E{-1}^{2} \begin{vmatrix} 6&5\\ 2 & 1 \end{vmatrix} + 4\E{-1}^{2+1}\begin{vmatrix} 3&1\\ 2 & 1 \end{vmatrix}+\dots \\
	%----------------------------------------------------------------------------
	\intertext{y finalmente, sumando $a_{31}A_{31}$:}
	%----------------------------------------------------------------------------
	% Tercera matriz
	\begin{vmatrix}
		\tikzmarknode{C11}{\text{-}2}                                         & 3 & 1                     \\
		4                                                                     & 6 & \tikzmarknode{C23}{5} \\
		\tikzmarknode[circle,draw=red,inner sep=0.5pt,densely dashed]{C31}{0} & 2 & \tikzmarknode{C33}{1}
	\end{vmatrix}
	          & = -2\E{-1}^2 \begin{vmatrix} 6&5\\ 2 & 1 \end{vmatrix} + 4\E{-1}^{3}\begin{vmatrix} 3&1\\ 2 & 1 \end{vmatrix} +
	0\E{-1}^{3+1}\begin{vmatrix} 3&1\\ 6 & 5 \end{vmatrix}                                                               \\
	% ----------------------------------------------
	% esto tiene que ir aqui para que las líneas de los pivotes se mantengan en su lugar
	\begin{tikzpicture}[overlay,remember picture ]
		\draw[red] (A11.east) -- (A11.east-|A13.east) (A11.south) -- (A11.south|-A31.south);
		\draw[red] (B21.east) -- (B21.east-|B23.east) (B21.south) -- (B21.south|-B31.south)
		(B21.north) -- (B21.north|-B11.north);
		\draw[red] (C31.east) -- (C31.east-|C33.east) (C31.north) -- (C31.north|-C11.north);
	\end{tikzpicture}
	% ----------------------------------------------
	%----------------------------------------------------------------------------
	\intertext{Desarrollando los determinantes de las matrices menores:}
	%----------------------------------------------------------------------------
	          & = -2\E{6 - 10} -4\E{3-2}+0\E{15-6}                                                        \\
	          & = -2\E{-4} -4\E{1}+0                                                                      \\
	          & = 8-4 = 4
\end{align*}


El $\det\E{B}=4$.
%=================================================================================================================
%                                                    SECCIÓN 3.2
%=================================================================================================================
\section{Propiedades de los determinantes}
Lea cuidadosamente  los Teoremas,  prestando  atención a  la notación  y a  la simbología  utilizada.
\begin{itemize}
	\item Teorema 3.2.1: Determinante del producto de matrices.
	\item Teorema 3.2.4: Determinante de la transpuesta de una matriz.
	\item Teorema 3.2.5: Teorema básico.
	\item Propiedad 3.2.1
	\item Propiedad 3.2.2
	\item Propiedad 3.2.5
	\item Propiedad 3.2.6
\end{itemize}
Analice y realice los ejemplos: 3.2.1, 3.2.4, 3.2.5, 3.2.6, 3.2.7 y 3.2.10
%--------------------------------------------------------
\subsection{Notas de clase:}
\textbf{Determinante de el producto de una matriz por un escalar:}

Según la propiedad 3.2.2, si un reglón o columna de $A$ se multiplica por un escalar, entonces el $\det{A}$ se
multiplica por el escalar $c$. Ahora considere la matriz:

\begin{align*}
	B = & cA                                                                              \\
	=   & c \begin{pmatrix}a_{11} & a_{12} & \cdots & a_{1n} \\
		a_{21} & a_{22} & \cdots & a_{2n} \\
		\vdots & \vdots & \ddots & \vdots \\
		a_{n1} & a_{n2} & \cdots & a_{nn}\end{pmatrix} &  &                                               \\
	=   & \begin{pmatrix}{\color{blue}{c}}a_{11} & {\color{blue}{c}}a_{12} & \cdots & {\color{blue}{c}}a_{1n} \\
		{\color{blue}{c}}a_{21} & {\color{blue}{c}}a_{22} & \cdots & {\color{blue}{c}}a_{2n} \\
		\vdots                  & \vdots                  & \ddots & \vdots                  \\
		{\color{blue}{c}}a_{n1} & {\color{blue}{c}}a_{n2} & \cdots &
		{\color{blue}{c}}a_{nn}\end{pmatrix}   &  & \text{Producto de una matriz por un escalar.}
\end{align*}
Vemos que cada  fila de $A$ está  multiplicada por el escalar  $c$.  Para calcular el determinante  de $B$ podemos
utilizar la propiedad 3.2.2:
\begin{align*}
	\B{B} & =\begin{vmatrix}{\color{blue}{c}}a_{11} & {\color{blue}{c}}a_{12} & \cdots & {\color{blue}{c}}a_{1n} \\
		{\color{blue}{c}}a_{21} & {\color{blue}{c}}a_{22} & \cdots & {\color{blue}{c}}a_{2n} \\
		\vdots                  & \vdots                  & \ddots & \vdots                  \\
		{\color{blue}{c}}a_{n1} & {\color{blue}{c}}a_{n2} & \cdots &
		{\color{blue}{c}}a_{nn}\end{vmatrix}
	= c\begin{vmatrix}
		a_{11}                  & a_{12}                  & \cdots & a_{1n}                  \\
		{\color{blue}{c}}a_{21} & {\color{blue}{c}}a_{22} & \cdots & {\color{blue}{c}}a_{2n} \\
		\vdots                  & \vdots                  & \ddots & \vdots                  \\
		{\color{blue}{c}}a_{n1} & {\color{blue}{c}}a_{n2} & \cdots & {\color{blue}{c}}a_{nn}\end{vmatrix}
	= c\cdot c\begin{vmatrix}
		a_{11}                  & a_{12}                  & \cdots & a_{1n} \\
		a_{21}                  & a_{22}                  & \cdots & a_{2n} \\
		\vdots                  & \vdots                  & \ddots & \vdots \\
		{\color{blue}{c}}a_{n1} & {\color{blue}{c}}a_{n2} & \cdots &
		{\color{blue}{c}}a_{nn}\end{vmatrix}                                                            \\
	      & =  c\cdot c\cdots c \begin{vmatrix}
		a_{11}                  & a_{12}                  & \cdots & a_{1n} \\
		a_{21}                  & a_{22}                  & \cdots & a_{2n} \\
		\vdots                  & \vdots                  & \ddots & \vdots \\
		{\color{blue}{c}}a_{n1} & {\color{blue}{c}}a_{n2} & \cdots &
		{\color{blue}{c}}a_{nn}\end{vmatrix}
	=  \tikzmarknode[]{A1}{c}\cdot c\cdots c\cdot \tikzmarknode[]{A2}{c} \begin{vmatrix}
		a_{11} & a_{12} & \cdots & a_{1n} \\
		a_{21} & a_{22} & \cdots & a_{2n} \\
		\vdots & \vdots & \ddots & \vdots \\
		a_{n1} &        & \cdots & a_{nn}\end{vmatrix} \\
	      & = c^{n}\det{A}
\end{align*}
\begin{tikzpicture}[overlay,remember picture]
	\draw [thick, decoration={brace, mirror, raise=0.2cm}, decorate] (A1.west) -- (A2.east)
	node [pos=0.5,anchor=north,yshift=-0.25cm] {$n$ veces};
\end{tikzpicture}

Hemos utilizado la propiedad $n$ número de veces ($n$ es el orden de la matriz $A$).
%--------------------------------------------------------
\subsection{Ejercicio resuelto 2:}
Sean las matrices:
$A=\begin{pmatrix}2&0&1\\0&1&4\\3&1&0\end{pmatrix}$,
$B=\begin{pmatrix}3&1&-1\\0&2&5\\0&0&-1\end{pmatrix}$ y
$C=\begin{pmatrix}1&0&2\\0&0&0\\2&4&1\end{pmatrix}$.\\[1em]
Calcular: $\det\E{2A^T} + 3\det\E{AB} +2\det\E{C^T}$\\[0.5em]

El enunciado  nos presenta 3 matrices  cuadradas de orden $3$.  Podemos  observar que la matriz  $B$ es triangular
superior y la  matriz $C$ tiene una  fila de ceros.  Además,  se nos  pide calcular una suma  de determinantes que
involucran matrices que no se dan como datos, por ejemplo $A^T$.

Para resolver el ejercicio vamos a utilizar los teoremas y propiedades de los determinantes:
\small{%
	\begin{align*}
		\det\E{2A^T} + 3\det\E{AB} +2\det\E{C^T} & = 2^3\det\E{A^T} + 3\det\E{AB} +2\det\E{C^T}         &  & \text{Propiedad:
		$\det\E{cA}=c^n\det A$.}                                                                                                   \\
		                                         & = 2^3\det\E{A^T} + 3\det\E{A}\det\E{B} +2\det\E{C^T} &  & \text{Teorema 3.2.1.} \\
		                                         & = 2^3\det\E{A} + 3\det\E{A}\det\E{B} +2\det\E{C}     &  & \text{Teorema 3.2.4.} \\
	\end{align*}
}
Vemos que necesitamos calcular los determinantes de las matrices $A$, $B$ y $C$. Según el enunciado $B$ es una
matriz triangular superior, por tanto su determinante es el producto de las componentes en su diagonal principal:
\[
	\det{B} = 3 \cdot 2 \cdot (-1) = -6.
\]
La  matriz $C$  tiene un  reglón de  ceros y  la  propiedad  3.2.1  asegura  que  $\det{C}=0$.  Resta  calcular el
determinante de  la matriz $A$,  que puede  hacerse mediante el método  de cofactores (expandiendo  por la primera
fila):

\begin{align*}
	\B{A}=\begin{vmatrix}2&0&1\\0&1&4\\3&1&0\end{vmatrix} & =  2\E{-1}^2 \begin{vmatrix} 1&4\\ 1 & 0 \end{vmatrix} +
	0\E{-1}^3 \begin{vmatrix} 0&4\\ 3 & 0 \end{vmatrix} +
	1\E{-1}^4 \begin{vmatrix} 0&1\\ 3 & 1 \end{vmatrix}                                         \\
	                                 & = 2\E{0-1} + 0 + 1\E{0-3} = -5
\end{align*}

Finalmente calculamos:
\[
	2^3\det\E{A} + 3\det\E{A}\det\E{B} +2\det\E{C} = 8\cdot(-5) + 3\cdot(-6)\cdot(-5) + 2\cdot0 = -45 + 90 = 45.
\]
%=================================================================================================================
%                                                    SECCIÓN 3.3
%=================================================================================================================
\section{Determinantes e inversas}

Estudie los teoremas:
\begin{itemize}
	\item Teorema 3.3.1.
	\item Teorema 3.3.4 Teorema resumen.
\end{itemize}

%--------------------------------------------------------
\subsection{Ejercicio resuelto 3:}
Dado el sistema no homogéneo de $3\times 3$, $A\vv{x}=\vv{b}$ para el cual $|A|=9$.
\begin{enumerate}[label=\alph*.]
	\item Clasifique el sistema dado.
	\item Resuelve el sistema homogéneo asociado $A\vv{x}=\vv{0}$.
\end{enumerate}
~\\[0.5em]
Para resolver el ejercicio utilizaremos el  Teorema 3.3.4.
\begin{enumerate}[label=\alph*.]
	\item Teniendo  en cuenta  la información del  enunciado $\B{A}=9$,  en  consecuencia,  el sistema  no homogéneo
	      $A\vv{x}=\vv{b}$ es Compatible Determinado pues el $\B{A}$ es distinto de cero.
	\item Como el $\B{A}\neq0$,  el Teorema resumen nos indica que el sistema homogéneo asociado,  $A\vv{x}=\vv{0}$,
	      es Compatible Determinado y su única solución es la trivial, $\vv{x}=\begin{pmatrix}0\\0\\0\end{pmatrix}$
\end{enumerate}
%=================================================================================================================
%                                                    SECCIÓN 3.4
%=================================================================================================================
\section{Regla de Cramer}
Estudie el Teorema 3.4.1 y realice los ejercicios 3.4.1 y 3.4.2.
%--------------------------------------------------------
\subsection{Notas de clase:}
La regla de Cramer  es una metodología para encontrar la solución (única)  de sistemas compatibles determinados de
$n\times n$.

Consideremos el sistema compatible determinado $A\vv{x}=\vv{b}$. El Teorema 3.4.1 nos dice que: si $\det{A}\neq0$,
entonces la solución única del sistema es:
\small{
	\renewcommand\arraystretch{2.5}
	\[
		\vv{x}=
		\begin{pmatrix}
			x_{1} \\x_{2}\\\vdots\\x_{n}
		\end{pmatrix}=
		\begin{pmatrix}
			\dfrac{\det A_1}{\det A} \\\dfrac{\det{A_2}}{\det A}\\\vdots\\\dfrac{\det A_{n}}{\det A}
		\end{pmatrix}
	\]
}
Donde $A_1$,  $A_2,A_i,\dots,  A_n$ son  matrices que se obtienen al  reemplazar la i-ésima columna de  $A$ con el
vector de términos independientes $\vv{b}$.  Veamos esto con un ejemplo.
%--------------------------------------------------------
\subsection{Ejercicio resuelto 4:}
Encuentre la solución del sistema: $\systeme{2x_1+x_3=6,-2x_2-3x_3=5,8x_1+5x_3=11}$, utilizando la regla de
Cramer.\\[1em]

El enunciado nos da un sistema no homogéneo de $3\times 3$, que en su forma matricial se representa como:
\begin{align*}
	A\vv{x}                                               & =\vv{b} \\
	\begin{pmatrix}2&0&1\\0&-2&-3\\8&0&5\end{pmatrix} \begin{pmatrix}x_1\\x_2\\x_3\end{pmatrix} & =
	\begin{pmatrix}6\\5\\11\end{pmatrix}
\end{align*}

Antes de utilizar la regla  de  Cramer  es  necesario  comprobar  que  la  hipótesis  del Teorema 3.1.4 se cumple.
Calculemos el $\det A$ expandiendo por la segunda columna:
\begin{align*}
	\B{A}=\begin{vmatrix}2&0&1\\0&-2&-3\\8&0&5\end{vmatrix} & =
	0\E{-1}^3 \begin{vmatrix} 0&-3\\ 8 & 5 \end{vmatrix} +
	\E{-2}\E{-1}^4 \begin{vmatrix} 2&1\\ 8 & 5 \end{vmatrix} +
	0\E{-1}^5 \begin{vmatrix} 2&1\\ 0& -3 \end{vmatrix}                         \\
	                                 & = - 2\E{10 - 8} + 0 = -4.
\end{align*}
La última ecuación nos dice que $\det A\neq 0$, lo implica que el sistema tiene solución única y
podemos utilizar la regla de Cramer. Tenemos que formar las matrices $A_i$ con $i=1,2,3$:
\[
	A_1 = \begin{pmatrix}\cb{6}&0&1\\\cb{5}&-2&-3\\\cb{11}&0&5\end{pmatrix},\quad
	A_2 = \begin{pmatrix}2&\cb{6}&1\\0&\cb{5}&-3\\8&\cb{11}&5\end{pmatrix}\quad\text{y}\quad
	A_3 = \begin{pmatrix}2&0&\cb{6}\\0&-2&\cb{5}\\8&0&\cb{11}\end{pmatrix}
\]
Recordemos que  cada matriz  $A_i$ se  forma reemplazando  la i-ésima  columna de  $A$ con  el vector  de términos
independientes.  Por ejemplo,  la  matriz $A_1$  es idéntica a  la matriz $A$  excepto por  la primer  columna que
contiene al vector $\vv{b}$.  El siguiente paso consiste en calcular el determinante de las matrices $A_i$:
\begin{align*}
	\B{A_1}=\begin{vmatrix}6&0&1\\5&-2&-3\\11&0&5\end{vmatrix}   & =
	0\E{-1}^3 \begin{vmatrix} 5&-3\\ 11 & 5 \end{vmatrix} +
	\E{-2}\E{-1}^4 \begin{vmatrix} 6&1\\ 11 & 5 \end{vmatrix} +
	0\E{-1}^5 \begin{vmatrix} 6&1\\ -2 & -3 \end{vmatrix} &                                              & \text{expandiendo por la columna 2.}   \\
	                                     & = 0 - 2\E{30 - 11} + 0 = -38                 &                                      & \\
	\B{A_2}=\begin{vmatrix}2&6&1\\0&5&-3\\8&11&5\end{vmatrix}   & =
	2\E{-1}^2 \begin{vmatrix} 5&-3\\ 11 & 5 \end{vmatrix} +
	0\E{-1}^3 \begin{vmatrix} 6&1\\ 11 & 5 \end{vmatrix} +
	8\E{-1}^4 \begin{vmatrix} 6&1\\ 5 & -3 \end{vmatrix} &                                              & \text{expandiendo por la columna 1.}   \\
	                                     & = 2\E{25+33}+ 0 +8\E{-18-5} = 116 -184 = -68 &                                      & \\
	\B{A_3}=\begin{vmatrix}2&0&6\\0&-2&5\\8&0&11\end{vmatrix}   & =
	0\E{-1}^3 \begin{vmatrix} 0&5\\ 8 & 11 \end{vmatrix} +
	\E{-2}\E{-1}^4 \begin{vmatrix} 2&6\\ 8 & 11 \end{vmatrix} +
	0\E{-1}^5 \begin{vmatrix} 2&6\\ 0 & 5 \end{vmatrix} &                                              & \text{expandiendo por la columna 2.}   \\
	                                     & = 0 - 2\E{22-48} + 0 = 52                    &                                      &
\end{align*}

Aplicando la regla de Cramer encontramos la solución al sistema:
\renewcommand\arraystretch{2.5}
\[
	\vv{x}=
	\begin{pmatrix}
		x_{1} \\x_{2}\\ x_{3}
	\end{pmatrix} =
	\begin{pmatrix}
		\dfrac{\det A_1}{\det A} \\\dfrac{\det{A_2}}{\det A}\\\dfrac{\det A_{3}}{\det A}
	\end{pmatrix} =
	\begin{pmatrix}
		\dfrac{-38}{-4} \\\dfrac{-68}{-4}\\ \dfrac{52}{-4}
	\end{pmatrix} =
	\begin{pmatrix}
		\dfrac{19}{2} \\17\\ -13
	\end{pmatrix}
\]
%=================================================================================================================
%                                                      BIBLIOGRAFÍA
%=================================================================================================================
\thispagestyle{fancy}
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%=================================================================================================================
\end{document}
%=================================================================================================================
