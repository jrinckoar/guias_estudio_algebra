%*****************************************************************************************************************
%*****************************************************************************************************************
%                                          ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                GUÍA DE ESTUDIO 8
%                                                NÚMEROS COMPLEJOS
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2021-08-18
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                         FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
{\left(\begin{array}{@{}#1@{}}}
		{\end{array}\right)}

\newcommand{\cb}[1]{{\color{blue}{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio VIII \\ Números Complejos\\[3pt]
	\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                     OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas del apéndice B.
%=================================================================================================================
%                                                     APÉNDICE B
%=================================================================================================================
\section{Apéndice B:}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
	\item Definición B.1: Definición de número complejo.
	\item Definición B.2: Definición de las operaciones de suma y multiplicación entre números complejos.
	\item Definición de conjugado de un número complejo (ecuación B.7).
	\item Definición de magnitud de un número complejo (ecuación B.10).
	\item Definición de argumento de un número complejo (ecuación B.11).
	\item Identidad de Euler (ecuación B.16).
	\item Forma binomial de un número complejo (ecuación B.7)
	\item Forma trigonométrica de un número complejo.
	\item Forma exponencial/polar de un número complejo (ecuación B.17).
\end{itemize}
Realice los ejemplos: B.2, B.3, B.4, B.5, B.6 y B.7.
%--------------------------------------------------------
\subsection{Notas de clase:}
\textbf{Número complejo:}
Según la definición B.1, un número complejo es la expresión de la forma:
\[
	z=\alpha+  i\beta \qquad\qquad \alpha,\beta\in \RR
\]
donde $\alpha$ es llamada parte real de $z$ ($\Re{z}=\alpha$) y $\beta$ es llamada parte imaginaria de $z$
($\Im{z}=\beta$). Así, cada número complejo puede ser graficado mediante un par ordenado $(\alpha, \beta)$ en el
plano complejo~(ver figura~\ref{fig:ncomp})
\begin{figure}[t!]
	\begin{center}
		\subfloat[\label{fig:ncomp}Representación de un número complejo $z=\alpha+ i\beta$ en el plano complejo.]
		{\includegraphics[width=0.3\textwidth]{ncomp.png}}
		\hspace{2.0cm}
		\subfloat[\label{fig:arg}Argumento de un número complejo $z$ y de su conjugado $\bar{z}$ en el plano
			complejo.]
		{\includegraphics[width=0.3\textwidth]{argumento.png}}
	\end{center}
	\caption{}
\end{figure}

Además, cada número complejo tiene un módulo y un argumento, el módulo se define como:
\[
	\B{z}=r=\sqrt{\alpha ^2+  \beta^2}.
\]
y el  argumento se define  como el ángulo $\theta$  entre el lado  positivo del eje real  y la recta  que conecta el
origen con el número $z$~(ver figura~\ref{fig:ncomp}). En este sentido, podemos calcular el argumento de $z$ como:
\[
	\arg{z}=\theta=
	\begin{cases}
		\tan^{-1}{\E{\frac{\beta}{\alpha}}} & \text{si}\,\, \alpha\neq 0,                            \\
		\pi/2                               & \text{si}\,\, \alpha = 0\quad \text{y}\quad \beta > 0, \\
		-\pi/2                              & \text{si}\,\, \alpha = 0\quad \text{y}\quad \beta < 0. \\
	\end{cases}
\]

Es importante aclarar que $\theta$ se mide en radianes y en el rango $(-\pi,\pi]$~(ver figura~\ref{fig:arg}).\\[1em]

\textbf{Representación de un número complejo:}
Un número complejo $z=\alpha+i\beta$ se puede representar principalmente de tres formas:
\begin{align*}
	z & =\alpha+  i\beta                &  & \text{forma binomial.}          \\
	z & =r\cos{\theta}+  ir\sen{\theta} &  & \text{forma trigonométrica.}    \\
	z & =r\EA{i\theta}                  &  & \text{forma exponencial/polar.} \\
\end{align*}

Es posible pasar de una forma a otra siguiendo el esquema de la figura~\ref{fig:esquema}.

\begin{figure}[t!]
	\centering
	\begin{tikzpicture}
		\node(0,0)[rectangle, draw, fill=blue!20, rounded corners, align=center, minimum height=0.5cm](BINOM)
		{Forma Binomial\\[0.5em]${z=\alpha+i\beta}$};
		\node [rectangle, draw, fill=blue!20, rounded corners, align=center, minimum height=0.5cm]
		[below right=1.6cm and -0.3cm of BINOM](TRIGO)
		{Forma Trigonométrica\\[0.5em]${z=r\E{\cos\theta+i\sen\theta}}$};
		\node [rectangle, draw, fill=blue!20, rounded corners, align=center, minimum height=0.5cm]
		[below right=1.6cm and -0.3cm of TRIGO](EXP)
		{Forma Exponencial\\[0.5em]${z=r\EA{i\theta}}$};

		{\large{
			\draw [ultra thick, color=blue, draw=none,] (BINOM.0) --
			node[xshift=5.4cm, yshift=1.3cm] {$ \begin{cases}
						r =      & \sqrt{\alpha^2 + \beta^2}         \\
						\theta = & {\small\begin{cases}
									\arctan\E{{\beta}/{\alpha}} & \text{si}\,\,\alpha\neq0                      \\
									\pi/2                       & \text{si}\,\,\alpha=0\,\,\text{y}\,\, \beta>0 \\
									-\pi/2                      & \text{si}\,\,\alpha=0\,\,\text{y}\,\, \beta<0
								\end{cases}}
					\end{cases}$.}  (TRIGO.90);
			\draw [->,ultra thick,blue]  (BINOM.0) to[out=0,in=90,looseness=1] (TRIGO.135);
			\draw [->,ultra thick,blue]  (BINOM.0) to[out=0,in=90,looseness=1] (EXP.135);

			\draw [ultra thick, color=olive, draw=none,] (TRIGO.0) --
			node[xshift=2.5cm, yshift=0.0cm, align=left]
			{Identidad de Euler\\$\cos\theta+i\sen\theta=\EA{i\theta}$}(EXP.90);
			\draw [->,ultra thick, olive]  (TRIGO.0) to[out=0,in=90,looseness=1] (EXP.120);

			\draw [ultra thick, color=red, draw=none,] (EXP.180) --
			node[xshift=-2.5cm, yshift=-0.5cm, align=left]
			{Identidad de Euler\\$\EA{i\theta}=\cos\theta+i\sen\theta$}(TRIGO.270);
			\draw [->,ultra thick, red]  (EXP.180) to[out=180,in=270,looseness=1] (TRIGO.270);

			\draw [ultra thick, color=black, draw=none,] (TRIGO.180) --
			node[xshift=-2.0cm, yshift=0.0cm] {$ \begin{cases}
						\alpha = & r\cos\theta \\
						\beta  = & r\sen\theta \\
					\end{cases}$.}  (BINOM.270);
			\draw [->,ultra thick, black]  (TRIGO.180) to[out=180,in=270,looseness=1] (BINOM.270);
		}}
	\end{tikzpicture}
	\caption{\label{fig:esquema}Representaciones de un número complejo $z$.}
\end{figure}

%--------------------------------------------------------
\subsection{Ejercicio 1:}
Sean los números complejos:
\[
	z_1 = 1+ \sqrt{3}i, \qquad z_2= 3\EA{-\frac{\pi}{2}i} \quad\text{y}\quad
	z_3=\frac{1}{2}\E{\cos\E{\frac{\pi}{4}} +i\sen\E{\frac{\pi}{4}}}.
\]

Realices las operaciones indicadas y grafique cada número en el plano complejo.
\begin{multicols}{3}
	\begin{enumerate}[label=\alph*.]
		\item $w_1=z_1-\conj{z_2}$.
		\item $w_2=z_2z_3$.
		\item $w_3=2z_1/z_2$.
	\end{enumerate}
\end{multicols}

El enunciado nos  presenta 3 números complejos en  tres formas diferentes.  El número $z_1$  está en forma binomial,
$z_2$ está en forma exponencial y $z_3$ está en forma trigonométrica.  Además, se nos pide realizar tres operaciones
distintas: una resta, un producto y un cociente.

\textbf{a.} El  número complejo $w_1$  es la resta  de los números  $z_1$ y  $\conj{z_2}$ (conjugado  de $z_2$).  En
general, es más fácil realizar las operaciones de suma y resta cuando los términos están en forma binomial.
\begin{align*}
	w_1 & = z_1 - \conj{z_2}                                                      &  &                                                                                          \\
	    & = 1 + \sqrt{3}i - \conj{3\EA{-\frac{\pi}{2}i}}                          &  &                                                                                          \\
	    & = 1 + \sqrt{3}i - 3\EA{\frac{\pi}{2}i}                                  &  & \text{Recordar que: \textbf{i.} $\B{\conj{z}} = \B{z}$ (ecuación B.13).}                 \\
	    &                                                                         &  & \text{\phantom{Recordar que:} \textbf{ii.} $\arg{\conj{z}} = -\arg{z}$ (ecuación B.14).} \\
	\intertext{expresaremos $\conj{z_2}$ en su forma binomial utilizando la identidad de Euler (ver
		figura~\ref{fig:esquema}):}
	w_1 & = 1 + \sqrt{3}i - 3\E{\cos\E{\frac{\pi}{2}} + i \sen\E{\frac{\pi}{2}} } &  &                                                                                          \\
	    & = 1 + \sqrt{3}i - 3\E{0 + i}                                            &  &                                                                                          \\
	    & = 1 + \sqrt{3}i - 3i                                                    &  &                                                                                          \\
	    & = 1 + \E{-3+\sqrt{3}}i                                                  &  &                                                                                          \\
\end{align*}

\textbf{b.} El  número complejo  $w_2$ es  el producto entre $z_2$ y  $z_3$. Es más conveniente realizar las
operaciones de producto y cociente de números complejos cuando los factores están en forma exponencial:
\begin{align*}
	w_2 & = z_2z_3                                                                                  &  &                                           \\
	    & = 3\EA{-\frac{\pi}{2}i} \,\,\cdot\,\,0.5\E{\cos\E{\frac{\pi}{4}} +i\sen\E{\frac{\pi}{4}}} &  &
	\intertext{expresaremos $z_3$ en su forma exponencial (ver figura~\ref{fig:esquema}):}
	w_2 & = 3\EA{-\frac{\pi}{2}i} \,\,\cdot\,\,0.5\EA{\frac{\pi}{4}i}                               &  & \text{Utilizamos la identidad de Euler.}  \\
	    & = 3\cdot 0.5 \cdot \EA{-\frac{\pi}{2}i}\cdot\EA{\frac{\pi}{4}i}                           &  &                                           \\
	    & = 1.5\EA{\E{-\frac{\pi}{2}+\frac{\pi}{4}}i}                                               &  & \text{Propiedad $\EA{x}\EA{y}=\EA{x+y}$.} \\
	    & = 1.5\EA{-\frac{\pi}{4}i}                                                                 &  &                                           \\
\end{align*}

\textbf{c.} El  número complejo  $w_3$ es  cociente entre  $2z_1$ y  $z_2$. Como ya dijimos, la operación de
división es mejor hacerla cuando el numerador y el denominador están en forma exponencial:
\begin{align*}
	w_3 & =2 z_1/z_2                                                                                      &  & \\
	    & = \frac{2\E{1+ \sqrt{3}i}}{3\EA{-\frac{\pi}{2}i}} = \frac{2+ 2\sqrt{3}i}{3\EA{-\frac{\pi}{2}i}} &  & \\
\end{align*}
debemos expresar el número $2+ 2\sqrt{3}i$ en su forma exponencial. Para esto calculamos su módulo y su argumento.
El modulo se expresa como:
\[
	r = \B{2+ 2\sqrt{3}i} = \sqrt{2^2+ \E{2\sqrt{3}}^2}= \sqrt{4+4\cdot3}=\sqrt{16}=4
\]
y el argumento como:
\[
	\theta = \arg\E{2+ 2\sqrt{3}i} = \tan^{-1}\E{\frac{2\sqrt{3}}{2}} =\tan^{-1}\E{\sqrt{3}}= \frac{\pi}{3}.
\]
y así, la forma exponencial de $2z_1=2+ 2\sqrt{3}i$ es $4\EA{\frac{\pi}{3}i}$. Retomando el cálculo de $w_3$:
\begin{align*}
	w_3 & = \frac{2+ 2\sqrt{3}i}{3\EA{-\frac{\pi}{2}i}} =\frac{4\EA{\frac{\pi}{3}i}}{3\EA{-\frac{\pi}{2}i}} &  &                                            \\
	    & = \frac{4}{3}\frac{\EA{\frac{\pi}{3}i}}{\EA{-\frac{\pi}{2}i}}                                     &  &                                            \\
	    & = \frac{4}{3}\EA{\frac{\pi}{3}i - \E{-\frac{\pi}{2}i}}                                            &  & \text{Propiedad $\EA{x}/\EA{y}=\EA{x-y}$.} \\
	    & = \frac{4}{3}\EA{\E{\frac{\pi}{3} + \frac{\pi}{2}}i} = \frac{4}{3}\EA{\frac{5\pi}{6}i}            &  &                                            \\
\end{align*}

A la hora de graficar en el plano complejo, lo más conveniente es expresar los números en forma binomial o
exponencial (ver figura~\ref{fig:planocomp01}).\\[1em]

\begin{figure}[t!]
	\centering
	\begin{tikzpicture}
		\node[anchor=south west] (O) at (0,0) {};

		\begin{polaraxis}[remember picture,
				at={($(O)+(0cm,0.0cm)$)},
				axis equal image,
				width=0.99\textwidth,
				major grid style={dotted, blue!100, line width=0.5pt},
				axis line style={gray!50!black},
				ticklabel style={blue!100!black},
				xtick={0,30,...,360},
				ytick={0,0.25,...,4},
				yticklabels={},
				ymax=2,
				ticklabel style = {font=\small},
				xticklabels={{$0$},{$\frac{1}{6}\pi$},{$\frac{1}{3}\pi$},{$\frac{1}{2}\pi$},{$\frac{2}{3}\pi$},
						{$\frac{5}{6}\pi$},{$\pi$},{$-\frac{5}{6}\pi$},{$-\frac{2}{3}\pi$},{$-\frac{1}{2}\pi$},
						{$-\frac{1}{3}\pi$},{$-\frac{1}{6}\pi$},{$0$}},
			]
		\end{polaraxis}
		\begin{scope}
			\begin{axis}[remember picture,
					at={($(O)+(0.0cm,0.0cm)$)},
					axis equal image,
					width=1.15\textwidth,
					axis lines=middle,
					xtick={-4,-3,...,4},
					ytick={-4,-3,...,4},
					grid=major,
					major grid style={black!40},
					minor tick num= 1,
					xmin=-4, xmax=4, ymin=-4, ymax=4,
					x label style={at={(axis description cs:1.05,0.55)},anchor=north},
					y label style={at={(axis description cs:0.55,0.95)},rotate=0,anchor=south},
					xlabel={$\Re{z}$},
					ylabel={$\Im{z}$}
				]
				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (1,1.732)} node[above right, red]{\large{$\bm{z_1}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (0,-3)} node[above right, red]{\large{$\bm{z_2}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (0.3535,0.3535)} node[above right, red]{\large{$\bm{z_3}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (3,-1.732)} node[above right, red]{\large{$\bm{w_1}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (1.06,-1.06)} node[above right, red]{\large{$\bm{w_2}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (-1.1547,0.667)} node[above right, red]{\large{$\bm{w_3}$}};
			\end{axis}
		\end{scope}
	\end{tikzpicture}
	\caption{Plano complejo\label{fig:planocomp01}}
\end{figure}

\textbf{Potencia de un número complejo:}
La $n$-ésima potencia entera de un número complejo $z=\alpha+i\beta$ es:
\begin{align*}
	z^n & = \E{\alpha+  i\beta}^n                     &  &                                                        \\
	    & = \E{r\EA{i\theta}}^n                       &  & \text{Escribiendo $z$ en su forma exponencial}         \\
	    & = r^n\EA{in\theta}                          &  & \text{Utilizando la propiedad $\E{\EA{x}}^y=\EA{xy}$.} \\
	    & = r^n\Q{\cos\E{n\theta}+  i\sen\E{n\theta}} &  & \text{Utilizando la identidad de Euler}.
\end{align*}

\newpage
\textbf{Raíces $n$-ésimas de un número complejo:}
Las $n$ raíces $n$-ésimas de un complejo $z=r\EA{i\theta}$ están dadas por:
\begin{equation}
	z_k =\sqrt[n]{r}\EA{i\E{\frac{\theta + 2k\pi}{n}}} \qquad\text{para}\quad k=0,1,2,\dots,n-1. \label{eq:01}
\end{equation}


%--------------------------------------------------------
\subsection{Ejercicio 2:}

\begin{figure}[ht!]
	\centering
	\centering

	\begin{tikzpicture}
		\node[anchor=south west] (O) at (0,0) {};

		\begin{polaraxis}[remember picture,
				at={($(O)+(0cm,0.0cm)$)},
				axis equal image,
				width=0.99\textwidth,
				major grid style={dotted, blue!100, line width=0.5pt},
				axis line style={gray!50!black},
				ticklabel style={blue!100!black},
				xtick={0,30,...,360},
				ytick={0,0.25,...,4},
				yticklabels={},
				ymax=2,
				ticklabel style = {font=\small},
				xticklabels={{$0$},{$\frac{1}{6}\pi$},{$\frac{1}{3}\pi$},{$\frac{1}{2}\pi$},{$\frac{2}{3}\pi$},
						{$\frac{5}{6}\pi$},{$\pi$},{$-\frac{5}{6}\pi$},{$-\frac{2}{3}\pi$},{$-\frac{1}{2}\pi$},
						{$-\frac{1}{3}\pi$},{$-\frac{1}{6}\pi$},{$0$}},
			]
		\end{polaraxis}

		\begin{scope}
			\begin{axis}[remember picture,
					at={($(O)+(0.0cm,0.0cm)$)},
					axis equal image,
					width=1.15\textwidth,
					axis lines=middle,
					xtick={-8,-6,...,8},
					ytick={-8,-6,...,8},
					grid=major,
					major grid style={black!40},
					minor tick num= 1,
					xmin=-8, xmax=8, ymin=-8, ymax=8,
					x label style={at={(axis description cs:1.05,0.55)},anchor=north},
					y label style={at={(axis description cs:0.55,0.95)},rotate=0,anchor=south},
					xlabel={$\Re{z}$},
					ylabel={$\Im{z}$}
				]
				\addplot[thick, olive, dashed, mark=*,mark  options={ultra  thick,  solid,fill=olive}]
				coordinates {(0, 0) (6.928,4)} node[below right, olive]{\large{$\bm{z}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (1.96961,0.3473)} node[above right, red]{\large{$\bm{z_0}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (-1.2856,1.5321)} node[above right, red]{\large{$\bm{z_1}$}};

				\addplot[thick, red, dashed, mark=*,mark  options={ultra  thick,  solid,fill=red}]
				coordinates {(0, 0) (-0.684,-1.88)} node[above right, red]{\large{$\bm{z_2}$}};

			\end{axis}
		\end{scope}
	\end{tikzpicture}

	\caption{Plano complejo.\label{fig:planocomp02}}
\end{figure}

Encuentre las raíces cúbicas de $z=8\EA{i\frac{\pi}{6}}$ y graficar.\\[1em]

El enunciado nos pide encontrar las tres raíces cúbicas de un complejo $z$ que está en forma exponencial,  su módulo
es $r=8$ y  su argumento es $\theta=\pi/6$.  Para encontrar estas  raíces utilizaremos la ecuación~(\ref{eq:01}),
teniendo en cuenta que $n=3$ y $k=\W{0,1,2}$.

\noindent Para $k=0$:
\begin{align*}
	z_0 & = \sqrt[3]{8}\EA{i\E{\frac{\pi/6 + 2\cdot0\cdot\pi}{3}}} \\
	    & = 2\EA{i\E{\frac{\pi/6}{3}}}=2\EA{i\frac{\pi}{18}}       \\
	    & \approx 1.97 + 0.35i
\end{align*}
Para $k=1$:
\begin{align*}
	z_1 & = \sqrt[3]{8}\EA{i\E{\frac{\pi/6 + 2\cdot1\cdot\pi}{3}}}                     \\
	    & = 2\EA{i\E{\frac{\pi/6 + 2\pi}{3}}}=2\EA{i\E{\frac{\frac{\pi+12\pi}{6}}{3}}} \\
	    & = 2\EA{i\frac{13\pi}{18}}                                                    \\
	    & \approx -1.28 + 1.53i
\end{align*}
Para $k=2$:
\begin{align*}
	z_2 & = \sqrt[3]{8}\EA{i\E{\frac{\pi/6 + 2\cdot2\cdot\pi}{3}}}                     \\
	    & = 2\EA{i\E{\frac{\pi/6 + 4\pi}{3}}}=2\EA{i\E{\frac{\frac{\pi+24\pi}{6}}{3}}} \\
	    & = 2\EA{i\frac{25\pi}{18}}                                                    \\
	\intertext{Podemos observar que  el  argumento  de  $z_2$  ($\theta_2=25\pi/18$)  es  mayor que $\pi$.  Por tanto,
		debemos restarle
		$2\pi$ para llevarlo al rango $(-\pi,\pi]$:}
	z_2 & =2\EA{i\frac{25\pi}{18}}=2\EA{i\E{\frac{25\pi}{18}-2\pi}}                    \\
	    & =2\EA{-\frac{11\pi}{18}i}                                                    \\
	    & \approx -0.68 - 1.88i
\end{align*}
Por último procedemos a graficar el complejo $z$ y sus 3 raíces, representando cada número en su forma
binomial~(ver~figura~\ref{fig:planocomp02}).


%===================================================================================================================
%                                                      BIBLIOGRAFÍA
%==================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%===================================================================================================================
\end{document}
%===================================================================================================================
