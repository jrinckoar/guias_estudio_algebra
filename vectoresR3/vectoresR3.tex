%*****************************************************************************************************************
%*****************************************************************************************************************
%                                          ÁLGEBRA LINEAL Y GEOMETRÍA ANALÍTICA
%*****************************************************************************************************************
%*****************************************************************************************************************
%                                                 GUÍA DE ESTUDIO 7
%                                                   VECTORES EN R3
%-----------------------------------------------------------------------------------------------------------------
% Juan Felipe Restrepo
% jrestrepo@ingenieria.uner.edu.ar
% 2020-04-14
%*****************************************************************************************************************
%*****************************************************************************************************************
%=================================================================================================================
%                                                         FORMATO
%=================================================================================================================
% Compilar como notas:
\documentclass[11pt]{article}
\input{../auxiliar/conf_tps.tex}
%=================================================================================================================
% Comandos locales
\usetikzlibrary{matrix,decorations.pathreplacing, calc, positioning,fit}
\usetikzlibrary{tikzmark}   % Determinantes
% Eliminación Gaussiana
\newenvironment{sysmatrix}[1]
 {\left(\begin{array}{@{}#1@{}}}
 {\end{array}\right)}

\newcommand{\cb}[1]{{\color{blue}{#1}}}
\newlength{\rowidth}% row operation width
\AtBeginDocument{\setlength{\rowidth}{3em}}
%=================================================================================================================
%                                                         TÍTULO
%=================================================================================================================
% Título y subtítulo
\title{\vspace{-1.5cm}Guía de Estudio VII \\ Vectores en el Espacio ($\RR^3$)\\[3pt]
\normalsize{Álgebra Lineal y Geometría Analítica}}
% Fecha
\date{}
%-----------------------------------------------------------------------------------------------------------------
\begin{document}
\pagestyle{plain}
\maketitle
\pagestyle{normal}
\vspace{-2cm}
%=================================================================================================================
%                                                     OBJETIVOS
%=================================================================================================================
\section{Objetivo}
Organizar la lectura y el estudio de los temas de la sección 4.3 y 4.4:
\begin{itemize}
  \item \textbf{Sección 4.3 Vectores en el espacio:} Presenta la definición de módulo y dirección de un vector en
    $\RR^3$. Además, enseña las definiciones de ángulo entre vectores y proyección de un vector sobre otro.
  \item  \textbf{Sección  4.4 El producto cruz de dos vectores:}  Define la operación de producto cruz entre dos
    vectores de $\RR^3$.
\end{itemize}
%=================================================================================================================
%                                                    SECCIÓN 4.3
%=================================================================================================================
\section{Sección 4.3: Vectores en el espacio}
Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Teorema 4.3.1: Distancia entre dos puntos del espacio.
  \item Definición 4.3.1: Operaciones de suma de vectores y multiplicación por un escalar.
  \item Definición 4.3.2: Definición de dirección de un vector en $\RR^3$.
  \item Teorema 4.3.2: Ángulo entre dos vectores del espacio.
  \item Definición 4.3.3: Definición de vectores paralelos y ortogonales en $\RR^3$.
  \item Definición 4.3.4: Definición de proyección de un vector sobre otro en $\RR^3$.
\end{itemize}
Realice los ejemplos: 4.3.3, 4.3.4, 4.3.5, 4.3.6 y 4.3.7
%--------------------------------------------------------
\subsection{Notas de clase:}
\textbf{Vectores  especiales en  $\RR^3$:} En  el espacio  podemos  definir  tres  vectores  unitarios,  el vector
${\vv{i}=\E{1,0,0}}$,  el vector  $\vv{j}=\E{0,1,0}$ y  el vector $\vv{k}=\E{0,0,1}$.  Todo  vector de  $\RR^3$ lo
podemos  escribir como  una \textbf{combinación  lineal} de  estos  tres  vectores.  Por  ejemplo,  sea  el vector
$\vv{u}=\E{a,b, c}$, entonces:
\begin{align*}
  \vv{u}&=\E{a,b, c}\\
        &= a\E{1,0,0} + b\E{0, 1, 0} + c\E{0,0,1}&&\text{Producto de un vector por un escalar y}\\
        &                                        &&\text{suma de vectores.}\\
        &= a\vv{i} + b\vv{j} + c\vv{k}           &&\text{Combinación lineal de los vectores $\vv{i}$, $\vv{j}$ y
        $\vv{k}$.}
\end{align*}

\newpage
\textbf{Sistemas de coordenadas en $\RR^{3}$:}
Existen dos  sistemas principales para  definir las coordenadas  en el espacio:  el  sistema derecho y  el sistema
izquierdo.  En este curso utilizaremos siempre el sistema derecho (figura~\ref{fig:sisd}). En este sistema podemos
localizar   cualquier   punto   $P$   mediante  una   terna   ordenada   de   números   reales   $\E{a,b,c}$  (ver
figura~\ref{fig:puntos}). Además, podemos definir los planos coordenados como se ve en la figura~\ref{fig:planos}.
\begin{figure}[t!]
   \begin{center}
        \subfloat[\label{fig:sisd}Sistema  derecho:  La mano  derecha indica  las direcciones  de crecimiento  en el
        sistema derecho.]{\includegraphics[width=0.3\textwidth]{sistemaderecho.png}}
        \hspace{0.2cm}
        \subfloat[\label{fig:puntos}Ubicación del punto $P=\E{a,b,c}$ en el espacio según el sistema derecho.]
        {\includegraphics[width=0.3\textwidth]{punto_espacio.png}}
        \hspace{0.2cm}
        \subfloat[\label{fig:planos}Planos coordenados en el sistema derecho.]
        {\includegraphics[width=0.3\textwidth]{planoscoordenados.png}}
    \end{center}
  \caption{}
\end{figure}


\textbf{Vector en $\RR3$:}
En el espacio también  podemos definir vectores.  Un vector es el conjunto todos  los segmentos de recta dirigidos
equivalentes  (los  segmentos de  recta  dirigidos equivalentes  son  aquellos  que  tienen  la  misma  magnitud y
dirección).  Si  un vector  $\vv{v}\in\RR^3$ está  representado con  un  punto  inicial  $P\E{x_1,y_1,z_1}$  y uno
terminal/final $Q\E{x_2,y_2,z_2}$, entonces podemos calcular las componentes del vector $\vv{v}$ como (ver
figura~\ref{fig:vectorPQ}):
\[
  \vv{v} = \begin{pmatrix}x_2-x_1\\y_2-y_1\\z_2-z_1\end{pmatrix}
\]
\begin{figure}[b!]
  \centering
  \includegraphics[width=0.27\textwidth]{vectorPQ.png}
  \caption{\label{fig:vectorPQ}Representación del vector $\vv{v}=\protect\vd{PQ}$.}
\end{figure}

\textbf{Dirección de un vector en  $\RR^{3}$:} La dirección de un vector en el espacio  es diferente de aquella en
el plano.  En $\RR^3$, la dirección de un vector $\vv{v}=\E{x,  y, z}$ no nulo, se define en términos de un vector
unitario que tiene la misma dirección que $\vv{v}$:
\begin{align*}
  \dir{\vv{v}} &=  \frac{\vv{v}}{\B{\vv{v}}} \\
         & = \E{\frac{x}{\B{\vv{v}}}, \frac{y}{\B{\vv{v}}}, \frac{z}{\B{\vv{v}}}} \\
         & = \E{\cos{\alpha}, \cos\beta, \cos\gamma}
\end{align*}
donde $\alpha$,  $\beta$ y $\gamma$ se llaman ángulos directores y están comprendidos en el intervalo $\Q{0,\pi}$.
Por otro  lado,  $\cos{\alpha}$,  $\cos\beta$ y $\cos\gamma$ se  denominan cosenos directores y  deben cumplir que
${\cos^2{\alpha} + \cos^2\beta + \cos^2\gamma=1}$.


%--------------------------------------------------------
\subsection{Ejercicio 1:}
Encontrar el vector $\vv{b}$  conociendo que su magnitud es $\B{\vv{b}}={10}$ y dos  de sus ángulos directores son
$\alpha=60\degree$ y $\gamma=120\degree$.

El enunciado nos pide encontrar las componentes del vector $\vv{b}=\E{x, y,  z}$.  Según los datos, solo conocemos
su magnitud y dos  ángulos  directores.  Con  el  fin  de  resolver  este  ejercicio utilizaremos la definición de
dirección de un vector en $\RR^3$:
\[
  \dir{\vv{b}}  = \E{\dfrac{x}{\B{\vv{b}}}, \dfrac{y}{\B{\vv{b}}}, \dfrac{z}{\B{\vv{b}}}}
                = \E{\cos{\alpha}, \cos\beta, \cos\gamma}
                = \E{\cos60\degree, \cos\beta, \cos120\degree}
\]

De esta ecuación obtenemos una igualdad entre dos vectores:
\[
    \E{\dfrac{x}{\B{\vv{b}}}, \dfrac{y}{\B{\vv{b}}}, \dfrac{z}{\B{\vv{b}}}} =
    \E{\cos60\degree, \cos\beta, \cos120\degree}
\]
e igualando componente a componente:
\[
  \left\{\arraycolsep=1.4pt\def\arraystretch{1.5}
    \begin{array}{rlrr}
    x &= \B{\vv{b}} \cos 60\degree = 10 \frac{1}{2} = 5 && \text{Teniendo en cuenta que $\B{\vv{b}}=10$.}\\
    y &= \B{\vv{b}} \cos \beta = 10 \cos\beta && \\
    z &= \B{\vv{b}} \cos 120\degree = 10 \E{-\frac{1}{2}} = -5 &&
    \end{array}
  \right.
\]

De esta forma hemos encontrado las coordenadas $x$ y $z$ del vector. Sin embargo, para encontrar la coordenada $y$
es necesario primero encontrar el valor del ángulo director $\beta$. Podemos establecer que:
\begin{align*}
  \cos^2{\alpha} + \cos^2\beta + \cos^2\gamma &=1 && \text{Identidad de los cosenos directores.}\\
  \cos^2\E{60\degree} + \cos^2\beta + \cos^2\E{120\degree} &=1 &&\\
  \cos^2\beta &=1 - \cos^2\E{60\degree} - \cos^2\E{120\degree} &&\\
  \cos^2\beta &=1 - \E{\frac{1}{2}}^2 - \E{-\frac{1}{2}}^2 &&\\
  \cos^2\beta &=\frac{1}{2} \\
  \cos\beta &= \pm \sqrt{\frac{1}{2}} = \pm \frac{\sqrt{2}}{2} && \text{Racionalizando.}\\
      \beta &= \cos^{-1}\E{\pm\frac{\sqrt{2}}{2}} && \text{Despejando para $\beta$.}
\end{align*}

La última ecuación nos  indica que $\beta=45\degree=\frac{\pi}{4}$rad o $\beta=225\degree=\frac{5\pi}{4}$rad.  Sin
embargo sabemos que los ángulos directores se establecen en el rango $\Q{0,\pi}$, por tanto $\beta=\frac{\pi}{4}$.
Así las componentes del vector $\vv{b}$ son:
\[
  \left\{
    \begin{array}{rl}
    x &= 5\\
    y &= \B{\vv{b}} \cos 45\degree = 10 \frac{\sqrt{2}}{2} = 5\sqrt{2}  \\
    z &= -5
    \end{array}
  \right.
\]

En conclusión el vector $\vv{b}=\E{5, 5\sqrt{2},-5}$.

%=================================================================================================================
%                                                    SECCIÓN 4.4
%=================================================================================================================
\newpage
\section{Sección  4.4: El producto cruz de dos vectores:}

Lea cuidadosamente las definiciones y teoremas, prestando atención a la notación y a la simbología utilizada:
\begin{itemize}
  \item Definición 4.4.1: Producto cruz.
  \item Teorema 4.4.1: Metodología para el cálculo del producto cruz.
  \item Teorema 4.4.2: Propiedades del producto cruz.
  \item Teorema 4.4.3: Cálculo del ángulo entre vectores mediante el producto cruz.
\end{itemize}
Analice y realice los ejemplos: 4.4.1, 4.4.2 y 4.4.3.

%--------------------------------------------------------
\subsection{Notas de clase:}
\textbf{Producto cruz:} Sobre el producto cruz $\vv{w}=\vv{u}\times\vv{v}$ es importante resaltar que:
\begin{itemize}
  \item El resultado de la operación de producto cruz es un vector.
  \item El producto cruz no conmuta, es decir $\vv{u}\times\vv{v}\neq\vv{v}\times\vv{u}$.
  \item El  vector $\vv{w}$ es ortogonal  a los vectores  $\vv{u}$ y  $\vv{v}$,  es decir  $\vv{w}\cdot\vv{u}=0$ y
  $\vv{w}\cdot\vv{v}=0$.
  \item   Dos   vectores  $\vv{u}$   y   $\vv{v}$,   diferentes  del   nulo,   son   paralelos   si   y   solo  si
  $\vv{u}\times\vv{v}=\vv{0}$.
\end{itemize}

\textbf{Ángulo entre dos vectores mediante el producto cruz:}
El ángulo $\phi$ entre dos   vectores   $\vv{u}$   y   $\vv{v}$,   diferentes   del   nulo, se puede encontrar
utilizando la relación:
\[
  \sen\phi = \frac{\B{\vv{u}\times\vv{v}}}{\B{\vv{u}}\B{\vv{v}}}
\]
donde $\B{\vv{u}\times\vv{v}}$ es  la magnitud del vector  resultante del producto cruz.


\textbf{Área del paralelogramo delimitado por los vectores $\vv{u}$ y $\vv{v}$:}

La cantidad  escalar $\B{\vv{u}\times\vv{v}}$ tiene una  interpretación geométrica,  es el  área del paralelogramo
formado por los vectores $\vv{u}$ y $\vv{v}$ (ver figura~\ref{fig:area}), es decir:
\[
  A = \B{\vv{u}\times\vv{v}} = \B{\vv{u}}\B{\vv{v}}\sen\phi
\]
\begin{figure}[b]
  \centering
  \includegraphics[width=0.3\textwidth]{pcruz.png}
  \caption{\label{fig:area}Área del paralelogramo que tiene como lados los vectores $\vv{u}$ y $\vv{v}$}
\end{figure}
%--------------------------------------------------------
\newpage
\subsection{Ejercicio 2:}
Calcular el área del paralelogramo formado por los vectores $\vv{u}=\E{2,-2,-3}$ y $\vv{v}=\E{2,0,3}$.

Vamos a resolver este  ejercicio por dos caminos distintos.  Sabemos que el área  del paralelogramo que tiene como
lados los vectores $\vv{u}$ y $\vv{v}$ es:
\begin{align}
  A & = \B{\vv{u}\times\vv{v}} \label{eq1}\\
    & = \B{\vv{u}}\B{\vv{v}}\sen\phi.\label{eq2}
\end{align}
El  primer camino  requiere que  calculemos el  área mediante  la ecuación~(\ref{eq1}).  Necesitamos  encontrar la
magnitud del  vector $\vv{u}\times\vv{v}$.  Calculemos el  producto cruz entre  $\vv{u}$ y $\vv{v}$  utilizando el
Teorema 4.4.1:

\begin{align*}
  \vv{u}\times\vv{v} &= \begin{vmatrix} \vv{i}&\vv{j}&\tikzmarknode{A1}{\vv{k}}\\ 2
  &-2&\tikzmarknode{A2}{-3}\\2&0&\tikzmarknode{A3}{3}\end{vmatrix}\\
  \intertext{utilizando el método de  cofactores y expandiendo por la primera fila  (para el producto cruz siempre
  se debe expandir por la primera fila):}
  \vv{u}\times\vv{v}&= \vv{i}\E{-1}^2\begin{vmatrix}-2&-3\\0&3\end{vmatrix}+
    \vv{j}\E{-1}^3\begin{vmatrix}2&-3\\2&3\end{vmatrix}+\vv{k}\E{-1}^4\begin{vmatrix}2&-2\\2&0\end{vmatrix} \\
       & = \vv{i}\E{-6-0}-\vv{j}\E{6+6} +\vv{k}\E{0+4}\\
       & = -6\vv{i}-12\vv{j} +4\vv{k}
\end{align*}
\begin{tikzpicture}[overlay,remember picture]
  \node[right=1.1cm of A1](N1){Versor $\E{\vv{i},\vv{j},\vv{k}}$};
  \node[right=1cm of A2](N2){Vector $\vv{u}$};
  \node[right=1.2cm of A3](N3){Vector $\vv{v}$};
  \draw[->](N1.west) -- ([xshift=12pt]A1.east);
  \draw[->](N2.west) -- ([xshift=8pt]A2.east);
  \draw[->](N3.west) -- ([xshift=12pt]A3.east);
\end{tikzpicture}
Ahora calculemos el área utilizando la ecuación~(\ref{eq1}):
\begin{align*}
  A & = \B{\vv{u}\times\vv{v}}\\
    & = \B{-6\vv{i}-12\vv{j} +4\vv{k}} = \sqrt{\E{-6}^2+\E{-12}^2+4^2}=14
\end{align*}

También podemos calcular el  área mediante la ecuación~(\ref{eq2}),  solo nos falta encontrar  el ángulo entre los
vectores $\vv{u}$ y $\vv{v}$. Podemos usar el Teorema 4.3.2:
\begin{align*}
  \cos\phi &= \frac{\vv{u}\cdot\vv{v}}{\B{\vv{u}}\B{\vv{v}}} \\
           &= \frac{4-9}{\sqrt{17}\sqrt{13}} \\
      \phi &= \cos^{-1}\E{\frac{-5}{\sqrt{17}\sqrt{13}}} \\
           &\approx 1.91\,\text{rad}
\end{align*}

La ecuación~(\ref{eq2}) nos dice que:
\begin{align*}
  A & = \B{\vv{u}}\B{\vv{v}}\sen\phi \\
    & =\sqrt{17}\sqrt{13}\sen\E{1.91}\approx14
\end{align*}

Finalmente podemos concluir que el área del paralelogramo formado por $\vv{u}$ y $\vv{v}$ es $A=14$ unidades
cuadradas.

%=================================================================================================================
%                                                      BIBLIOGRAFÍA
%=================================================================================================================
\nocite{Grossman2008}
\bibliographystyle{../auxiliar/model2-names}
\bibliography{../auxiliar/Math_bibliography}
%=================================================================================================================
\end{document}
%=================================================================================================================
